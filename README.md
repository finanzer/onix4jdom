The implementation of this project is currently in progress.

# Introduction
This library is for writing onix message with java and is based on jdom2. The ONIX for Books Product Information Message is the international standard for representing and communicating book industry product information in electronic form.

> ONIX for Books was the first, and is the most widely-adopted, member of EDItEUR’s ONIX family of standards. It was initially developed by EDItEUR jointly with Book Industry Communication (UK) and the Book Industry Study Group (US), and is now maintained under the guidance of an International Steering Committee including not only BIC and BISG but also national user groups in Australia, Belgium, Canada, China, Egypt, Finland, France, Germany, Italy, Japan, Korea, The Netherlands, Norway, Russia, Spain, and Sweden. The ONIX for Books Product Information Message is the international standard for representing and communicating book industry product information in electronic form.

You can use only the api methods and if you know what you do you can use all methods of jdom classes for manipulating the xml.

Currently the most used version 2.1 of onix is the implementation in progress. The current version of onix 3.0 will be implemented later.   

# Usage
This library needs Java 8, because its used lambda, the time and date api and other java 8 features.

You can find example usage in test class.

