package de.machmireinebook;

import org.jdom2.IllegalDataException;

/**
 * This exception is thrown if the code  of a codelist is not valid or this code is not valid in this circumstances.
 */
public class IllegalOnixCodeListValueException extends IllegalDataException
{
    /**
     * This will create an exceptoin with the specified error message.
     *
     * @param reason cause of the problem
     */
    public IllegalOnixCodeListValueException(String reason)
    {
        super(reason);
    }
}
