package de.machmireinebook;

import org.jdom2.IllegalDataException;

/**
 * This exception is thrown if the data transfered to an OnixElement are not valid for this OnixElement.
 */
public class IllegalOnixDataException extends IllegalDataException
{
    /**
     * This will create an exception with the specified error message.
     *
     * @param reason cause of the problem
     */
    public IllegalOnixDataException(String reason)
    {
        super(reason);
    }
}
