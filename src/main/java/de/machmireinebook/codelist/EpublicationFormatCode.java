/*

 * 
 OnixCodeList

 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.machmireinebook.codelist;

import java.util.HashMap;
import java.util.Map;

/**
 * marker interface to assist in IDE navigation to code-list 11 (Epublication format code)
 */
interface OnixCodeList11
{}

/**
 * {@code Enum} that corresponds to ONIX <b>Codelist 11</b>
 * <p>
 * Description: Epublication format code
 * 
 * @see <a href="http://www.editeur.org/14/code-lists">About ONIX Codelists</a>
 * @see <a
 *      href="http://www.editeur.org/files/ONIX%20for%20books%20-%20code%20lists/ONIX_BookProduct_Codelists_Issue_32.html#codelist11">ONIX
 *      Codelist 11 in Reference Guide</a>
 */
public enum EpublicationFormatCode implements OnixCodeList, OnixCodeList11
{
	HTML("01", "HTML"), //

	PDF("02", "PDF"), //

	/**
	 * '.LIT' file format used by Microsoft Reader software
	 */
	Microsoft_Reader("03", "Microsoft Reader"), //

	RocketBook("04", "RocketBook"), //

	Rich_text_format_RTF("05", "Rich text format (RTF)"), //

	Open_Ebook_Publication_Structure_OEBPS("06", "Open Ebook Publication Structure (OEBPS) format standard"), //

	XML("07", "XML"), //

	SGML("08", "SGML"), //

	/**
	 * '.EXE' file format used when an epublication is delivered as a self-executing package of software and content
	 */
	EXE("09", "EXE"), //

	/**
	 * '.TXT' file format
	 */
	ASCII("10", "ASCII"), //

	/**
	 * Proprietary file format used for the MobiPocket reader software
	 */
	MobiPocket_format("11", "MobiPocket format");

	public final String code;
	public final String description;

	private EpublicationFormatCode(String code, String description)
	{
		this.code = code;
		this.description = description;
	}

	@Override
	public String getCode()
	{
		return code;
	}

	@Override
	public String getDescription()
	{
		return description;
	}

	private static volatile Map<String, EpublicationFormatCode> map;

	private static Map<String, EpublicationFormatCode> map()
	{
		Map<String, EpublicationFormatCode> result = map;
		if (result == null)
		{
			synchronized (EpublicationFormatCode.class)
			{
				result = map;
				if (result == null)
				{
					result = new HashMap<>();
					for (EpublicationFormatCode e : values())
						result.put(e.code, e);
					map = result;
				}
			}
		}
		return result;
	}

	public static EpublicationFormatCode byCode(String code)
	{
		if (code == null || code.isEmpty())
			return null;
		return map().get(code);
	}
}
