/*

 * 
 OnixCodeList

 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.machmireinebook.codelist;



/*
 * NOTE: THIS IS AN AUTO-GENERATED FILE, DON'T EDIT MANUALLY
 */

/**
 * marker interface to assist in IDE navigation to code-list 37 (Front cover image file link type code)
 */
interface CodeList37
{}

/**
 * {@code Enum} that corresponds to ONIX <b>Codelist 37</b>
 * <p>
 * Description: Front cover image file link type code
 * 
 * @see <a href="http://www.editeur.org/14/code-lists">About ONIX Codelists</a>
 * @see <a
 *      href="http://www.editeur.org/files/ONIX%20for%20books%20-%20code%20lists/ONIX_BookProduct_Codelists_Issue_32.html#codelist37">ONIX
 *      Codelist 37 in Reference Guide</a>
 */
public enum FrontCoverImageFileLinkTypes implements OnixCodeList, CodeList37
{
	URL("01", "URL"), //

	DOI("02", "DOI"), //

	PURL("03", "PURL"), //

	URN("04", "URN"), //

	FTP_address("05", "FTP address"), //

	filename("06", "filename");

	public final String code;
	public final String description;

	private FrontCoverImageFileLinkTypes(String code, String description)
	{
		this.code = code;
		this.description = description;
	}

	@Override
	public String getCode()
	{
		return code;
	}

	@Override
	public String getDescription()
	{
		return description;
	}

	public static FrontCoverImageFileLinkTypes byCode(String code)
	{
		if (code == null || code.isEmpty())
			return null;
		for (FrontCoverImageFileLinkTypes e : values())
			if (e.code.equals(code))
				return e;
		return null;
	}
}
