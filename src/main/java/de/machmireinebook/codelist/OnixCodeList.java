package de.machmireinebook.codelist;

/**
 * User: mjungierek
 * Date: 29.07.2016
 * Time: 22:13
 */
public interface OnixCodeList
{
    String getCode();
    String getDescription();
}
