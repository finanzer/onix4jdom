package de.machmireinebook.codelist;

/**
 * User: mjungierek
 * Date: 10.08.2016
 * Time: 23:27
 */
public interface OnixCodeListFactory<T>
{
    T byCode(String code);
}
