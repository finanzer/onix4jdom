/*

 * 
 OnixCodeList

 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.machmireinebook.codelist;

import java.util.HashMap;
import java.util.Map;

/**
 * marker interface to assist in IDE navigation to code-list 51 (Product relation code)
 */
interface OnixCodeList51
{}

/**
 * {@code Enum} that corresponds to ONIX <b>Codelist 51</b>
 * <p>
 * Description: Product relation code
 * 
 * @see <a href="http://www.editeur.org/14/code-lists">About ONIX Codelists</a>
 * @see <a
 *      href="http://www.editeur.org/files/ONIX%20for%20books%20-%20code%20lists/ONIX_BookProduct_Codelists_Issue_32.html#codelist51">ONIX
 *      Codelist 51 in Reference Guide</a>
 */
public enum ProductRelationCode implements OnixCodeList, OnixCodeList51
{
	/**
	 * &lt;Product&gt; is related to &lt;RelatedProduct&gt; in a way that cannot be specified by another code value
	 */
	Unspecified("00", "Unspecified", "Nicht näher bezeichnet"), //

	/**
	 * &lt;Product&gt; includes &lt;RelatedProduct&gt;
	 */
	Includes("01", "Includes", "Umfasst"), //

	/**
	 * &lt;Product&gt; is part of &lt;RelatedProduct&gt;: use for 'also available as part of'
	 */
	Is_part_of("02", "Is part of", "Ist Teil von"), //

	/**
	 * &lt;Product&gt; replaces, or is new edition of, &lt;RelatedProduct&gt;
	 */
	Replaces("03", "Replaces", "Ersetzt"), //

	/**
	 * &lt;Product&gt; is replaced by, or has new edition, &lt;RelatedProduct&gt; (reciprocal of code 03)
	 */
	Replaced_by("05", "Replaced by", "Wird ersetzt durch"), //

	/**
	 * &lt;Product&gt; is available in an alternative format as &lt;RelatedProduct&gt; - indicates an alternative format
	 * of the same content which is or may be available
	 */
	Alternative_format("06", "Alternative format", "Anderes Format"), //

	/**
	 * &lt;Product&gt; has an ancillary or supplementary product &lt;RelatedProduct&gt;
	 */
	Has_ancillary_product("07", "Has ancillary product", "Ergänzendes Produkt zu"), //

	/**
	 * &lt;Product&gt; is ancillary or supplementary to &lt;RelatedProduct&gt;
	 */
	Is_ancillary_to("08", "Is ancillary to", "Ist Ergänzung zu"), //

	/**
	 * &lt;Product&gt; is remaindered as &lt;RelatedProduct&gt;, when a remainder merchant assigns its own identifier to
	 * the product
	 */
	Is_remaindered_as("09", "Is remaindered as", "Wird verramscht als"), //

	/**
	 * &lt;Product&gt; was originally sold as &lt;RelatedProduct&gt;, indicating the publisher's original identifier for
	 * a title which is offered as a remainder under a different identifier (reciprocal of code 09)
	 */
	Is_remainder_of("10", "Is remainder of", "Restbestand von"), //

	/**
	 * &lt;Product&gt; is an other-language version of &lt;RelatedProduct&gt;
	 */
	Is_other_language_version_of("11", "Is other-language version of", "Ist Ausgabe in anderer Sprache von"), //

	/**
	 * &lt;Product&gt; has a publisher's suggested alternative &lt;RelatedProduct&gt;, which does not, however, carry
	 * the same content (cf 05 and 06)
	 */
	Publisher_s_suggested_alternative("12", "Publisher’s suggested alternative", "Vom Verlag vorgeschlagene Alternative"), //

	/**
	 * &lt;Product&gt; is an epublication based on printed product &lt;RelatedProduct&gt;
	 */
	Epublication_based_on_print_product("13", "Epublication based on (print product)", "E-Book basiert auf"), //

	/**
	 * &lt;Product&gt; is an epublication 'rendered' as &lt;RelatedProduct&gt;: use in ONIX 2.1 only when the
	 * &lt;Product&gt; record describes a package of electronic content which is available in multiple 'renderings'
	 * (coded 000 in &lt;EpubTypeCode&gt;): NOT USED in ONIX 3.0
	 */
	Epublication_is_distributed_as("14", "Epublication is distributed as", "E-Book lieferbar als"), //

	/**
	 * &lt;Product&gt; is a 'rendering' of an epublication &lt;RelatedProduct&gt;: use in ONIX 2.1 only when the
	 * &lt;Product&gt; record describes a specific rendering of an epublication content package, to identify the
	 * package: NOT USED in ONIX 3.0
	 */
	Epublication_is_a_rendering_of("15", "Epublication is a rendering of", "E-Book ist Wiedergabe von"), //

	/**
	 * &lt;Product&gt; is a POD replacement for &lt;RelatedProduct&gt;. &lt;RelatedProduct&gt; is an out-of-print
	 * product replaced by a print-on-demand version under a new ISBN
	 */
	POD_replacement_for("16", "POD replacement for", "Print-On-Demand-Ersatz für"), //

	/**
	 * &lt;Product&gt; is replaced by POD &lt;RelatedProduct&gt;. &lt;RelatedProduct&gt; is a print-on-demand
	 * replacement, under a new ISBN, for an out-of-print &lt;Product&gt; (reciprocal of code 16)
	 */
	Replaced_by_POD("17", "Replaced by POD", "Wird ersetzt durch Print-On-Demand-Ausgabe"), //

	/**
	 * &lt;Product&gt; is a special edition of &lt;RelatedProduct&gt;. Used for a special edition (German:
	 * Sonderausgabe) with different cover, binding etc - more than 'alternative format' - which may be available in
	 * limited quantity and for a limited time
	 */
	Is_special_edition_of("18", "Is special edition of", "Sonderausgabe von"), //

	/**
	 * &lt;Product&gt; has a special edition &lt;RelatedProduct&gt; (reciprocal of code 18)
	 */
	Has_special_edition("19", "Has special edition", "Sonderausgabe verfügbar als"), //

	/**
	 * &lt;Product&gt; is a prebound edition of &lt;RelatedProduct&gt; (in the US, a prebound edition is 'a book that
	 * was previously bound and has been rebound with a library quality hardcover binding. In almost all commercial
	 * cases, the book in question began as a paperback.')
	 */
	Is_prebound_edition_of("20", "Is prebound edition of", "Vorgebundene Ausgabe von"), //

	/**
	 * &lt;Product&gt; is the regular edition of which &lt;RelatedProduct&gt; is a prebound edition
	 */
	Is_original_of_prebound_edition("21", "Is original of prebound edition", "Original der vorgebundenen Ausgabe"), //

	/**
	 * &lt;Product&gt; and &lt;RelatedProduct&gt; have a common author
	 */
	Product_by_same_author("22", "Product by same author", "Produkt des gleichen Autors"), //

	/**
	 * &lt;RelatedProduct&gt; is another product that is suggested as similar to &lt;Product&gt; ('if you liked
	 * &lt;Product&gt;, you may also like &lt;RelatedProduct&gt;')
	 */
	Similar_product("23", "Similar product", "Ähnliches Produkt"), //

	/**
	 * &lt;Product&gt; is a facsimile edition of &lt;RelatedProduct&gt;
	 */
	Is_facsimile_of("24", "Is facsimile of", "Ist Faksimile von"), //

	/**
	 * &lt;Product&gt; is the original edition from which a facsimile edition &lt;RelatedProduct&gt; is taken
	 * (reciprocal of code 25)
	 */
	Is_original_of_facsimile("25", "Is original of facsimile", "Original der Faksimileausgabe"), //

	/**
	 * &lt;Product&gt; is a license for a digital &lt;RelatedProduct&gt;, traded or supplied separately
	 */
	Is_license_for("26", "Is license for", "Lizensiert für"), //

	/**
	 * &lt;RelatedProduct&gt; is an electronic version of print &lt;Product&gt; (reciprocal of code 13)
	 */
	Electronic_version_available_as("27", "Electronic version available as", "E-Book verfügbar als"), //

	/**
	 * &lt;RelatedProduct&gt; is an 'enhanced' version of &lt;Product&gt;, with additional content. Typically used to
	 * link an enhanced e-book to its original 'unenhanced' equivalent, but not specifically limited to linking e-books
	 * - for example, may be used to link illustrated and non-illustrated print books. &lt;Product&gt; and
	 * &lt;RelatedProduct&gt; should share the same &lt;ProductForm&gt;
	 */
	Enhanced_version_available_as("28", "Enhanced version available as", "Erweiterte Ausgabe verfügbar als"), //

	/**
	 * &lt;RelatedProduct&gt; is a basic version of &lt;Product&gt; (reciprocal of code 28). &lt;Product&gt; and
	 * &lt;RelatedProduct&gt; should share the same &lt;ProductForm&gt;
	 */
	Basic_version_available_as("29", "Basic version available as", "Standardversion verfügbar als"), //

	/**
	 * &lt;RelatedProduct&gt; and &lt;Product&gt; are part of the same collection (eg two products in same series or
	 * set)
	 */
	Product_in_same_collection("30", "Product in same collection", "Produkt in der gleichen Reihe/Serie"), //

	/**
	 * &lt;RelatedProduct&gt; is an alternative product in another sector (of the same geographical market). Indicates
	 * an alternative that carries the same content, but available to a different set of customers, as one or both
	 * products are retailer-, channel- or market sector-specific
	 */
	Has_alternative_in_a_different_market_sector("31", "Has alternative in a different market sector", "Als Alternative für einen anderen Markt vorhanden"), //

	/**
	 * &lt;RelatedProduct&gt; is an equivalent product, often intended for another (geographical) market. Indicates an
	 * alternative that carries essentially the same content, though slightly adapted for local circumstances (as
	 * opposed to a translation - use code 11)
	 */
	Has_equivalent_intended_for_a_different_market("32", "Has equivalent intended for a different market", "Gleichwertiges Produkt für einen anderen Markt vorhanden"), //

	/**
	 * &lt;RelatedProduct&gt; is an alternative product, often intended for another (geographical) market. Indicates the
	 * content of the alternative is identical in all respects
	 */
	Has_alternative_intended_for_different_market("33", "Has alternative intended for different market", "Als Alternative für einen anderen Markt vorhanden"), //

	/**
	 * &lt;Product&gt; cites &lt;RelatedProduct&gt;
	 */
	Cites("34", "Cites", "Zitiert"), //

	/**
	 * &lt;Product&gt; is the object of a citation in &lt;RelatedProduct&gt;
	 */
	Is_cited_by("35", "Is cited by", "Wird zitiert von"), //

	/**
	 * Use to give the ISBN of another book that had sales (both in terms of copy numbers and customer profile)
	 * comparable to that the publisher or distributor estimates for the product. Use in ONIX 2.1 ONLY &lt;p&gt;NOTE:
	 * Introduced in Onix3
	 */
	Sales_expectation("36", "Sales expectation", "Umsatzerwartung");

	public final String code;
	public final String description;
	public final String descriptionGerman;

	ProductRelationCode(String code, String description, String descriptionGerman)
	{
		this.code = code;
		this.description = description;
		this.descriptionGerman = descriptionGerman;
	}

	@Override
	public String getCode()
	{
		return code;
	}

	@Override
	public String getDescription()
	{
		return description;
	}

	private static volatile Map<String, ProductRelationCode> map;

	private static Map<String, ProductRelationCode> map()
	{
		Map<String, ProductRelationCode> result = map;
		if (result == null)
		{
			synchronized (ProductRelationCode.class)
			{
				result = map;
				if (result == null)
				{
					result = new HashMap<>();
					for (ProductRelationCode e : values())
						result.put(e.code, e);
					map = result;
				}
			}
		}
		return result;
	}

	public static ProductRelationCode byCode(String code)
	{
		if (code == null || code.isEmpty())
			return null;
		return map().get(code);
	}
}
