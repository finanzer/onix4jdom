package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.IllegalOnixDataException;
import de.machmireinebook.codelist.ContributorRoleCode;
import de.machmireinebook.codelist.CountryCode;
import de.machmireinebook.codelist.RegionCode;
import de.machmireinebook.codelist.UnnamedPersonsCode;

/**
 * Authorship and other forms of contribution are described by repeats of the &lt;Contributor&gt; composite, within which the
 * recommended form of representation of a person name is the structured data element group consisting of Person name
 * part 1 to Person name part 8. If desired, more than one form of representation of the same name may be sent in a single
 * occurrence of the composite.

 A contributor composite is valid in terms of the XML DTD provided it contains at least a &lt;ContributorRole&gt; code and:
 <ul>
 <li>one or more of the forms of representation of a person name, with or without an occurrence of the &gt;PersonNameIdentifier&gt;
 composite, or</li>
 <li>a &lt;CorporateName&gt; element, or </li>
 <li>an occurrence of the &lt;PersonNameIdentifier&gt; composite without any accompanying name element(s), or </li>
 <li>an &lt;UnnamedPersons&gt; element. </li>
 </ul>
 Other elements are optional.
 The &lt;ContributorStatement&gt; element may be used to provide a free text statement of the whole of the authorship
 of the product in the form in which the publisher intends it to be displayed. Individual name elements must still
 be sent for indexing to support computer searching.

 A new optional element &lt;NoContributor&gt; may be used to make a positive statement in an ONIX record that the
 item has no named authorship.
 */
public class Contributor extends OnixElement
{
    private OnixElement sequenceNumber = new OnixElement("SequenceNumber", "b034");
    private OnixCodeListValueElement<ContributorRoleCode> contributorRole = new OnixCodeListValueElement<>("ContributorRole", "b035", OnixOccurenceType.MANDATORY, ContributorRoleCode::byCode);
    private OnixElement languageCode = new OnixElement("LanguageCode", "b252");
    private OnixElement sequenceNumberWithinRole = new OnixElement("SequenceNumberWithinRole", "b340");
    private OnixElement personName = new OnixElement("PersonName", "b036");
    private OnixElement personNameInverted = new OnixElement("PersonNameInverted", "b037");
    private OnixElement titlesBeforeNames = new OnixElement("TitlesBeforeNames", "b038");
    private OnixElement namesBeforeKey = new OnixElement("NamesBeforeKey", "b039");
    private OnixElement prefixToKey = new OnixElement("PrefixToKey", "b247");
    private OnixElement keyNames = new OnixElement("KeyNames", "b040");
    private OnixElement namesAfterKey = new OnixElement("NamesAfterKey", "b041");
    private OnixElement suffixToKey = new OnixElement("SuffixToKey", "b248");
    private OnixElement lettersAfterNames = new OnixElement("LettersAfterNames", "b042");
    private OnixElement titlesAfterNames = new OnixElement("TitlesAfterNames", "b043");
    private List<PersonNameIdentifier> personNameIdentifiers = new ArrayList<>();
    private List<Name> names = new ArrayList<>();
    private List<PersonDate> personDates = new ArrayList<>();
    private List<ProfessionalAffiliation> professionalAffiliations = new ArrayList<>();
    private OnixElement corporateName = new OnixElement("CorporateName", "b047");
    private OnixElement biographicalNote = new OnixElement("BiographicalNote", "b044");
    private List<Website> websites = new ArrayList<>();
    private OnixElement contributorDescription = new OnixElement("ContributorDescription", "b048");
    private OnixCodeListValueElement<UnnamedPersonsCode> unnamedPersons = new OnixCodeListValueElement<>("UnnamedPersons", "b249", UnnamedPersonsCode::byCode);
    private OnixCodeListValueElement<CountryCode> countryCode = new OnixCodeListValueElement<>("CountryCode", "b251", CountryCode::byCode);
    private OnixCodeListValueElement<RegionCode> regionCode = new OnixCodeListValueElement<>("RegionCode", "b398", RegionCode::byCode);

    public Contributor()
    {
        super("Contributor", "contributor");
        addContent(sequenceNumber);
        addContent(contributorRole);
        addContent(languageCode);
        addContent(sequenceNumberWithinRole);
        addContent(personName);
        addContent(personNameInverted);
        addContent(titlesBeforeNames);
        addContent(namesBeforeKey);
        addContent(prefixToKey);
        addContent(keyNames);
        addContent(namesAfterKey);
        addContent(suffixToKey);
        addContent(lettersAfterNames);
        addContent(titlesAfterNames);
        addContent(corporateName);
        addContent(biographicalNote);
        addContent(contributorDescription);
        addContent(unnamedPersons);
        addContent(countryCode);
        addContent(regionCode);
    }

    public void setSequenceNumber(String sequenceNumber)
    {
        this.sequenceNumber.setText(sequenceNumber);
    }

    public void setContributorRole(ContributorRoleCode contributorRole)
    {
        this.contributorRole.setOnixValue(contributorRole);
    }

    public void setLanguageCode(String languageCode)
    {
        if (!contributorRole.getOnixValue().equals(ContributorRoleCode.Translated_by) &&
                !contributorRole.getOnixValue().equals(ContributorRoleCode.Translated_with_commentary_by) &&
                !contributorRole.getOnixValue().equals(ContributorRoleCode.Edited_and_translated_by))
        {
            throw new IllegalOnixDataException("LanguageCode could only set if ContributorRole is equals B06, B08 or B10");
        }
        this.languageCode.setText(languageCode);
    }

    public void setSequenceNumberWithinRole(String sequenceNumberWithinRole)
    {
        this.sequenceNumberWithinRole.setText(sequenceNumberWithinRole);
    }

    public void setPersonName(String personName)
    {
        this.personName.setText(personName);
    }

    public void setPersonNameInverted(String personNameInverted)
    {
        this.personNameInverted.setText(personNameInverted);
    }

    public void setTitlesBeforeNames(String titlesBeforeNames)
    {
        this.titlesBeforeNames.setText(titlesBeforeNames);
    }

    public void setNamesBeforeKey(String namesBeforeKey)
    {
        this.namesBeforeKey.setText(namesBeforeKey);
    }

    public void setPrefixToKey(String prefixToKey)
    {
        this.prefixToKey.setText(prefixToKey);
    }

    public void setKeyNames(String keyNames)
    {
        this.keyNames.setText(keyNames);
    }

    public void setNamesAfterKey(String namesAfterKey)
    {
        this.namesAfterKey.setText(namesAfterKey);
    }

    public void setSuffixToKey(String suffixToKey)
    {
        this.suffixToKey.setText(suffixToKey);
    }

    public void setLettersAfterNames(String lettersAfterNames)
    {
        this.lettersAfterNames.setText(lettersAfterNames);
    }

    public void setTitlesAfterNames(String titlesAfterNames)
    {
        this.titlesAfterNames.setText(titlesAfterNames);
    }

    public void addPersonNameIdentifier(PersonNameIdentifier personNameIdentifier)
    {
        personNameIdentifiers.add(personNameIdentifier);
        int index = indexOf(lettersAfterNames);
        addContent(index + personNameIdentifiers.size(), personNameIdentifier);
    }

    public void addName(Name name)
    {
        names.add(name);
        int index = indexOf(lettersAfterNames);
        addContent(index + personNameIdentifiers.size() + names.size(), name);
    }

    public void addPersonDate(PersonDate personDate)
    {
        personDates.add(personDate);
        int index = indexOf(lettersAfterNames);
        addContent(index + personNameIdentifiers.size() + names.size() + personDates.size(), personDate);
    }


    public void addProfessionalAffiliation(ProfessionalAffiliation professionalAffiliation)
    {
        professionalAffiliations.add(professionalAffiliation);
        int index = indexOf(lettersAfterNames);
        addContent(index + personNameIdentifiers.size() + names.size() +
                personDates.size() + professionalAffiliations.size(), professionalAffiliation);
    }

    /**
     * The name of a corporate body which contributed to the creation of the product, unstructured.
     *
     * @param corporateName
     */
    public void setCorporateName(String corporateName)
    {
        this.corporateName.setText(corporateName);
    }

    /**
     * A biographical note about a contributor to the product. (See the &lt;OtherText&gt; composite in Group PR.15 for a
     * biographical note covering all contributors to a product in a single text.) Optional and non-repeating.
     * May occur with a person name or with a corporate name. A biographical note in ONIX should always contain the
     * name of the person or body concerned, and it should always be presented as a piece of continuous text
     * consisting of full sentences. Some recipients of ONIX data feeds will not accept text which has embedded URLs.
     * A contributor website link can be sent using the &lt;Website&gt; composite.
     *
     * @param biographicalNote
     */
    public void setBiographicalNote(String biographicalNote)
    {
        this.biographicalNote.setText(biographicalNote);
    }

    /**
     * A repeatable group of data elements which together identify and provide pointers to a website which is related
     * to the party identified in an occurrence of the &lt;Contributorgt; composite.
     *
     * @param website
     */
    public void addWebsite(Website website)
    {
        websites.add(website);
        int index = indexOf(biographicalNote);
        addContent(index + websites.size(), website);
    }

    /**
     * Brief text describing a contributor to the product, at the publisher's discretion. Optional and non-repeating.
     * It may be used with either a person or corporate name, to draw attention to any aspect of a contributor's
     * background which supports the promotion of the book.
     */
    public void setContributorDescription(String contributorDescription)
    {
        this.contributorDescription.setText(contributorDescription);
    }

    /**
     * An ONIX code allowing a positive indication to be given when authorship is unknown or anonymous,
     * or when as a matter of editorial policy only a limited number of contributors are named.
     * Optional and non-repeating.
     *
     * @param unnamedPersons
     */
    public void setUnnamedPersons(UnnamedPersonsCode unnamedPersons)
    {
        this.unnamedPersons.setOnixValue(unnamedPersons);
    }

    /**
     * A code identifying a country with which a contributor is particularly associated, when this is significant
     * for the marketing of a product. Optional and repeatable.
     *
     * @param countryCode
     */
    public void setCountryCode(CountryCode countryCode)
    {
        this.countryCode.setOnixValue(countryCode);
    }

    /**
     * An ONIX code identifying a region with which a contributor is particularly associated, when this is significant
     * for the marketing of a product. Optional and repeatable.
     */
    public void setRegionCode(RegionCode regionCode)
    {
        this.regionCode.setOnixValue(regionCode);
    }
}
