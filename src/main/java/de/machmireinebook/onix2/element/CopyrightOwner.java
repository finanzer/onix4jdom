package de.machmireinebook.onix2.element;

/**
 * A repeatable group of data elements which together name a copyright owner. At least one occurrence is mandatory in
 * each occurrence of the &lt;CopyrightStatement&gt; composite. Each occurrence of the &lt;CopyrightOwner&gt; composite must carry
 * a single name (personal or corporate), or an identifier, or both.
 */
public class CopyrightOwner extends OnixElement
{
    private CopyrightOwnerIdentifier copyrightOwnerIdentifier;
    private OnixElement<String> personName = new OnixElement<>("PersonName", "b036");
    private OnixElement<String> corporateName = new OnixElement<>("CorporateName", "b047");

    public CopyrightOwner()
    {
        super("CopyrightOwner", "copyrightowner");
        addContent(personName);
        addContent(corporateName);
    }

    public void setCopyrightOwnerIdentifier(CopyrightOwnerIdentifier copyrightOwnerIdentifier)
    {
        this.copyrightOwnerIdentifier = copyrightOwnerIdentifier;
        addContent(0, this.copyrightOwnerIdentifier);
    }

    /**
     *  The name of a person, used here for a personal copyright owner. Optional and non-repeating. Each occurrence of
     *  the &lt;CopyrightOwner&gt; composite may carry a single name (personal or corporate), or an identifier, or both a
     *  name and an identifier.
     *
     * @param personName
     */
    public void setPersonName(String personName)
    {
        this.personName.setOnixValue(personName);
    }
    
    /**
     * The name of a corporate body, used here for a corporate copyright owner. Optional and non-repeating. Each
     * occurrence of the &lt;CopyrightOwner&gt; composite may carry a single name (personal or corporate), or an identifier,
     * or both a name and an identifier.
     * 
     * @param corporateName
     */
    public void setCorporateName(String corporateName)
    {
        this.corporateName.setOnixValue(corporateName);
    }
}
