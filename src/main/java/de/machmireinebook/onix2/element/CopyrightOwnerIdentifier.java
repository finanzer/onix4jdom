package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.NameCodeType;

/**
 * A group of data elements which together represent a coded identification of a copyright owner. Optional, and
 * non-repeating. May be sent either instead of or as well as a name.
 */
public class CopyrightOwnerIdentifier extends OnixElement
{
    private OnixCodeListValueElement<NameCodeType> copyrightOwnerIDType = new OnixCodeListValueElement<>("CopyrightOwnerIDType", "b392", OnixOccurenceType.MANDATORY, NameCodeType::byCode);
    private OnixElement<String> idTypeName = new OnixElement<>("IDTypeName", "b233");
    private OnixElement<String> idValue = new OnixElement<>("IDValue", "b244", OnixOccurenceType.MANDATORY);

    public CopyrightOwnerIdentifier()
    {
        super("CopyrightOwnerIdentifier", "copyrightowneridentifier");
        addContent(copyrightOwnerIDType);
        addContent(idTypeName);
        addContent(idValue);
    }

    /**
     * An ONIX code which identifies the scheme from which the value in the &lt;IDValue&gt; element is taken. Mandatory in
     * each occurrence of the &lt;CopyrightOwnerIdentifier&gt; composite, and non-repeating.
     *
     * @param copyrightOwnerIDType
     */
    public void setCopyrightOwnerIDType(NameCodeType copyrightOwnerIDType)
    {
        this.copyrightOwnerIDType.setOnixValue(copyrightOwnerIDType);
    }

    /**
     * A name which identifies a proprietary identifier scheme when, and only when, the code in the
     * &lt;CopyrightOwnerIDType&gt; element indicates a proprietary scheme. Optional and non-repeating.
     *
     * @param idTypeName the name of idType if CopyrightOwnerIDType element indicates a proprietary scheme
     */
    public void setIdTypeName(String idTypeName)
    {
        this.idTypeName.setText(idTypeName);
    }

    /**
     * A code value taken from the scheme specified in the &lt;CopyrightOwnerIDType&gt; element. Mandatory in each
     * occurrence of the &lt;CopyrightOwnerIdentifier&gt; composite, and non-repeating.
     *
     * @param idValue the value to set
     */
    public void setIdValue(String idValue)
    {
        this.idValue.setText(idValue);
    }

}
