package de.machmireinebook.onix2.element;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

/**
 * An optional and repeatable group of data elements which together represent a structured copyright statement for the
 * product. Either a structured copyright statement or statements, or a copyright year in the separate &lt;CopyrightYear&gt;
 * element which follows the composite, but not both, may be sent.
 */
public class CopyrightStatement extends OnixElement
{
    private List<OnixElement<Year>> copyrightYears = new ArrayList<>();
    private List<CopyrightOwner> copyrightOwners = new ArrayList<>();

    public CopyrightStatement()
    {
        super("CopyrightStatement", "copyrightstatement");
    }

    public void addCopyrightYear(Year year)
    {
        OnixElement<Year> yearOnixElement = new OnixElement<>("CopyrightYear", "b087");
        yearOnixElement.setOnixValue(year);
        copyrightYears.add(yearOnixElement);
        addContent(copyrightYears.size(), yearOnixElement);
    }

    public void addCopyrightOwner(CopyrightOwner copyrightOwner)
    {
        copyrightOwners.add(copyrightOwner);
        addContent(copyrightYears.size() + copyrightOwners.size(),  copyrightOwner);
    }
}
