package de.machmireinebook.onix2.element;

import java.util.Locale;

import de.machmireinebook.codelist.ExtentTypeCode;
import de.machmireinebook.codelist.ExtentUnit;
import de.machmireinebook.stringconverter.NumberStringConverter;

/**
 * A repeatable group of data elements which together describe an extent pertaining to the product.
 */
public class Extent extends OnixElement
{
    private OnixCodeListValueElement<ExtentTypeCode> extentType = new OnixCodeListValueElement<>("ExtentType", "b218", OnixOccurenceType.MANDATORY, ExtentTypeCode::byCode);
    private OnixElement<Number> extentValue = new OnixElement<>("ExtentValue", "b219", OnixOccurenceType.MANDATORY, new NumberStringConverter(Locale.US, "0.0"));
    private OnixCodeListValueElement<ExtentUnit> extentUnit = new OnixCodeListValueElement<>("ExtentUnit", "b220", OnixOccurenceType.MANDATORY, ExtentUnit::byCode);


    public Extent()
    {
        super("Extent", "extent");
        addContent(extentType);
        addContent(extentValue);
        addContent(extentUnit);
    }

    /**
     * An ONIX code which identifies the type of extent carried in the composite, eg running time for an audio or
     * video product. Mandatory in each occurrence of the &lt;Extent&gt; composite, and non-repeating.
     *
     * @param extentType
     */
    public void setExtentType(ExtentTypeCode extentType)
    {
        this.extentType.setOnixValue(extentType);
    }

    /**
     * The numeric value of the extent specified in &lt;ExtentType&gt;. Mandatory in each occurrence of the &lt;Extent&gt;
     *     composite, and non-repeating.
     *
     * @param extentValue
     */
    public void setExtentValue(Number extentValue)
    {
        this.extentValue.setOnixValue(extentValue);
    }

    /**
     * An ONIX code indicating the unit used for the &lt;ExtentValue&gt; and the format in which the value is presented.
     * Mandatory in each occurrence of the &lt;Extent&gt; composite, and non-repeating.
     *
     * @param extentUnit
     */
    public void setExtentUnit(ExtentUnit extentUnit)
    {
        this.extentUnit.setOnixValue(extentUnit);
    }
}
