package de.machmireinebook.onix2.element;


import java.time.LocalDate;

/**
 * User: mjungierek
 * Date: 29.07.2016
 * Time: 15:19
 */
public class Header extends OnixElement
{
    private OnixElement<String> fromCompany = new OnixElement<>("FromCompany", "m174");
    private OnixElement<String> fromPerson = new OnixElement<>("FromPerson", "m175");
    private OnixElement<String> fromEmail = new OnixElement<>("FromEmail", "m283");
    private OnixElement<String> sentDate = new OnixElement<>("SentDate", "m182");

    public Header()
    {
        super("Header", "header");
        addContent(fromCompany);
        addContent(fromPerson);
        addContent(fromEmail);
        addContent(sentDate);
    }

    public void setFromCompany(String fromCompany)
    {
        this.fromCompany.setText(fromCompany);
    }

    public void setFromPerson(String fromPerson)
    {
        this.fromPerson.setText(fromPerson);
    }

    public void setFromEmail(String fromEmail)
    {
        this.fromEmail.setText(fromEmail);
    }

    public void setSentDate(LocalDate localDate)
    {
        this.sentDate.setText(localDate.toString());
    }
}
