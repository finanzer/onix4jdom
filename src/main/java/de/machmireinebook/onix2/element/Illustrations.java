package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.IllustrationAndOtherContentTypeCode;
import de.machmireinebook.stringconverter.IntegerStringConverter;

/**
 * A repeatable group of data elements which together specify the number of illustrations or other content items of a
 * stated type which the product carries. Use of the &lt;Illustrations&gt; composite is optional.
 */
public class Illustrations extends OnixElement
{
    private OnixCodeListValueElement<IllustrationAndOtherContentTypeCode> illustrationType = new OnixCodeListValueElement<>("IllustrationType", "b256", OnixOccurenceType.MANDATORY, IllustrationAndOtherContentTypeCode::byCode);
    private OnixElement<String> illustrationTypeDescription = new OnixElement<>("IllustrationTypeDescription", "b361");
    private OnixElement<Integer> number = new OnixElement<>("Number", "b257", new IntegerStringConverter());

    public Illustrations()
    {
        super("Illustrations", "illustrations");
        addContent(illustrationType);
        addContent(illustrationTypeDescription);
        addContent(number);
    }

    /**
     * An ONIX code which identifies the type of illustration or other content to which an occurrence of the composite
     * refers. Mandatory in each occurrence of the &lt;Illustrations&gt; composite, and non-repeating.
     *
     * @param illustrationType
     */
    public void setIllustrationType(IllustrationAndOtherContentTypeCode illustrationType)
    {
        this.illustrationType.setOnixValue(illustrationType);
    }

    /**
     * Text describing the type of illustration or other content to which an occurrence of the composite refers, when a
     * code is insufficient. Optional and non-repeating. Required when &lt;IllustrationType&gt; carries the value 00.
     *
     * @param illustrationTypeDescription
     */
    public void setIllustrationTypeDescription(String illustrationTypeDescription)
    {
        this.illustrationTypeDescription.setText(illustrationTypeDescription);
    }

    /**
     * The number of illustrations or other content items of the type specified in &lt;IllustrationType&gt;.
     * Optional and non-repeating.
     *
     * @param number
     */
    public void setNumber(Integer number)
    {
        this.number.setOnixValue(number);
    }
}
