package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.CountryCode;
import de.machmireinebook.codelist.LanguageCode;
import de.machmireinebook.codelist.LanguageRoleCode;

/**
 * User: mjungierek
 * Date: 07.08.2016
 * Time: 20:44
 */
public class Language extends OnixElement
{
    private OnixCodeListValueElement<LanguageRoleCode> languageRole = new OnixCodeListValueElement<>("LanguageRole", "b253", OnixOccurenceType.MANDATORY, LanguageRoleCode::byCode);
    private OnixCodeListValueElement<LanguageCode> languageCode = new OnixCodeListValueElement<>("LanguageCode", "b252", OnixOccurenceType.MANDATORY, LanguageCode::byCode);
    private OnixCodeListValueElement<CountryCode> countryCode = new OnixCodeListValueElement<>("CountryCode", "b251", CountryCode::byCode);

    public Language()
    {
        super("Language", "language");
        addContent(languageRole);
        addContent(languageCode);
        addContent(countryCode);
    }

    /**
     * An ONIX code indicating the 'role' of a language in the context of the ONIX record. Mandatory in each occurrence
     * of the &lt;Language&gt; composite, and non-repeating.
     *
     * @param languageRole
     */
    public void setLanguageRole(LanguageRoleCode languageRole)
    {
        this.languageRole.setOnixValue(languageRole);
    }

    /**
     * An ISO code indicating a language. Mandatory in each occurrence of the &lt;Language&gt; composite, and non-repeating.
     *
     * @param languageCode
     */
    public void setLanguageCode(LanguageCode languageCode)
    {
        this.languageCode.setOnixValue(languageCode);
    }

    /**
     * A code identifying the country when this specifies a variant of the language, eg US English. Optional and non-repeating.
     *
     * @param countryCode
     */
    public void setCountryCode(CountryCode countryCode)
    {
        this.countryCode.setOnixValue(countryCode);
    }
}
