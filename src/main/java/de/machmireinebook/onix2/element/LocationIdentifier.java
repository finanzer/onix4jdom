package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.SupplierIdentifierTypeCode;

/**
 * A group of data elements which together define the identifier of a stock location in accordance with a specified
 * scheme, and allowing different types of location identifier to be supported without defining additional data elements.
 * Optional and non-repeating.
 */
public class LocationIdentifier extends OnixElement
{
    private OnixCodeListValueElement<SupplierIdentifierTypeCode> locationIDType = new OnixCodeListValueElement<>("LocationIDType", "j377", OnixOccurenceType.MANDATORY, SupplierIdentifierTypeCode::byCode);
    private OnixElement<String> idTypeName = new OnixElement<>("IDTypeName", "b233");
    private OnixElement<String> idValue = new OnixElement<>("IDValue", "b244", OnixOccurenceType.MANDATORY);

    public LocationIdentifier()
    {
        super("LocationIdentifier", "locationidentifier");
        addContent(locationIDType);
        addContent(idTypeName);
        addContent(idValue);
    }

    /**
     * An ONIX code identifying the scheme from which the identifier in the &lt;IDValue&gt; element is taken. Mandatory in
     * each occurrence of the &lt;LocationIdentifier&gt; composite, and non-repeating.
     *
     * @param locationIDType SupplierIdentifierTypeCode object
     */
    public void setPersonNameIDType(SupplierIdentifierTypeCode locationIDType)
    {
        this.locationIDType.setOnixValue(locationIDType);
    }

    /**
     * A name which identifies a proprietary identifier scheme when the code in the &lt;LocationIDType&gt; element indicates
     * a proprietary scheme, eg a wholesaler’s own code. Optional and non-repeating.
     *
     * @param idTypeName Free text, suggested maximum length 50 characters
     */
    public void setIdTypeName(String idTypeName)
    {
        this.idTypeName.setText(idTypeName);
    }

    /**
     *  An identifier of the type specified in the &lt;LocationIDType&gt; element. Mandatory in each occurrence of the
     *  &lt;LocationIdentifier&gt; composite, and non-repeating.
     *
     * @param idValue
     */
    public void setIdValue(String idValue)
    {
        this.idValue.setText(idValue);
    }
}
