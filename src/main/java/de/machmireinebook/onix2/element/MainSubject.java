package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.MainSubjectSchemeIdentifierCode;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 * User: mjungierek
 * Date: 10.08.2016
 * Time: 23:07
 */
public class MainSubject extends OnixElement
{
    private OnixCodeListValueElement<MainSubjectSchemeIdentifierCode> mainSubjectSchemeIdentifier
            = new OnixCodeListValueElement<>("MainSubjectSchemeIdentifier", "b191", OnixOccurenceType.MANDATORY, MainSubjectSchemeIdentifierCode::byCode);
    private OnixElement<String> subjectSchemeVersion = new OnixElement<>("SubjectSchemeVersion", "b068");
    private OnixElement<String> subjectCode = new OnixElement<>("SubjectCode", "b069");
    private OnixElement<String> subjectHeadingText = new OnixElement<>("SubjectHeadingText", "b070");

    public MainSubject()
    {
        super("MainSubject", "mainsubject");
        addContent(mainSubjectSchemeIdentifier);
        addContent(subjectSchemeVersion);
        addContent(subjectHeadingText);
    }


    /**
     * An ONIX code which identifies a subject scheme which is designated for use in a &lt;MainSubject&gt; composite.
     * Mandatory in each occurrence of the composite, and non-repeating.
     *
     * When the scheme listed in the code list display is annotated 'Code', use the associated &lt;SubjectCode&gt; element to
     * carry the value (if so required, the &lt;SubjectHeadingText&gt; element can be used simultaneously to carry the text
     * equivalent of the code). When the scheme is annotated 'Text', use the &lt;SubjectHeadingText&gt; element to carry
     * the text of the subject heading.
     *
     * @param mainSubjectSchemeIdentifier
     */
    public void setMainSubjectSchemeIdentifier(MainSubjectSchemeIdentifierCode mainSubjectSchemeIdentifier)
    {
        this.mainSubjectSchemeIdentifier.setOnixValue(mainSubjectSchemeIdentifier);
    }

    /**
     * A number which identifies a version or edition of the subject scheme specified in the associated
     * &lt;MainSubjectSchemeIdentifier&gt; element. Optional and non-repeating.
     *
     * @param subjectSchemeVersion
     */
    public void setSubjectSchemeVersion(String subjectSchemeVersion)
    {
        this.subjectSchemeVersion.setText(subjectSchemeVersion);
    }

    /**
     * A subject class or category code from the scheme specified in the &lt;MainSubjectSchemeIdentifier&gt; element. Either
     * &lt;SubjectCode&gt; or &lt;SubjectHeadingText&gt; or both must be present in each occurrence of the &lt;MainSubject&gt;
     * composite. Non-repeating.
     */
    public void setSubjectCode(String subjectCode)
    {
        this.subjectCode.setText(subjectCode);
    }

    /**
     * The text of a heading taken from the scheme specified in the &lt;MainSubjectSchemeIdentifier&gt; element; or the text
     * equivalent to the &lt;SubjectCode&gt; value, if both code and text are sent. Either &lt;SubjectCode&gt; or
     * &lt;SubjectHeadingText&gt; or both must be present in each occurrence of the &lt;MainSubject&gt; composite. Non-repeating.
     *
     * @param subjectHeadingText
     */
    public void setSubjectHeadingText(String subjectHeadingText)
    {
        this.subjectHeadingText.setText(subjectHeadingText);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        result.setValid(result.isValid() && StringUtils.isNotEmpty(subjectCode.getText()) || StringUtils.isNotEmpty(subjectHeadingText.getText()));
        return result;
    }
}
