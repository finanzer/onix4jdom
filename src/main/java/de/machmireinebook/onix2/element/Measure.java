package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.MeasureTypeCode;
import de.machmireinebook.codelist.MeasureUnitCode;
import de.machmireinebook.stringconverter.NumberStringConverter;

/**
 * An optional and repeatable group of data elements which together identify a measurement and the units in which it is expressed.
 */
public class Measure extends OnixElement
{
    private OnixCodeListValueElement<MeasureTypeCode> measureTypeCode = new OnixCodeListValueElement<>("MeasureTypeCode", "c093", OnixOccurenceType.MANDATORY, MeasureTypeCode::byCode);
    private OnixElement<Number> measurement = new OnixElement<>("Measurement", "c094", OnixOccurenceType.MANDATORY, new NumberStringConverter());
    private OnixCodeListValueElement<MeasureUnitCode> measureUnitCode = new OnixCodeListValueElement<>("MeasureUnitCode", "c095", OnixOccurenceType.MANDATORY, MeasureUnitCode::byCode);

    public Measure()
    {
        super("Measure", "measure");
        addContent(measureTypeCode);
        addContent(measurement);
        addContent(measureUnitCode);
    }

    /**
     * An ONIX code indicating the dimension which is specified by an occurrence of the measure composite. Mandatory in
     * each occurrence of the &lt;Measure&gt; composite, and non-repeating.
     *
     * @param measureTypeCode
     */
    public void setMeasureTypeCode(MeasureTypeCode measureTypeCode)
    {
        this.measureTypeCode.setOnixValue(measureTypeCode);
    }

    /**
     * The number which represents the dimension specified in &lt;MeasureTypeCode&gt; in the measure units specified in
     * &lt;MeasureUnitCode&gt;. Mandatory in each occurrence of the &lt;Measure&gt; composite, and non-repeating.
     *
     * @param measurement
     */
    public void setMeasurement(Number measurement)
    {
        this.measurement.setOnixValue(measurement);
    }

    /**
     * An ONIX code indicating the measure unit in which dimensions are given. Mandatory in each occurrence of the
     * &lt;Measure&gt; composite, and non-repeating. This element must follow the dimension to which the measure unit applies.
     *
     * @param measureUnitCode
     */
    public void setMeasureUnitCode(MeasureUnitCode measureUnitCode)
    {
        this.measureUnitCode.setOnixValue(measureUnitCode);
    }
}
