package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.codelist.PersonOrganizationNameTypes;

/**
 * User: mjungierek
 * Date: 31.07.2016
 * Time: 21:13
 */
public class Name extends OnixElement
{
    private OnixCodeListValueElement<PersonOrganizationNameTypes> personNameType = new OnixCodeListValueElement<>("PersonNameType", "b250", OnixOccurenceType.MANDATORY, PersonOrganizationNameTypes::byCode);
    private OnixElement<String> personName = new OnixElement<>("PersonName", "b036");
    private OnixElement<String> personNameInverted = new OnixElement<>("PersonNameInverted", "b037");
    private OnixElement<String> titlesBeforeNames = new OnixElement<>("TitlesBeforeNames", "b038");
    private OnixElement<String> namesBeforeKey = new OnixElement<>("NamesBeforeKey", "b039");
    private OnixElement<String> prefixToKey = new OnixElement<>("PrefixToKey", "b247");
    private OnixElement<String> keyNames = new OnixElement<>("KeyNames", "b040");
    private OnixElement<String> namesAfterKey = new OnixElement<>("NamesAfterKey", "b041");
    private OnixElement<String> suffixToKey = new OnixElement<>("SuffixToKey", "b248");
    private OnixElement<String> lettersAfterNames = new OnixElement<>("LettersAfterNames", "b042");
    private OnixElement<String> titlesAfterNames = new OnixElement<>("TitlesAfterNames", "b043");
    private List<PersonNameIdentifier> personNameIdentifiers = new ArrayList<>();

    public Name()
    {
        super("Name", "name");
        addContent(personNameType);
        addContent(personName);
        addContent(personNameInverted);
        addContent(titlesBeforeNames);
        addContent(namesBeforeKey);
        addContent(prefixToKey);
        addContent(keyNames);
        addContent(namesAfterKey);
        addContent(suffixToKey);
        addContent(lettersAfterNames);
        addContent(titlesAfterNames);
    }

    public void setPersonNameType(PersonOrganizationNameTypes personNameType)
    {
        this.personNameType.setOnixValue(personNameType);
    }

    public void setPersonName(String personName)
    {
        this.personName.setText(personName);
    }

    public void setPersonNameInverted(String personNameInverted)
    {
        this.personNameInverted.setText(personNameInverted);
    }

    public void setTitlesBeforeNames(String titlesBeforeNames)
    {
        this.titlesBeforeNames.setText(titlesBeforeNames);
    }

    public void setNamesBeforeKey(String namesBeforeKey)
    {
        this.namesBeforeKey.setText(namesBeforeKey);
    }

    public void setPrefixToKey(String prefixToKey)
    {
        this.prefixToKey.setText(prefixToKey);
    }

    public void setKeyNames(String keyNames)
    {
        this.keyNames.setText(keyNames);
    }

    public void setNamesAfterKey(String namesAfterKey)
    {
        this.namesAfterKey.setText(namesAfterKey);
    }

    public void setSuffixToKey(String suffixToKey)
    {
        this.suffixToKey.setText(suffixToKey);
    }

    public void setLettersAfterNames(String lettersAfterNames)
    {
        this.lettersAfterNames.setText(lettersAfterNames);
    }

    public void setTitlesAfterNames(String titlesAfterNames)
    {
        this.titlesAfterNames.setText(titlesAfterNames);
    }

    public void addPersonNameIdentifier(PersonNameIdentifier personNameIdentifier)
    {
        personNameIdentifiers.add(personNameIdentifier);
        int index = indexOf(lettersAfterNames);
        addContent(index + personNameIdentifiers.size(), personNameIdentifier);
    }
}
