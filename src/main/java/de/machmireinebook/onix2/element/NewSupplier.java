package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.IllegalOnixDataException;

import org.apache.commons.lang3.StringUtils;

/**
 * A group of data elements which together specify a new supply source to which orders are referred. Use only when the
 * code in &lt;ProductAvailability&gt; or &lt;AvailabilityCode&gt; indicates “no longer available from us, refer to new supplier”.
 * Only one occurrence of the composite is permitted in this context.
 */
public class NewSupplier extends OnixElement
{
    private OnixElement<String> supplierEANLocationNumber = new OnixElement<>("SupplierEANLocationNumber", "j135");
    private OnixElement<String> supplierSAN = new OnixElement<>("SupplierSAN", "j136");
    private List<SupplierIdentifier> supplierIdentifiers = new ArrayList<>();
    private OnixElement<String> supplierName = new OnixElement<>("SupplierName", "j137");
    private OnixElement<String> telephoneNumber = new OnixElement<>("TelephoneNumber", "j270");
    private OnixElement<String> faxNumber = new OnixElement<>("FaxNumber", "j271");
    private OnixElement<String> emailAddress = new OnixElement<>("EmailAddress", "j272");

    public NewSupplier()
    {
        super("NewSupplier", "newsupplier");
        addContent(supplierEANLocationNumber);
        addContent(supplierSAN);
        addContent(supplierName);
        addContent(telephoneNumber);
        addContent(faxNumber);
        addContent(emailAddress);
    }

    /**
     * An EAN-13 location number identifying a supply source from which the product may be ordered by a trade customer.
     * Now also known as an “EAN-UCC Global Location Number” or GLN. Optional, but each occurrence of the &lt;NewSupplier&gt;
     * composite must carry either at least one supplier identifier, or a &lt;SupplierName&gt;.
     *
     * @param supplierEANLocationNumber Fixed-length, thirteen numeric digits, of which the last is a check digit.
     */
    public void setSupplierEANLocationNumber(String supplierEANLocationNumber)
    {
        if (supplierEANLocationNumber.length() != 13 || !StringUtils.isNumeric(supplierEANLocationNumber))
        {
            throw new IllegalOnixDataException("SupplierEANLocationNumber is a fixed-length number, with thirteen digits, of which the last is a check digit.");
        }
        this.supplierEANLocationNumber.setText(supplierEANLocationNumber);
    }

    /**
     * The book trade Standard Address Number that identifies the supplier with whom trade orders for the product
     * should be placed. Used in the US and UK. Optional, but each occurrence of the &lt;NewSupplier&gt; composite must
     * carry either at least one supplier identifier, or a &lt;SupplierName&gt;.
     *
     * @param supplierSAN
     */
    public void setSupplierSAN(String supplierSAN)
    {
        if (supplierSAN.length() != 7)
        {
            throw new IllegalOnixDataException("SupplierSAN is a fixed-length string, with seven digits or six digits and the letter X.");
        }
        this.supplierSAN.setText(supplierSAN);
    }

    /**
     * A repeatable group of data elements which together define the identifier of a supplier in accordance with a
     * specified scheme, and allowing different types of supplier identifier to be included without defining additional
     * data elements. Optional, but each occurrence of the &lt;NewSupplier&gt; composite must carry either at least
     * one supplier identifier, or a &lt;SupplierName&gt;.
     *
     * @param supplierIdentifier
     */
    public void addSupplierIdentifier(SupplierIdentifier supplierIdentifier)
    {
        supplierIdentifiers.add(supplierIdentifier);
        int index = indexOf(supplierSAN);
        addContent(index + supplierIdentifiers.size(), supplierIdentifier);
    }

    /**
     * The name of a supply source from which the product may be ordered by a trade customer. Optional and non-repeating;
     * required if no supplier identifier is sent.
     *
     * @param supplierName Variable-length text, suggested maximum length 100 characters
     */
    public void setSupplierName(String supplierName)
    {
        this.supplierName.setText(supplierName);
    }

    /**
     * A telephone number of a supply source from which the product may be ordered by a trade customer. Optional and repeatable.
     *
     * @param telephoneNumber Variable-length text, suggested maximum length 20 characters
     */
    public void setTelephoneNumber(String telephoneNumber)
    {
        this.telephoneNumber.setText(telephoneNumber);
    }

    /**
     * A fax number of a supply source from which the product may be ordered by a trade customer. Optional and repeatable.
     *
     * @param faxNumber Variable-length text, suggested maximum length 20 characters
     */
    public void setFaxNumber(String faxNumber)
    {
        this.faxNumber.setText(faxNumber);
    }

    /**
     * An email address for a supply source from which the product may be ordered by a trade customer. Optional and repeatable.
     *
     * @param emailAddress Variable-length text, suggested maximum length 100 characters
     */
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress.setText(emailAddress);
    }
}
