package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.codelist.CountryCode;
import de.machmireinebook.codelist.RegionCode;
import de.machmireinebook.validation.ValidationMessage;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 * A repeatable group of data elements which together identify a country or countries in which the product is not for
 * sale, together with the ISBN and/or other product identifier and/or the name of the publisher of the same work in
 * the specified country/ies.
 */
public class NotForSale extends OnixElement
{
    private OnixCodeListValueElement<CountryCode> rightsCountry = new OnixCodeListValueElement<>("RightsCountry", "b090", CountryCode::byCode);
    private OnixCodeListValueElement<RegionCode> rightsTerritory = new OnixCodeListValueElement<>("RightsTerritory", "b388", RegionCode::byCode);
    private List<ProductIdentifier> productIdentifiers = new ArrayList<>();
    private OnixElement<String> recordReference = new OnixElement<>("PublisherName", "b081");

    public NotForSale()
    {
        super("NotForSale", "notforsale");
        addContent(rightsCountry);
        addContent(rightsTerritory);
    }

    /**
     * One or more ISO standard codes identifying a country. Successive codes may be separated by spaces. Thus, a
     * single occurrence of the element can carry an unlimited number of country codes, for countries for which details
     * of another publisher's product are given. For upwards compatibility, the element remains repeatable, so that
     * multiple countries can also be listed as multiple occurrences of the whole element. At least one occurrence of
     * &lt;RightsCountry&gt; or &lt;RightsTerritory&gt; is mandatory in each occurrence of the &lt;NotForSale&gt; composite.
     *
     * @param rightsCountry
     */
    public void setRightsCountry(CountryCode rightsCountry)
    {
        this.rightsCountry.setOnixValue(rightsCountry);
    }

    /**
     * One or more ONIX codes identifying a territory which is not a country, but which is precisely defined in
     * geographical terms, eg World, Northern Ireland, Australian National Territory. Successive codes
     * are separated by spaces, so that the element can carry an unlimited number of territory codes, for territories
     * for which details of another publisher's product are given. Optional and non-repeating.
     *
     * @param rightsTerritory
     */
    public void setRightsTerritory(RegionCode rightsTerritory)
    {
        this.rightsTerritory.setOnixValue(rightsTerritory);
    }

    /**
     * A repeatable group of data elements which together define the identifier of a product in accordance with a s
     * pecified scheme, and allowing new types of product identifier to be included without defining additional data
     * elements.
     *
     * @param productIdentifier
     */
    public void addProductIdentifier(ProductIdentifier productIdentifier)
    {
        productIdentifiers.add(productIdentifier);
        int index = indexOf(rightsTerritory);
        addContent(index + productIdentifiers.size(), productIdentifier);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        if (StringUtils.isEmpty(rightsCountry.getText()) && StringUtils.isEmpty(rightsTerritory.getText()))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("NotForSale", "At least one occurrence of " +
                    " <RightsCountry> or <RightsTerritory> is mandatory in any occurrence of the<NotForSale> composite.",
                    "RightsCountry", "RightsTerritory"));
        }

        return result;
    }

}
