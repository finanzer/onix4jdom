package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;

/**
 * User: mjungierek
 * Date: 29.07.2016
 * Time: 13:58
 */
public class ONIXMessage extends Document
{
    private Header header = new Header();
    private OnixElement noProduct = new OnixElement("NoProduct", "noproduct", OnixOutputType.SHORT);
    private List<Product> products = new ArrayList<>();

    public ONIXMessage()
    {
        Element root = new Element("ONIXMessage");
        setRootElement(root);
        root.addContent(header);
        root.addContent(noProduct);
        setDocType(new DocType("ONIXMessage", "http://www.editeur.org/onix/2.1/reference/onix-international.dtd"));
    }

    public Header getHeader()
    {
        return header;
    }

    public void addProduct(Product product)
    {
        if (products.isEmpty())
        {
            getRootElement().removeContent(noProduct);
        }
        products.add(product);
        getRootElement().addContent(product);
    }


}
