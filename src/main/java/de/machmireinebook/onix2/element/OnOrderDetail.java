package de.machmireinebook.onix2.element;

import java.time.LocalDate;

import de.machmireinebook.stringconverter.IntegerStringConverter;
import de.machmireinebook.stringconverter.LocalDateStringConverter;

/**
 * A repeatable group of data elements which together specify details of a stock shipment currently awaited,
 * normally from overseas.
 */
public class OnOrderDetail extends OnixElement
{
    private OnixElement<Integer> onOrder = new OnixElement<>("OnOrder", "j351", OnixOccurenceType.MANDATORY, new IntegerStringConverter());
    private OnixElement<LocalDate> expectedDate = new OnixElement<>("ExpectedDate", "j302", OnixOccurenceType.MANDATORY, new LocalDateStringConverter());

    public OnOrderDetail()
    {
        super("OnOrderDetail", "onorderdetail");
        addContent(onOrder);
        addContent(expectedDate);
    }

    /**
     * The quantity of stock on order. Mandatory in each occurrence of the &lt;OnOrderDetail&gt; composite, and non-repeating.
     *
     * @param onOrder Variable-length integer, suggested maximum length 7 digits
     */
    public void setOnOrder(Integer onOrder)
    {
        this.onOrder.setOnixValue(onOrder);
    }

    /**
     *  The date on which a stock shipment is expected. Mandatory in each occurrence of the &lt;OnOrderDetail&gt; composite, and non-repeating.
     *
     * @param expectedDate LocalDate object
     */
    public void setExpectedDate(LocalDate expectedDate)
    {
        this.expectedDate.setOnixValue(expectedDate);
    }
}
