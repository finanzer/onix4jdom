package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.OnixCodeList;
import de.machmireinebook.codelist.OnixCodeListFactory;
import de.machmireinebook.stringconverter.OnixCodeListStringConverter;

/**
 * An onix element build by a code list. This class differs from OnixElement only by special constructors for setting
 * OnixCodeListFactory.
 */
public class OnixCodeListValueElement<T extends OnixCodeList> extends OnixElement<T>
{
    public OnixCodeListValueElement(String name, String shortTagName, OnixCodeListFactory<T> factory)
    {
        super(name, shortTagName, new OnixCodeListStringConverter<>(factory));
    }

    public OnixCodeListValueElement(String name, String shortTagName, OnixOccurenceType occurenceType, OnixCodeListFactory<T> factory)
    {
        super(name, shortTagName, occurenceType, new OnixCodeListStringConverter<>(factory));
    }
}
