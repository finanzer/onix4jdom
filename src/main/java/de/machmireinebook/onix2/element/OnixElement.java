package de.machmireinebook.onix2.element;

import java.util.Arrays;
import java.util.List;

import de.machmireinebook.stringconverter.StringConverter;
import de.machmireinebook.validation.ValidationResult;

import org.jdom2.Element;
import org.jdom2.IllegalNameException;

/**
 * A special jdom element for wrapping the specific onix datatypes.
 */
public class OnixElement<T> extends Element
{
    private final List<String> allowedAttributes = Arrays.asList("datestamp", "sourcename", "sourcetype", "collationkey",
                                            "dateformat", "language", "release", "textcase", "textformat", "textscript");
    private String shortTagName;
    private OnixOccurenceType occurenceType = OnixOccurenceType.OPTIONAL;
    private OnixOutputType outputType  = OnixOutputType.OMIT;

    private StringConverter<T> stringConverter;

    public OnixElement(String name, String shortTagName)
    {
        super(name);
        this.shortTagName = shortTagName;
    }

    public OnixElement(String name, String shortTagName, StringConverter<T> stringConverter)
    {
        super(name);
        this.shortTagName = shortTagName;
        this.stringConverter = stringConverter;
    }

    public OnixElement(String name, String shortTagName, OnixOccurenceType occurenceType)
    {
        super(name);
        this.shortTagName = shortTagName;
        this.occurenceType = occurenceType;
    }

    public OnixElement(String name, String shortTagName, OnixOccurenceType occurenceType, StringConverter<T> stringConverter)
    {
        super(name);
        this.shortTagName = shortTagName;
        this.occurenceType = occurenceType;
        this.stringConverter = stringConverter;
    }

    public OnixElement(String name, String shortTagName, OnixOutputType outputType)
    {
        super(name);
        this.shortTagName = shortTagName;
        this.outputType = outputType;
    }

    public void setDatestamp(String value)
    {
        setAttribute("datestamp", value);
    }

    public void setSourcename(String value)
    {
        setAttribute("sourcename", value);
    }

    public void setSourcetype(String value)
    {
        setAttribute("sourcetype", value);
    }

    /**
     * @throws IllegalNameException If an attribute name is used, that is not allowed in onix this exception will be thrown.
     * @param name
     * @param value
     * @return this element modified
     */
    @Override
    public Element setAttribute(String name, String value)
    {
        if (!allowedAttributes.contains(name))
        {
            throw new IllegalNameException(name + " is not an allowed onix attribute name");
        }
        return super.setAttribute(name, value);
    }

    public String getShortTagName()
    {
        return shortTagName;
    }

    public void setShortTagName(String shortTagName)
    {
        this.shortTagName = shortTagName;
    }

    public OnixOccurenceType getOccurenceType()
    {
        return occurenceType;
    }

    public OnixOutputType getOutputType()
    {
        return outputType;
    }

    public StringConverter<T> getStringConverter()
    {
        return stringConverter;
    }

    public void setStringConverter(StringConverter<T> stringConverter)
    {
        this.stringConverter = stringConverter;
    }

    /**
     * Sets the specific onix type T as data to this element. Internally the onix value will be converted to text and set to setText() method via the
     * stringconverter. If no StringConverter is set, value.toString() will be used.
     */
    public void setOnixValue(T value)
    {
        String text;
        if (value == null)
        {
            text = "";
        }
        else
        {
            if (stringConverter == null)
            {
                text = value.toString();
            }
            else
            {
                text = stringConverter.toString(value);
            }
        }
        setText(text);
    }

    public T getOnixValue()
    {
        return stringConverter.fromString(getText());
    }

    public ValidationResult validate()
    {
        return new ValidationResult();
    }


}
