package de.machmireinebook.onix2.element;

/**
 * User: mjungierek
 * Date: 30.07.2016
 * Time: 20:53
 */
public enum OnixOccurenceType
{
    MANDATORY,
    OPTIONAL
}
