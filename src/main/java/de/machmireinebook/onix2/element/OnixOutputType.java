package de.machmireinebook.onix2.element;

/**
 * User: mjungierek
 * Date: 31.07.2016
 * Time: 11:52
 */
public enum OnixOutputType
{
    OMIT,
    SHORT
}
