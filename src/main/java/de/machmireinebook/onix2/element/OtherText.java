package de.machmireinebook.onix2.element;

import java.time.LocalDate;

import de.machmireinebook.codelist.OtherTextTypeCode;
import de.machmireinebook.codelist.TextFormatCode;
import de.machmireinebook.codelist.TextLinkTypeCode;
import de.machmireinebook.stringconverter.LocalDateStringConverter;
import de.machmireinebook.validation.ValidationMessage;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 * User: mjungierek
 * Date: 12.08.2016
 * Time: 21:13
 */
public class OtherText extends OnixElement
{
    private OnixCodeListValueElement<OtherTextTypeCode> textTypeCode = new OnixCodeListValueElement<>("TextTypeCode", "d102", OtherTextTypeCode::byCode);
    private OnixCodeListValueElement<TextFormatCode> textFormat = new OnixCodeListValueElement<>("TextFormatCode", "d103", TextFormatCode::byCode);
    private OnixElement<String> text = new OnixElement<>("Text", "d104");
    private OnixCodeListValueElement<TextLinkTypeCode> textLinkType = new OnixCodeListValueElement<>("TextLinkType", "d105", TextLinkTypeCode::byCode);
    private OnixElement<String> textLink = new OnixElement<>("TextLink", "d106");
    private OnixElement<String> textAuthor = new OnixElement<>("TextAuthor", "d107");
    private OnixElement<String> textSourceCorporate = new OnixElement<>("TextSourceCorporate", "b374");
    private OnixElement<String> textSourceTitle = new OnixElement<>("TextSourceTitle", "d108");
    private OnixElement<LocalDate> textPublicationDate = new OnixElement<>("TextPublicationDate", "d109", new LocalDateStringConverter());
    private OnixElement<LocalDate> startDate = new OnixElement<>("StartDate", "b324", new LocalDateStringConverter("yyyyMMdd"));
    private OnixElement<LocalDate> endDate = new OnixElement<>("EndDate", "b325", new LocalDateStringConverter("yyyyMMdd"));

    public OtherText()
    {
        super("OtherText", "othertext");
        addContent(textTypeCode);
        addContent(textFormat);
        addContent(text);
        addContent(textLinkType);
        addContent(textLink);
        addContent(textAuthor);
        addContent(textSourceCorporate);
        addContent(textSourceTitle);
        addContent(textPublicationDate);
        addContent(startDate);
        addContent(endDate);
    }

    /**
     * An ONIX code which identifies the type of text which is sent in the &lt;Text&gt; element, or referenced in the
     * &lt;TextLink&gt; element. Mandatory in each occurrence of the &lt;OtherText&gt; composite, and non-repeating.
     *
     * @param textTypeCode
     */
    public void setTextTypeCode(OtherTextTypeCode textTypeCode)
    {
        this.textTypeCode.setOnixValue(textTypeCode);
    }

    /**
     * An ONIX code which identifies the format of text which is sent in the &lt;Text&gt; element, or referenced in the
     * &lt;TextLink&gt; element. Optional and non-repeating. <b>It is now possible to use a 'textformat' attribute in the
     * &lt;Text&gt; element for this purpose, and this is the recommended practise when the text is sent in the ONIX record.
     * The &lt;TextFormat&gt; element may still be used when the text is held outside the ONIX record, and referenced by the
     * &lt;TextLink&gt; element.</b>
     *
     * @param textFormat
     */
    public void setTextFormat(TextFormatCode textFormat)
    {
        this.textFormat.setOnixValue(textFormat);
    }

    /**
     * The text specified in the &lt;TextTypeCode&gt; element, if it is suitable to be sent in full as part of the ONIX record.
     * Either the &lt;Text&gt; element or both of the &lt;TextLinkType&gt; and &lt;TextLink&gt; elements must be present in any occurrence
     * of the &lt;OtherText&gt; composite. Non-repeating.
     *
     * The &lt;Text&gt; element may carry any of the following ONIX attributes: textformat, language, transliteration, textcase.
     *
     * @param text
     */
    public void setTextValue(String text)
    {
        this.text.setText(text);
    }

    /**
     * An ONIX code which identifies the type of link which is given in the &lt;TextLink&gt; element.
     *
     * @param textLinkType
     */
    public void setTextLinkType(TextLinkTypeCode textLinkType)
    {
        this.textLinkType.setOnixValue(textLinkType);
    }

    /**
     * A link to the text item specified in the &lt;TextTypeCode&gt; element, using the link type specified in &lt;TextLinkType&gt;.
     *
     * @param textLink
     */
    public void setTextLink(OnixElement<String> textLink)
    {
        this.textLink = textLink;
    }

    /**
     * The name of the author of text sent in the &lt;Text&gt; element, or referenced in the &lt;TextLink&gt; element,
     * eg if it is a review or promotional quote.
     *
     * @param textAuthor
     */
    public void setTextAuthor(String textAuthor)
    {
        this.textAuthor.setText(textAuthor);
    }

    /**
     * The name of a company or corporate body responsible for the text sent in the &lt;Text&gt; element, or referenced in
     * the &lt;TextLink&gt; element, eg if it is part of a Reading Group Guide. Optional and non-repeating.
     *
     * @param textSourceCorporate
     */
    public void setTextSourceCorporate(String textSourceCorporate)
    {
        this.textSourceCorporate.setText(textSourceCorporate);
    }

    /**
     * The title of a publication from which the text sent in the &lt;Text&gt; element, or referenced in the &lt;TextLink&gt;
     * element, was taken, eg if it is a review quote. Optional and non-repeating.
     *
     * @param textSourceTitle
     */
    public void setTextSourceTitle(String textSourceTitle)
    {
        this.textSourceTitle.setText(textSourceTitle);
    }

    /**
     * The date on which text sent in the &lt;Text&gt; element, or referenced in the &lt;TextLink&gt; element, was published.
     * Optional and non-repeating.
     *
     * @param textPublicationDate
     */
    public void setTextPublicationDate(OnixElement<LocalDate> textPublicationDate)
    {
        this.textPublicationDate = textPublicationDate;
    }

    /**
     * The date from which text sent in the &lt;Text&gt; element, or referenced in the &lt;TextLink&gt; element, is intended to be
     * used, eg for date-limited promotions. Optional and non-repeating, but either both or neither of &lt;StartDate&gt;
     * and &lt;EndDate&gt; must be present.
     *
     * @param startDate
     */
    public void setStartDate(LocalDate startDate)
    {
        this.startDate.setOnixValue(startDate);
    }

    /**
     * The date until which text sent in the &lt;Text&gt; element, or referenced in the &lt;TextLink&gt; element, is intended to
     * be used, eg for date-limited promotions. Optional and non-repeating, but either both or neither of
     * &lt;StartDate&gt; and &lt;EndDate&gt; must be present.
     *
     * @param endDate
     */
    public void setEndDate(LocalDate endDate)
    {
        this.endDate.setOnixValue(endDate);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        if(!(StringUtils.isNotEmpty(text.getText()) || (StringUtils.isNotEmpty(textLinkType.getText()) && StringUtils.isNotEmpty(textLink.getText()))))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("OtherText", "Either the <Text> element or both of the <TextLinkType> and <TextLink> elements must be present in any occurrence\n" +
                    "     * of the <OtherText> composite", "Text", "TextLinkType", "TextLink"));
        }

        if(!((StringUtils.isEmpty(startDate.getText()) && StringUtils.isEmpty(endDate.getText())) ||
                (StringUtils.isNotEmpty(startDate.getText()) && StringUtils.isNotEmpty(endDate.getText()))))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("OtherText", "both or neither of " +
                    " <StartDate> and <EndDate> must be present", "StartDate", "EndDate"));
        }
        return result;
    }
}
