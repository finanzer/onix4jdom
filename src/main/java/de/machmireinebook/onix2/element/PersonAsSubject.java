package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

/**
 * An optional and repeatable group of data elements which together represent the name of a person who is part of the
 * subject of a product.
 *
 * Within the &lt;PersonAsSubject&gt; composite, elements defined for person names as contributors may be used as follows:
 * <ul>
 *     <li>PR.8.5 Person name, for an unstructured name in normal order, eg John F. Kennedy</li>
 *      <li>PR.8.6 Person name, inverted, for an unstructured name in inverted order, eg Kennedy, John F.</li>
 * <li>PR.8.7 to PR.8.14 Person name parts 1 to 8, for a fully structured name</li>
 * <li>&lt;PersonNameIdentifier&gt; composite, PR.8.15 to PR.8.17, for a coded name identifier.</li>
 * </ul>
 A valid occurrence of the &lt;PersonAsSubject&gt; composite should carry one or more of these forms. Any combination
 is permitted, provided that only the &lt;PersonNameIdentifier&gt; composite is repeatable, in the unlikely event that more
 than one coded identifier is given for the same person.
 */
public class PersonAsSubject extends OnixElement
{
    private OnixElement personName = new OnixElement("PersonName", "b036");
    private OnixElement personNameInverted = new OnixElement("PersonNameInverted", "b037");
    private OnixElement titlesBeforeNames = new OnixElement("TitlesBeforeNames", "b038");
    private OnixElement namesBeforeKey = new OnixElement("NamesBeforeKey", "b039");
    private OnixElement prefixToKey = new OnixElement("PrefixToKey", "b247");
    private OnixElement keyNames = new OnixElement("KeyNames", "b040");
    private OnixElement namesAfterKey = new OnixElement("NamesAfterKey", "b041");
    private OnixElement suffixToKey = new OnixElement("SuffixToKey", "b248");
    private OnixElement lettersAfterNames = new OnixElement("LettersAfterNames", "b042");
    private OnixElement titlesAfterNames = new OnixElement("TitlesAfterNames", "b043");
    private List<PersonNameIdentifier> personNameIdentifiers = new ArrayList<>();

    public PersonAsSubject()
    {
        super("PersonAsSubject", "personassubject");
        addContent(personName);
        addContent(personNameInverted);
        addContent(titlesBeforeNames);
        addContent(namesBeforeKey);
        addContent(prefixToKey);
        addContent(keyNames);
        addContent(namesAfterKey);
        addContent(suffixToKey);
        addContent(lettersAfterNames);
        addContent(titlesAfterNames);
    }

    public void setPersonName(String personName)
    {
        this.personName.setText(personName);
    }

    public void setPersonNameInverted(String personNameInverted)
    {
        this.personNameInverted.setText(personNameInverted);
    }

    public void setTitlesBeforeNames(String titlesBeforeNames)
    {
        this.titlesBeforeNames.setText(titlesBeforeNames);
    }

    public void setNamesBeforeKey(String namesBeforeKey)
    {
        this.namesBeforeKey.setText(namesBeforeKey);
    }

    public void setPrefixToKey(String prefixToKey)
    {
        this.prefixToKey.setText(prefixToKey);
    }

    public void setKeyNames(String keyNames)
    {
        this.keyNames.setText(keyNames);
    }

    public void setNamesAfterKey(String namesAfterKey)
    {
        this.namesAfterKey.setText(namesAfterKey);
    }

    public void setSuffixToKey(String suffixToKey)
    {
        this.suffixToKey.setText(suffixToKey);
    }

    public void setLettersAfterNames(String lettersAfterNames)
    {
        this.lettersAfterNames.setText(lettersAfterNames);
    }

    public void setTitlesAfterNames(String titlesAfterNames)
    {
        this.titlesAfterNames.setText(titlesAfterNames);
    }

    public void addPersonNameIdentifier(PersonNameIdentifier personNameIdentifier)
    {
        personNameIdentifiers.add(personNameIdentifier);
        int index = indexOf(lettersAfterNames);
        addContent(index + personNameIdentifiers.size(), personNameIdentifier);
    }

}
