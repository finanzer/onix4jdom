package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.DateFormatCode;
import de.machmireinebook.codelist.PersonDateRoleCode;

/**
 *
 */
public class PersonDate extends OnixElement
{
    private OnixCodeListValueElement<PersonDateRoleCode> personDateRole = new OnixCodeListValueElement<>("PersonDateRole", "b305", OnixOccurenceType.MANDATORY, PersonDateRoleCode::byCode);
    private OnixCodeListValueElement<DateFormatCode> dateFormat = new OnixCodeListValueElement<>("DateFormat", "j260", DateFormatCode::byCode);
    private OnixElement date = new OnixElement("Date", "b306");

    public PersonDate()
    {
        super("PersonDate", "persondate");
        addContent(personDateRole);
        addContent(dateFormat);
        addContent(date);
    }

    public void setPersonDateRole(PersonDateRoleCode personDateRole)
    {
        this.personDateRole.setOnixValue(personDateRole);
    }

    public void setDateFormat(DateFormatCode dateFormat)
    {
        this.dateFormat.setOnixValue(dateFormat);
    }

    public void setDate(String date)
    {
        this.date.setText(date);
    }
}
