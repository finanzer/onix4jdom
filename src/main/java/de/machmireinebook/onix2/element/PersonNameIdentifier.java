package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.PersonNameIdentifierTypeCode;

/**
 *
 */
public class PersonNameIdentifier extends OnixElement
{
    private OnixCodeListValueElement<PersonNameIdentifierTypeCode> personNameIDType = new OnixCodeListValueElement<>("PersonNameIDType", "b390", OnixOccurenceType.MANDATORY, PersonNameIdentifierTypeCode::byCode);
    private OnixElement idTypeName = new OnixElement("IDTypeName", "b233");
    private OnixElement idValue = new OnixElement("IDValue", "b244", OnixOccurenceType.MANDATORY);

    public PersonNameIdentifier()
    {
        super("PersonNameIdentifier", "personnameidentifier");
        addContent(personNameIDType);
        addContent(idTypeName);
        addContent(idValue);
    }

    public void setPersonNameIDType(PersonNameIdentifierTypeCode productIDType)
    {
        this.personNameIDType.setOnixValue(productIDType);
    }

    public void setIdTypeName(String idTypeName)
    {
        this.idTypeName.setText(idTypeName);
    }

    public void setIdValue(String idValue)
    {
        this.idValue.setText(idValue);
    }
}
