package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.PriceTypeCode;
import de.machmireinebook.codelist.PriceTypeQualifierCode;

/**
 * A repeatable group of data elements which together specify a unit price.
 */
public class Price extends OnixElement
{
    private OnixCodeListValueElement<PriceTypeCode> priceTypeCode = new OnixCodeListValueElement<>("PriceTypeCode", "j148", PriceTypeCode::byCode);
    private OnixCodeListValueElement<PriceTypeQualifierCode> priceQualifier = new OnixCodeListValueElement<>("PriceQualifier", "j261", PriceTypeQualifierCode::byCode);
    private OnixElement<String> priceTypeDescription = new OnixElement<>("PriceTypeDescription", "j262");


    public Price()
    {
        super("Price", "price");
        addContent(priceTypeCode);
        addContent(priceQualifier);
        addContent(priceTypeDescription);
    }

    /**
     * An ONIX code indicating the type of the price in the &lt;PriceAmount&gt; field within the &lt;Price&gt; composite. Optional,
     * provided that a &lt;DefaultPriceTypeCode&gt; has been specified in the message header, and non-repeating.
     *
     * @param priceTypeCode PriceTypeCode object
     */
    public void setPriceTypeCode(PriceTypeCode priceTypeCode)
    {
        this.priceTypeCode.setOnixValue(priceTypeCode);
    }

    /**
     * An ONIX code which further specifies the type of price, eg member price, reduced price when purchased as part of
     * a set. Optional and non-repeating.
     *
     * @param priceQualifier PriceTypeQualifierCode object
     */
    public void setPriceQualifier(PriceTypeQualifierCode priceQualifier)
    {
        this.priceQualifier.setOnixValue(priceQualifier);
    }

    /**
     * Free text which further describes the price type. Optional and non-repeating. In the Netherlands, when the
     * &lt;PriceQualifier&gt; code identifies a “voucher price”, the &lt;PriceTypeDescription&gt; should give the “EAN action number”
     * that identifies the offer.
     *
     * @param priceTypeDescription Text, suggested maximum length 200 characters
     */
    public void setPriceTypeDescription(String priceTypeDescription)
    {
        this.priceTypeDescription.setText(priceTypeDescription);
    }
}
