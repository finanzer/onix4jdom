package de.machmireinebook.onix2.element;

import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.IllegalOnixCodeListValueException;
import de.machmireinebook.IllegalOnixDataException;
import de.machmireinebook.codelist.BarcodeIndicatorCode;
import de.machmireinebook.codelist.CountryCode;
import de.machmireinebook.codelist.EditionTypeCode;
import de.machmireinebook.codelist.EpublicationFormatCode;
import de.machmireinebook.codelist.NameCodeType;
import de.machmireinebook.codelist.NotificationOrUpdateTypeCode;
import de.machmireinebook.codelist.ProductCompositionCode;
import de.machmireinebook.codelist.ProductContentTypeCode;
import de.machmireinebook.codelist.ProductFormCode;
import de.machmireinebook.codelist.ProductFormDetailCode;
import de.machmireinebook.codelist.ProductPackagingTypeCode;
import de.machmireinebook.codelist.PublishingStatusCode;
import de.machmireinebook.codelist.RecordSourceTypeCode;
import de.machmireinebook.codelist.ThesisTypeCode;
import de.machmireinebook.codelist.TradeCategoryCode;
import de.machmireinebook.stringconverter.IntegerStringConverter;
import de.machmireinebook.stringconverter.LocalDateStringConverter;
import de.machmireinebook.validation.ValidationMessage;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * Not (yet) implemented
 * <ul>
 *   <li>all deprecated elements</li>
 *   <li>Contained item composite</li>
 *   <li>PR.9 Conference</li>
 *   <li>Religious text composite</li>
 *   <li>PR.12.12 Map scale</li>
 *   <li>PR.14 Audience</li>
 *   <li>PR.16 Links to image/audio/video files </li>
 *   <li>PR.17 Prizes </li>
 *   <li>PR.18 ContentItem  </li>
 *   <li>in PR.19: Contact composite, Imprint or brand composite      </li>
 *   </ul>
 */
public class Product extends OnixElement
{
    private OnixElement<String> recordReference = new OnixElement<>("RecordReference", "a001");
    private OnixCodeListValueElement<NotificationOrUpdateTypeCode> notificationType = new OnixCodeListValueElement<>("NotificationType", "a002", NotificationOrUpdateTypeCode::byCode);
    private OnixCodeListValueElement<ProductCompositionCode> deletionCode =
            new OnixCodeListValueElement<>("DeletionCode", "a198", ProductCompositionCode::byCode);
    private OnixElement<String> deletionText = new OnixElement<>("DeletionText", "a199");
    private OnixCodeListValueElement<RecordSourceTypeCode> recordSourceType =
            new OnixCodeListValueElement<>("RecordSourceType", "a194", RecordSourceTypeCode::byCode);
    private OnixCodeListValueElement<NameCodeType> recordSourceIdentifierType =
            new OnixCodeListValueElement<>("RecordSourceIdentifierType", "a195", NameCodeType::byCode);
    private OnixElement<String> recordSourceIdentifier = new OnixElement<>("RecordSourceIdentifier", "a196");
    private OnixElement<String> recordSourceName = new OnixElement<>("RecordSourceName", "a197");
    private List<ProductIdentifier> productIdentifiers = new ArrayList<>();
    private OnixCodeListValueElement<BarcodeIndicatorCode> barcode = new OnixCodeListValueElement<>("Barcode", "b246", BarcodeIndicatorCode::byCode);
    private OnixCodeListValueElement<ProductFormCode> productForm = new OnixCodeListValueElement<>("ProductForm", "b012", ProductFormCode::byCode);
    private OnixCodeListValueElement<ProductFormDetailCode> productFormDetail = new OnixCodeListValueElement<>("ProductFormDetail", "b333", ProductFormDetailCode::byCode);
    private List<ProductFormFeature> formFeatures = new ArrayList<>();
    private OnixCodeListValueElement<ProductPackagingTypeCode> productPackaging = new OnixCodeListValueElement<>("ProductPackaging", "b225", ProductPackagingTypeCode::byCode);
    private OnixElement<String> productFormDescription = new OnixElement<>("ProductFormDescription", "b014");
    private OnixElement<Integer> numberOfPieces = new OnixElement<>("NumberOfPieces", "b210", new IntegerStringConverter());
    private OnixCodeListValueElement<TradeCategoryCode> tradeCategory = new OnixCodeListValueElement<>("TradeCategory", "b384", TradeCategoryCode::byCode);
    private OnixCodeListValueElement<ProductContentTypeCode> productContentType = new OnixCodeListValueElement<>("ProductContentType", "b385", ProductContentTypeCode::byCode);
    private List<ProductClassification> productClassifications = new ArrayList<>();
    private OnixCodeListValueElement<ProductContentTypeCode> epubType = new OnixCodeListValueElement<>("EpubType", "b211", ProductContentTypeCode::byCode);
    private OnixElement<String> epubTypeVersion = new OnixElement<>("EpubTypeVersion", "b212");
    private OnixElement<String> epubTypeDescription  = new OnixElement<>("EpubTypeDescription", "b213");
    private OnixCodeListValueElement<EpublicationFormatCode> epubFormat = new OnixCodeListValueElement<>("EpubFormat", "b214", EpublicationFormatCode::byCode);
    private OnixElement<String> epubFormatVersion = new OnixElement<>("EpubFormatVersion", "b215");
    private OnixElement<String> epubTypeNote  = new OnixElement<>("EpubTypeNote", "b277");
    private List<Series> allSeries = new ArrayList<>();
    private OnixElement noSeries = new OnixElement("NoSeries", "n338", OnixOutputType.SHORT);
    private List<Title> titles = new ArrayList<>();
    private List<WorkIdentifier> workIdentifiers = new ArrayList<>();
    private List<Website> websites = new ArrayList<>();
    private OnixCodeListValueElement<ThesisTypeCode> thesisType = new OnixCodeListValueElement<>("ThesisType", "b368", ThesisTypeCode::byCode);
    private OnixElement thesisPresentedTo = new OnixElement("ThesisPresentedTo", "b369");
    private OnixElement<Year> thesisYear = new OnixElement<>("ThesisYear", "b370");
    private List<Contributor> contributors = new ArrayList<>();
    private OnixElement noContributor = new OnixElement("NoContributor", "nocontributor", OnixOutputType.SHORT);
    private OnixElement contributorStatement = new OnixElement("ContributorStatement", "b049");
    private List<OnixCodeListValueElement<EditionTypeCode>> editionTypeCodes = new ArrayList<>();
    private OnixElement editionNumber = new OnixElement("EditionNumber", "b057");
    private OnixElement editionVersionNumber = new OnixElement("EditionVersionNumber", "b057");
    private List<Language> languages = new ArrayList<>();
    private OnixElement<Integer> numberOfPages = new OnixElement<>("NumberOfPages", "b061", new IntegerStringConverter());
    private OnixElement<String> pagesRoman = new OnixElement<>("PagesRoman", "b254");
    private OnixElement<String> pagesArabic = new OnixElement<>("PagesArabic", "b255");
    private List<Extent> extents = new ArrayList<>();
    private OnixElement<Integer> numberOfIllustrations = new OnixElement<>("NumberOfIllustrations", "b125", new IntegerStringConverter());
    private OnixElement<String> illustrationsNote = new OnixElement<>("IllustrationsNote", "b062");
    private List<Illustrations> allIllustrations = new ArrayList<>();
    private OnixElement<String> basicMainSubject = new OnixElement<>("BASICMainSubject", "b064");
    private OnixElement<String> basicVersion = new OnixElement<>("BASICVersion", "b200");
    private OnixElement<String> bicMainSubject = new OnixElement<>("BICMainSubject", "b065");
    private OnixElement<String> bicVersion = new OnixElement<>("BICVersion", "b066");
    private List<MainSubject> mainSubjects = new ArrayList<>();
    private List<Subject> subjects = new ArrayList<>();
    private List<PersonAsSubject> personsAsSubject = new ArrayList<>();
    private List<OnixElement<String>> corporateBodiesAsSubject = new ArrayList<>();
    private List<OnixElement<String>> placesAsSubject = new ArrayList<>();
    private List<OtherText> otherTexts = new ArrayList<>();
    private List<Publisher> publishers = new ArrayList<>();
    private OnixElement<String> cityOfPublication = new OnixElement<>("CityOfPublication", "b209");
    private OnixCodeListValueElement<CountryCode> countryOfPublication = new OnixCodeListValueElement<>("CountryOfPublication", "b083", CountryCode::byCode);
    private OnixCodeListValueElement<PublishingStatusCode> publishingStatus = new OnixCodeListValueElement<>("PublishingStatus", "b394", PublishingStatusCode::byCode);
    private OnixElement<String> publishingStatusNote = new OnixElement<>("PublishingStatusNote", "b395");
    private OnixElement<LocalDate> announcementDate = new OnixElement<>("AnnouncementDate", "b086", new LocalDateStringConverter("yyyyMMdd"));
    private OnixElement<LocalDate> tradeAnnouncementDate = new OnixElement<>("TradeAnnouncementDate", "b362", new LocalDateStringConverter("yyyyMMdd"));
    private OnixElement<LocalDate> publicationDate = new OnixElement<>("PublicationDate", "b003", new LocalDateStringConverter("yyyyMMdd"));
    private List<CopyrightStatement> copyrightStatements = new ArrayList<>();
    private OnixElement<Year> copyrightYear = new OnixElement<>("CopyrightYear", "b087");
    private OnixElement<Year> yearFirstPublished = new OnixElement<>("YearFirstPublished", "b088");
    private List<SalesRights> allSalesRights = new ArrayList<>();
    private List<NotForSale> allNotForSale = new ArrayList<>();
    private List<SalesRestriction> salesRestrictions = new ArrayList<>();
    private List<Measure> measures = new ArrayList<>();
    private List<RelatedProduct> relatedProducts = new ArrayList<>();
    private OnixElement<LocalDate> outOfPrintDate = new OnixElement<>("OutOfPrintDate", "h134", new LocalDateStringConverter("yyyyMMdd"));

    public Product()
    {
        super("Product", "product");
        addContent(recordReference);
        addContent(notificationType);
        addContent(deletionCode);
        addContent(deletionText);
        addContent(recordSourceType);
        addContent(recordSourceIdentifierType);
        addContent(recordSourceIdentifier);
        addContent(recordSourceName);

        addContent(barcode);
        addContent(productForm);
        addContent(productFormDetail);

        addContent(productPackaging);
        addContent(productFormDescription);
        addContent(numberOfPieces);
        addContent(tradeCategory);
        addContent(productContentType);

        addContent(epubType);
        addContent(epubTypeVersion);
        addContent(epubTypeDescription);
        addContent(epubTypeNote);
        addContent(noSeries);

        addContent(thesisType);
        addContent(thesisPresentedTo);
        addContent(thesisYear);

        addContent(contributorStatement);

        addContent(editionNumber);
        addContent(editionVersionNumber);

        addContent(numberOfPages);
        addContent(pagesRoman);
        addContent(pagesArabic);

        addContent(numberOfIllustrations);

        addContent(basicMainSubject);
        addContent(basicVersion);
        addContent(bicMainSubject);
        addContent(bicVersion);

        addContent(cityOfPublication);
        addContent(countryOfPublication);
        addContent(publishingStatus);
        addContent(publishingStatusNote);
        addContent(announcementDate);
        addContent(tradeAnnouncementDate);
        addContent(publicationDate);

        addContent(outOfPrintDate);
    }

    /**
     * For every product, you must choose a single number which will uniquely identify the Information record which you
     * send out about that product, and which will remain as its permanent identifier every time you send an update.
     * It doesn't matter what number you choose, provided that it is unique and permanent. This number doesn't really
     * identify the product ' even though you may choose to use the ISBN or another product identifier ' it identifies
     * your information record about the product, so that the person to whom you are sending an update can match it
     * with what you have previously sent. A good way of generating numbers which are not part of a recognized product
     * identification scheme but which can be guaranteed to be unique is to preface the number with an Internet domain
     * name which is registered to your organisation.
     *
     * @param recordReference
     */
    public void setRecordReference(String recordReference)
    {
        this.recordReference.setText(recordReference);
    }

    public void setNotificationType(NotificationOrUpdateTypeCode notificationType)
    {
        this.notificationType.setOnixValue(notificationType);
    }

    public void setDeletionCode(ProductCompositionCode deletionCode)
    {
        if (notificationType.getOnixValue().equals(NotificationOrUpdateTypeCode.Delete))
        {
            this.deletionCode.setOnixValue(deletionCode);
        }
        else
        {
            throw new IllegalOnixCodeListValueException("deletion code could only set if NotificationOrUpdateTypeCode equals Delete");
        }
    }

    public void setDeletionText(String deletionText)
    {
        this.deletionText.setText(deletionText);
    }

    public void setRecordSourceType(RecordSourceTypeCode recordSourceTypeCode)
    {
        this.recordSourceType.setOnixValue(recordSourceTypeCode);
    }

    public void setRecordSourceIdentifierType(NameCodeType recordSourceIdentifierType)
    {
        this.recordSourceIdentifierType.setOnixValue(recordSourceIdentifierType);
    }

    public void setRecordSourceIdentifier(String recordSourceIdentifier)
    {
        if (recordSourceIdentifierType.getOnixValue() == null)
        {
            throw new IllegalOnixDataException("RecordSourceIdentifier could only set if RecordSourceIdentifierType is present");
        }
        this.recordSourceIdentifier.setText(recordSourceIdentifier);
    }

    public void setRecordSourceName(String recordSourceName)
    {
        this.recordSourceName.setText(recordSourceName);
    }

    public void addProductIdentifier(ProductIdentifier productIdentifier)
    {
        productIdentifiers.add(productIdentifier);
        int index = indexOf(recordSourceName);
        addContent(index + productIdentifiers.size(), productIdentifier);
    }

    public void setBarcode(BarcodeIndicatorCode barcode)
    {
        this.barcode.setOnixValue(barcode);
    }

    public void setProductForm(ProductFormCode productForm)
    {
        this.productForm.setOnixValue(productForm);
    }

    public void setProductFormDetail(ProductFormDetailCode productFormDetail)
    {
        this.productFormDetail.setOnixValue(productFormDetail);
    }

    public void addProductFormFeature(ProductFormFeature formFeature)
    {
        formFeatures.add(formFeature);
        int index = indexOf(productFormDetail);
        addContent(index + formFeatures.size(), formFeature);
    }

    public void setProductPackaging(ProductPackagingTypeCode productPackaging)
    {
        this.productPackaging.setOnixValue(productPackaging);
    }

    public void setProductFormDescription(String productFormDescription)
    {
        this.productFormDescription.setText(productFormDescription);
    }

    /**
     * If the product is homogeneous (ie all items or pieces which constitute the product have the same form), the number
     * of items or pieces may be included here. If the product consists of a number of items or pieces of different
     * forms (eg books and audio cassettes), the &lt;ContainedItem&gt; composite should be used ' see below. This field is
     * optional and non-repeating.
     *
     * @param numberOfPieces
     */
    public void setNumberOfPieces(Integer numberOfPieces)
    {
        this.numberOfPieces.setOnixValue(numberOfPieces);
    }

    public void setTradeCategory(TradeCategoryCode tradeCategory)
    {
        this.tradeCategory.setOnixValue(tradeCategory);
    }

    public void setProductContentType(ProductContentTypeCode productContentType)
    {
        this.productContentType.setOnixValue(productContentType);
    }

    public void addProductClassification(ProductClassification productClassification)
    {
        productClassifications.add(productClassification);
        int index = indexOf(productContentType);
        addContent(index + productClassifications.size(), productClassification);
    }

    /**
     * An ONIX code identifying the type of an epublication. This element is mandatory if and only if the
     * &lt;ProductForm&gt; code for the product is DG.
     *
     * @param epubType
     */
    public void setEpubType(ProductContentTypeCode epubType)
    {
        if (!productForm.getOnixValue().equals(ProductFormCode.Electronic_book_text))
        {
            throw new IllegalOnixDataException("EpubType could only set if ProductForm is equals DG");
        }
        this.epubType.setOnixValue(epubType);
    }

    /**
     * A version number which applies to a specific epublication type. Optional and non-repeating, and can occur only
     * if the &lt;EpubType&gt; field is present.
     *
     * @param epubTypeVersion
     */
    public void setEpubTypeVersion(String epubTypeVersion)
    {
        if (epubType.getOnixValue() == null)
        {
            throw new IllegalOnixDataException("EpubTypeVersion could only set if EpubType is present");
        }
        this.epubTypeVersion.setText(epubTypeVersion);
    }

    /**
     * A free text description of an epublication type. Optional and non-repeating, and can occur only if the
     * &lt;EpubType&gt; field is present.
     *
     * @param epubTypeDescription
     */
    public void setEpubTypeDescription(String epubTypeDescription)
    {
        if (epubType.getOnixValue() == null)
        {
            throw new IllegalOnixDataException("EpubTypeDescription could only set if EpubType is present");
        }
        this.epubTypeDescription.setText(epubTypeDescription);
    }

    /**
     * An ONIX code identifying the underlying format of an epublication. Optional and non-repeating, and can occur only
     * if the &lt;EpubType&gt; field is present. Note that where the epublication type is wholly defined by the delivery
     * format, this element effectively duplicates the &lt;EpubType&gt; field.
     *
     * @param epubFormat
     */
    public void setEpubFormat(EpublicationFormatCode epubFormat)
    {
        this.epubFormat.setOnixValue(epubFormat);
    }

    /**
     * A version number which applies to an epublication format. Optional and non-repeating, and can occur only if the
     * &lt;EpubFormat&gt; field is present.
     *
     * @param epubFormatVersion
     */
    public void setEpubFormatVersion(String epubFormatVersion)
    {
        this.epubFormatVersion.setText(epubFormatVersion);
    }

    /**
     * A free text description of features of a product which are specific to its appearance as a particular epublication
     * type. Optional and non-repeatable, and can occur only if the &lt;EpubType&gt; field is present.
     *
     * @param epubTypeNote
     */
    public void setEpubTypeNote(String epubTypeNote)
    {
        if (epubType.getOnixValue() == null)
        {
            throw new IllegalOnixDataException("EpubTypeNote could only set if EpubType is present");
        }
        this.epubTypeNote.setText(epubTypeNote);
    }

    /**
     * A 'series' means an indefinite number of products, published over an indefinite time period, and grouped together
     * under a series title, primarily for marketing purposes. A series does not have an EAN-13 number, ISBN or UPC,
     * and it is not traded as a single item, although it may be possible to place a standing order for successive items
     * in the series to be supplied automatically.
     *
     * A product may occasionally belong to two or more series. Consequently the series elements constitute a repeatable composite.
     *
     * Series elements include a series code if any, the series title, and any enumeration of the product within the series.
     *
     * A repeatable group of data elements which together describe a series of which the product is part.
     *
     * @param singleSeries
     */
    public void addSeries(Series singleSeries)
    {
        if (allSeries.isEmpty())
        {
            removeContent(noSeries);
        }
        allSeries.add(singleSeries);
        int index = indexOf(epubTypeNote);
        addContent(index + allSeries.size(), singleSeries);
    }

    public void addTitle(Title title)
    {
        titles.add(title);
        int index = indexOf(epubTypeNote);
        addContent(index + (allSeries.size() == 0 ? 1 : allSeries.size()) + titles.size(), title);
    }

    public void addWorkIdentifier(WorkIdentifier workIdentifier)
    {
        workIdentifiers.add(workIdentifier);
        int index = indexOf(epubTypeNote);
        addContent(index + (allSeries.size() == 0 ? 1 : allSeries.size()) + titles.size() + workIdentifiers.size(), workIdentifier);
    }

    public void addWebsite(Website website)
    {
        websites.add(website);
        int index = indexOf(epubTypeNote);
        addContent(index + (allSeries.size() == 0 ? 1 : allSeries.size()) + titles.size() + workIdentifiers.size()
                + websites.size(), website);
    }

    public void setThesisType(ThesisTypeCode thesisType)
    {
        this.thesisType.setOnixValue(thesisType);
    }

    public void setThesisPresentedTo(String thesisPresentedTo)
    {
        this.thesisPresentedTo.setText(thesisPresentedTo);
    }

    public void setThesisYear(String thesisYear)
    {
        this.thesisYear.setText(thesisYear);
    }

    public void addContributor(Contributor contributor)
    {
        if (contributors.isEmpty())
        {
            removeContent(noContributor);
        }
        contributors.add(contributor);
        int index = indexOf(thesisYear);
        addContent(index + contributors.size(), contributor);
    }

    /**
     * Free text showing how the authorship should be described in an online display, when a standard concatenation
     * of individual contributor elements would not give a satisfactory presentation. When this field is sent,
     * the receiver should use it to replace all name detail sent in the &lt;Contributor&gt; composite for display purposes
     * only. It does not replace the &lt;BiographicalNote&gt; element. The individual name detail must also be sent in
     * the &lt;Contributor&gt; composite for indexing and retrieval.
     *
     * @param contributorStatement
     */
    public void setContributorStatement(String contributorStatement)
    {
        this.contributorStatement.setText(contributorStatement);
    }

    /**
     * An ONIX code, indicating the type of a version or edition. Optional, and repeatable if the product has
     * characteristics of two or more types (eg revised and annotated).
     *
     * @param editionTypeCode
     */
    public void setEditionTypeCodes(EditionTypeCode editionTypeCode)
    {
        OnixCodeListValueElement<EditionTypeCode> editionTypeCodeElement = new OnixCodeListValueElement<>("EditionTypeCode", "b056", EditionTypeCode::byCode);
        editionTypeCodeElement.setOnixValue(editionTypeCode);
        this.editionTypeCodes.add(editionTypeCodeElement);
        int index = indexOf(contributorStatement);
        addContent(index + editionTypeCodes.size(), editionTypeCodeElement);
    }

    /**
     * The number of a numbered edition. Optional and non-repeating. Normally sent only for the second and subsequent
     * editions of a work, but by agreement between parties to an ONIX exchange a first edition may be explicitly numbered.
     *
     * @param editionNumber
     */
    public void setEditionNumber(String editionNumber)
    {
        this.editionNumber.setText(editionNumber);
    }

    /**
     * The number of a numbered revision within an edition number. To be used only where a publisher uses such two-level
     * numbering to indicate revisions which do not constitute a new edition under a new ISBN or other distinctive
     * product identifier. Optional and non-repeating. If this field is used, an &lt;EditionNumber&gt; must also be present.
     *
     * @param editionVersionNumber
     */
    public void setEditionVersionNumber(String editionVersionNumber)
    {
        this.editionVersionNumber.setText(editionVersionNumber);
    }

    /**
     * These elements specify the language(s) of the text of a product and/or of the original work of which it is a
     * translation. Group PR.11 is not mandatory. A default language of text can be declared in an ONIX message header '
     * see field MH.20 in ONIX for Books ' Product Information Message ' XML Message Specification.
     *
     * @param language
     */
    public void addLanguage(Language language)
    {
        languages.add(language);
        int index = indexOf(editionVersionNumber);
        addContent(index + languages.size(), language);
    }

    /**
     * An indication of the total number of pages in a book or other printed product. This is not intended to
     * represent a precise count of numbered and unnumbered pages. It is usually sufficient to take the number
     * from the last numbered page. If there are two or more separate numbering sequences (eg xviii + 344),
     * the numbers in each sequence may be added together to make an overall total (in this case 362), but do not
     * count unnumbered pages except if the book does not have numbered pages at all.
     *
     * For multi-volume books, enter the total for all the volumes combined.
     *
     * This field is optional, but it is normally required for a printed book unless the &lt;PagesRoman&gt; and
     * &lt;PagesArabic&gt; elements are used, and is non-repeating.
     *
     * @param numberOfPages
     */
    public void setNumberOfPages(Integer numberOfPages)
    {
        this.numberOfPages.setOnixValue(numberOfPages);
    }

    /**
     * The number of pages numbered in roman numerals. The &lt;PagesRoman&gt; and &lt;PagesArabic&gt; elements together represent an
     * alternative to &lt;NumberOfPages&gt; where there is a requirement to specify these numbering sequences separately.
     * For most ONIX applications, however, &lt;NumberOfPages&gt; will be preferred. Optional and non-repeating.
     *
     * @param pagesRoman
     */
    public void setPagesRoman(String pagesRoman)
    {
        this.pagesRoman.setText(pagesRoman);
    }

    /**
     * The number of pages numbered in Arabic numerals. Optional and non-repeating.
     *
     * @param pagesArabic
     */
    public void setPagesArabic(String pagesArabic)
    {
        this.pagesArabic.setText(pagesArabic);
    }

    /**
     * A repeatable group of data elements which together describe an extent pertaining to the product.
     *
     * @param extent
     */
    public void addExtent(Extent extent)
    {
        extents.add(extent);
        int index = indexOf(pagesArabic);
        addContent(index + extents.size(), extent);
    }

    /**
     * The total number of illustrations in a book or other printed product. The more informative free text field
     * &lt;IllustrationsNote&gt; or the &lt;Illustrations&gt; composite are preferred, but where the sender of the product
     * information maintains only a simple numeric field, the &lt;NumberOfIllustrations&gt; element may be used.
     * Optional and non-repeating.
     *
     * @param numberOfIllustrations
     */
    public void setNumberOfIllustrations(Integer numberOfIllustrations)
    {
        this.numberOfIllustrations.setOnixValue(numberOfIllustrations);
    }

    /**
     * For books or other text media only, this data element carries text stating the number and type of illustrations.
     * The text may also include other content items, eg maps, bibliography, tables, index etc. Optional and non-repeating.
     *
     * @param illustrationsNote
     */
    public void setIllustrationsNote(String illustrationsNote)
    {
        this.illustrationsNote.setText(illustrationsNote);
    }

    /**
     * A repeatable group of data elements which together specify the number of illustrations or other content items of
     * a stated type which the product carries. Use of the &lt;Illustrations&gt; composite is optional.
     *
     * @param illustrations
     */
    public void addIllustrations(Illustrations illustrations)
    {
        allIllustrations.add(illustrations);
        int index = indexOf(illustrationsNote);
        addContent(index + allIllustrations.size(), illustrations);
    }

    /**
     * A BISAC subject category code which identifies the main subject of the product. Optional and non-repeating.
     * Additional BISAC subject category codes may be sent using the &lt;Subject&gt; composite. Note that the data element
     * reference name was assigned during a period when the BISAC name had been changed to BASIC. The name has
     * now officially reverted to 'BISAC', but the ONIX data element name cannot be changed for reasons
     * of upwards compatibility.
     *
     * @param basicMainSubject
     */
    public void setBasicMainSubject(String basicMainSubject)
    {
        this.basicMainSubject.setText(basicMainSubject);
    }

    /**
     * A number identifying the version of the BISAC subject categories used in &lt;BASICMainSubject&gt;.
     * Optional and non-repeating, and may only occur when &lt;BASICMainSubject&gt; is also present.
     *
     * @param basicVersion
     */
    public void setBasicVersion(String basicVersion)
    {
        if (StringUtils.isEmpty(basicMainSubject.getText()))
        {
            throw new IllegalOnixDataException("BasicVersion could only set if BASICMainSubject is present");
        }
        this.basicVersion.setText(basicVersion);
    }

    /**
     * A BIC subject category code which identifies the main subject of the product. Optional and non-repeating.
     * Additional BIC subject category codes may be sent using the &lt;Subject&gt; composite.
     *
     * @param bicMainSubject
     */
    public void setBicMainSubject(String bicMainSubject)
    {
        this.bicMainSubject.setText(bicMainSubject);
    }

    /**
     * A number identifying the version of the BIC subject category scheme used in &lt;BICMainSubject&gt;. Optional
     * and non-repeating, and may only occur when &lt;BICMainSubject&gt; is also present.
     *
     * @param bicVersion
     */
    public void setBicVersion(String bicVersion)
    {
        if (StringUtils.isEmpty(bicMainSubject.getText()))
        {
            throw new IllegalOnixDataException("BicVersion could only set if BICMainSubject is present");
        }
        this.bicVersion.setText(bicVersion);
    }

    /**
     * An optional and repeatable group of data elements which together describe a main subject classification or
     * subject heading which is taken from a recognized scheme other than BISAC or BIC.
     *
     * @param mainSubject
     */
    public void addMainSubject(MainSubject mainSubject)
    {
        mainSubjects.add(mainSubject);
        int index = indexOf(bicVersion);
        addContent(index + mainSubjects.size(), mainSubject);
    }

    /**
     * An optional and repeatable group of data elements which together describe a subject classification or subject
     * heading which is additional to the BISAC, BIC or other main subject category.
     *
     * @param subject
     */
    public void addSubject(Subject subject)
    {
        subjects.add(subject);
        int index = indexOf(bicVersion);
        addContent(index + mainSubjects.size()+ subjects.size(), subject);
    }

    /**
     * An optional and repeatable group of data elements which together represent the name of a person who is part of the
     * subject of a product.
     *
     * @param personAsSubject
     */
    public void addPersonAsSubject(PersonAsSubject personAsSubject)
    {
        personsAsSubject.add(personAsSubject);
        int index = indexOf(bicVersion);
        addContent(index + mainSubjects.size()+ subjects.size() + personsAsSubject.size(), personAsSubject);
    }

    /**
     * The name of a corporate body which is part of the subject of the product. Optional, and repeatable if more than
     * one corporate body is involved.
     *
     * @param corporateBodyAsSubject
     */
    public void addCorporateBodyAsSubject(String corporateBodyAsSubject)
    {
        OnixElement<String> corporateBodyAsSubjectElement = new OnixElement<>("CorporateBodyAsSubjectElement", "b071");
        corporateBodyAsSubjectElement.setOnixValue(corporateBodyAsSubject);
        corporateBodiesAsSubject.add(corporateBodyAsSubjectElement);
        int index = indexOf(bicVersion);
        addContent(index + mainSubjects.size()+ subjects.size() + personsAsSubject.size() + corporateBodiesAsSubject.size(),
                corporateBodyAsSubjectElement);
    }

    /**
     * The name of a place or region or geographical entity which is part of the subject of the product. Optional,
     * and repeatable if the subject of the product includes more than one place.
     *
     * @param placeAsSubject
     */
    public void addPlaceAsSubject(String placeAsSubject)
    {
        OnixElement<String> placeAsSubjectElement = new OnixElement<>("PlaceAsSubject", "b072");
        placeAsSubjectElement.setOnixValue(placeAsSubject);
        placesAsSubject.add(placeAsSubjectElement);
        int index = indexOf(bicVersion);
        addContent(index + mainSubjects.size()+ subjects.size() + personsAsSubject.size() + corporateBodiesAsSubject.size()
                        + placesAsSubject.size(),
                placeAsSubjectElement);
    }

    /**
     * An optional and repeatable group of data elements which together identify and either include, or provide
     * pointers to, text related to the product.
     *
     * @param otherText the other text to add to onix message
     */
    public void addOtherText(OtherText otherText)
    {
        otherTexts.add(otherText);
        int index = indexOf(bicVersion);
        addContent(index + mainSubjects.size()+ subjects.size() + personsAsSubject.size() + corporateBodiesAsSubject.size()
                + placesAsSubject.size() + otherTexts.size(),  otherText);
    }

    /**
     * A repeatable group of data elements which together identify an entity which is associated with the publishing of a
     * product. The composite will allow additional publishing roles to be introduced without adding new fields.
     * Each occurrence of the composite must carry either a name code or a name or both.
     *
     * @param publisher the Publisher to add to onix message
     */
    public void addPublisher(Publisher publisher)
    {
        publishers.add(publisher);
        int index = indexOf(bicVersion);
        addContent(index + mainSubjects.size()+ subjects.size() + personsAsSubject.size() + corporateBodiesAsSubject.size()
                + placesAsSubject.size() + otherTexts.size() + publishers.size(),  publisher);
    }

    /**
     * The name of a city or town associated with the imprint or publisher. Optional, and repeatable if the imprint
     * carries two or more cities of publication.
     *
     * A place of publication is normally given in the form in which it appears on the title page. If the place name
     * appears in more than one language, use the language of the title carried in the ONIX record. If this criterion
     * does not apply, use the form that appears first. Alternatively, some ONIX applications may follow their
     * own 'house style'.
     *
     * @param cityOfPublication
     */
    public void setCityOfPublication(String cityOfPublication)
    {
        this.cityOfPublication.setOnixValue(cityOfPublication);
    }

    /**
     * A code identifying the country where the product is issued. Optional and non-repeating.
     *
     * @param countryOfPublication
     */
    public void setCountryOfPublication(CountryCode countryOfPublication)
    {
        this.countryOfPublication.setOnixValue(countryOfPublication);
    }

    /**
     * An ONIX code which identifies the status of a published product. Optional and non-repeating,
     * <b>but it is very strongly recommended that this element should be included in all ONIX Books Product
     * records, and it is possible that it may be made mandatory in a future release, or that it will be treated as
     * mandatory in national ONIX accreditation schemes.</b>
     *
     * Where the element is sent by a sender who is not the publisher, based on information that has been previously
     * supplied by the publisher, it is strongly recommended that the element should carry a datestamp attribute
     * to indicate its likely reliability. See ONIX for Books ' Product Information Message ' XML Message Specification,
     * Section 4, for details of the datestamp attribute.
     *
     * @param publishingStatus
     */
    public void setPublishingStatus(PublishingStatusCode publishingStatus)
    {
        this.publishingStatus.setOnixValue(publishingStatus);
    }

    /**
     * Free text that describes the status of a published product, when the code in &lt;PublishingStatus&gt; is insufficient.
     * Optional and non-repeating, but must be accompanied by the &lt;PublishingStatus&gt; element.
     * 
     * @param publishingStatusNote
     */
    public void setPublishingStatusNote(String publishingStatusNote)
    {
        this.publishingStatusNote.setOnixValue(publishingStatusNote);
    }

    /**
     *  Date when information about the product can be issued to the general public. (Some publishers issue advance
     *  information under embargo.) Optional and non-repeating.
     *
     * @param announcementDate
     */
    public void setAnnouncementDate(LocalDate announcementDate)
    {
        this.announcementDate.setOnixValue(announcementDate);
    }

    /**
     *  Date when information about the product can be issued to the book trade, while remaining embargoed for the
     *  general public. (Some publishers issue advance information under embargo.) Optional and non-repeating.
     *
     * @param tradeAnnouncementDate
     */
    public void setTradeAnnouncementDate(LocalDate tradeAnnouncementDate)
    {
        this.tradeAnnouncementDate.setOnixValue(tradeAnnouncementDate);
    }

    /**
     *  The date of first publication of this product in the home market of the publisher named in PR.19 (that is,
     *  under the current ISBN or other identifier, as distinct from the date of first publication of the work,
     *  which may be given in &lt;YearFirstPublished&gt; on the next page). In advance information, this will be an
     *  expected date, which should be replaced by the actual date of publication when known. The date should be
     *  given as precisely as possible, but in early notifications a month and year are sufficient (for this case use
     *  setPublicationMonthYear()); and for backlist
     *  titles the year of publication is sufficient (for this case use setPublicationYear()).
     *
     *  Note that in advance information this date must not be interpreted as the date when the product will
     *  first be available in a territory other than the publisher's home market. See the &lt;SupplyDetail&gt; and
     *  &lt;MarketRepresentation&gt; composites, Groups PR.24 and PR.25, for other market-specific detail.
     *
     *  Optional and non-repeating.
     *
     * @param publicationDate he date of first publication as LocalDate
     */
    public void setPublicationDate(LocalDate publicationDate)
    {
        this.publicationDate.setOnixValue(publicationDate);
    }

    /**
     *  The date of first publication of this product in the home market of the publisher named in PR.19 (that is,
     *  under the current ISBN or other identifier, as distinct from the date of first publication of the work,
     *  which may be given in &lt;YearFirstPublished&gt; on the next page). In advance information, this will be an
     *  expected date, which should be replaced by the actual date of publication when known. The date should be
     *  given as precisely as possible, but in early notifications a month and year are sufficient (use this method); and for backlist
     *  titles the year of publication is sufficient (use setPublicationYear()).
     *
     *  Note that in advance information this date must not be interpreted as the date when the product will
     *  first be available in a territory other than the publisher's home market. See the &lt;SupplyDetail&gt; and
     *  &lt;MarketRepresentation&gt; composites, Groups PR.24 and PR.25, for other market-specific detail.
     *
     *  Optional and non-repeating.
     *
     * @param publicationDate The mont and year of first publication as YearMonth object
     */
    public void setPublicationMonthYear(YearMonth publicationDate)
    {
        this.publicationDate.setStringConverter(new LocalDateStringConverter("yyyyMM"));
        //the date value is not outputted, so it's not important which value it has
        this.publicationDate.setOnixValue(publicationDate.atDay(1));
    }

    /**
     *  The date of first publication of this product in the home market of the publisher named in PR.19 (that is,
     *  under the current ISBN or other identifier, as distinct from the date of first publication of the work,
     *  which may be given in &lt;YearFirstPublished&gt; on the next page). In advance information, this will be an
     *  expected date, which should be replaced by the actual date of publication when known. The date should be
     *  given as precisely as possible, but in early notifications a month and year are sufficient (use
     *  setPublicationMonthYear()); and for backlist titles the year of publication is sufficient (use this method).
     *
     *  Note that in advance information this date must not be interpreted as the date when the product will
     *  first be available in a territory other than the publisher's home market. See the &lt;SupplyDetail&gt; and
     *  &lt;MarketRepresentation&gt; composites, Groups PR.24 and PR.25, for other market-specific detail.
     *
     *  Optional and non-repeating.
     *
     * @param year The year of first publication as Year object
     */
    public void setPublicationYear(Year year)
    {
        this.publicationDate.setText(year.toString());
    }

    /**
     * An optional and repeatable group of data elements which together represent a structured copyright statement for
     * the product. Either a structured copyright statement or statements, or a copyright year in the separate
     * &lt;CopyrightYear&gt; element which follows the composite, but not both, may be sent.
     *
     * @param copyrightStatement
     */
    public void addCopyrightStatement(CopyrightStatement copyrightStatement)
    {
        copyrightStatements.add(copyrightStatement);
        int index = indexOf(publicationDate);
        addContent(index + copyrightStatements.size(),  copyrightStatement);
    }
    
    /**
     * The copyright year as it appears in a copyright statement on the product. Mandatory in each occurrence of
     * the &lt;CopyrightStatement&gt; composite, and repeatable if several years are listed.
     * 
     * @param copyrightYear
     */
    public void setCopyrightYear(Year copyrightYear)
    {
        this.copyrightYear.setOnixValue(copyrightYear);
    }

    /**
     * The year when the work first appeared in any language or edition, if different from the copyright year.
     * Optional and non-repeating.
     *
     * @param yearFirstPublished
     */
    public void setYearFirstPublished(Year yearFirstPublished)
    {
        this.yearFirstPublished.setOnixValue(yearFirstPublished);
    }

    /**
     * A repeatable group of data elements which together identify territorial sales rights which a publisher chooses
     * to exercise in a product. The &lt;SalesRights&gt; composite may occur once for each value of &lt;b089&gt;. See examples
     * at the end of Group PR.21.
     *
     * @param salesRights
     */
    public void addSalesRights(SalesRights salesRights)
    {
        allSalesRights.add(salesRights);
        int index = indexOf(yearFirstPublished);
        addContent(index + allSalesRights.size(),  salesRights);
    }

    /**
     * A repeatable group of data elements which together identify a country or countries in which the product is not
     * for sale, together with the ISBN and/or other product identifier and/or the name of the publisher of the same
     * work in the specified country/ies.
     *
     * @param notForSale
     */
    public void addNotForSale(NotForSale notForSale)
    {
        allNotForSale.add(notForSale);
        int index = indexOf(yearFirstPublished);
        addContent(index + allSalesRights.size() + allNotForSale.size(),  notForSale);
    }

    /**
     * A group of data elements which together identify a non-territorial sales restriction which a publisher applies to
     * a product. Optional and repeatable.
     *
     * @param salesRestriction
     */
    public void addSalesRestriction(SalesRestriction salesRestriction)
    {
        salesRestrictions.add(salesRestriction);
        int index = indexOf(yearFirstPublished);
        addContent(index + allSalesRights.size() + allNotForSale.size() + salesRestrictions.size(),  salesRestriction);
    }

    /**
     * An optional and repeatable group of data elements which together identify a measurement and the units in which
     * it is expressed.
     *
     * Product dimensions are preferably expressed as repeated occurrences of a &lt;Measure&gt; composite, which identifies
     * the dimension which is to be specified, the measurement quantity, and the measure unit which is used.
     *
     * Group PR.22 is not mandatory, but if it is used, any of the following content is valid:
     *
     * Repeats of the &lt;Measure&gt; composite only
     * Some or all of the &lt;Height&gt;, &lt;Width&gt;, &lt;Thickness&gt;, &lt;Weight&gt; fields only (not implemented)
     * &lt;Dimensions&gt; element only, or with (a) or (b). (not implemented)
     *
     * In advance information, overall book sizes may be given as estimates based on the page trim size, but they should
     * if possible be replaced by accurate details when they are known, or on publication.
     *
     * @param measure
     */
    public void addMeasure(Measure measure)
    {
        measures.add(measure);
        int index = indexOf(yearFirstPublished);
        addContent(index + allSalesRights.size() + allNotForSale.size() + salesRestrictions.size() + measures.size(), measure);
    }

    /**
     * A repeatable group of data elements which together describe a product which has a specified relationship to the
     * product which is described in the ONIX record. Although for reasons of upwards compatibility the composite includes
     * individual fields for ISBN and EAN-13 number, use of the nested &lt;ProductIdentifier&gt; composite is to be preferred,
     * since it allows any recognized identifier scheme (eg DOI) to be used.
     *
     * The minimum required content of an occurrence of the &lt;RelatedProduct&gt; composite is a &lt;RelationCode&gt; and either a
     * product identifier or a &lt;ProductForm&gt; value. In other words, it is valid to list related products by relationship
     * and identifier only, or by relationship and form only.
     */
    public void addRelatedProduct(RelatedProduct relatedProduct)
    {
        relatedProducts.add(relatedProduct);
        int index = indexOf(yearFirstPublished);
        addContent(index + allSalesRights.size() + allNotForSale.size() + salesRestrictions.size() + measures.size() + relatedProducts.size(), relatedProduct);
    }

    public void setOutOfPrintDate(LocalDate outOfPrintDate)
    {
        this.outOfPrintDate.setOnixValue(outOfPrintDate);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        if ((StringUtils.isNotEmpty(recordSourceIdentifier.getText()) && StringUtils.isEmpty(recordSourceIdentifierType.getText()))
                || (StringUtils.isEmpty(recordSourceIdentifier.getText()) && StringUtils.isNotEmpty(recordSourceIdentifierType.getText())))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("Product", "<RecordSourceIdentifierType> must also be present if <RecordSourceIdentifier> field is present and vice versa", "RecordSourceIdentifierType", "RecordSourceIdentifier"));
        }
        if (StringUtils.isNotEmpty(copyrightYear.getText()) && !copyrightStatements.isEmpty())
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("Product", "Either a structured copyright statement or " +
                    " statements, or a copyright year in the separate " +
                    " <CopyrightYear> element which follows the composite, but not both, are valid.", "CopyrightStatement", "CopyrightYear"));
        }
        return result;
    }


}
