package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.ProductClassificationTypeCode;

/**
 *
 */
public class ProductClassification extends OnixElement
{
    private OnixCodeListValueElement<ProductClassificationTypeCode> productClassificationType = new OnixCodeListValueElement<>("ProductClassificationType", "b274", OnixOccurenceType.MANDATORY, ProductClassificationTypeCode::byCode);
    private OnixElement productClassificationCode = new OnixElement("ProductClassificationCode", "b275", OnixOccurenceType.MANDATORY);
    private OnixElement percent = new OnixElement("Percent", "b337");

    public ProductClassification()
    {
        super("ProductClassification", "productclassification");
        addContent(productClassificationType);
        addContent(productClassificationCode);
    }

    public void setProductClassificationType(ProductClassificationTypeCode productClassificationType)
    {
        this.productClassificationType.setOnixValue(productClassificationType);
    }

    public void setProductClassificationCode(String productClassificationCode)
    {
        this.productClassificationCode.setText(productClassificationCode);
    }

    public void setPercent(String percent)
    {
        this.percent.setText(percent);
    }
}
