package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.OnixCodeListFactory;
import de.machmireinebook.codelist.ProductFormDetailCode;
import de.machmireinebook.codelist.ProductFormFeatureTypeCodeList;
import de.machmireinebook.stringconverter.OnixCodeListStringConverter;

/**
 * User: mjungierek
 * Date: 30.07.2016
 * Time: 16:59
 */
public class ProductFormFeature extends OnixElement
{
    private OnixCodeListValueElement<ProductFormDetailCode> productFormFeatureType = new OnixCodeListValueElement<>("ProductFormFeatureType", "b334", ProductFormDetailCode::byCode);
    private OnixElement<ProductFormFeatureTypeCodeList> productFormFeatureValue = new OnixElement<>("ProductFormFeatureValue", "b335");
    private OnixElement productFormFeatureDescription = new OnixElement("ProductFormFeatureDescription", "b336");


    public ProductFormFeature()
    {
        super("ProductFormFeature", "productformfeature");
        addContent(productFormFeatureType);
        addContent(productFormFeatureValue);
        addContent(productFormFeatureDescription);
    }

    public void setProductFormFeatureType(ProductFormDetailCode productFormFeatureType)
    {
        this.productFormFeatureType.setOnixValue(productFormFeatureType);
    }

    /**
     *
     * @param productFormFeatureValue
     * @param factory the factory of the concrete implementation of ProductFormFeatureTypeCodeList to convert string to CodeListValue,
     *                because the type of implementation is lost after setting the onix value
     */
    public void setProductFormFeatureValue(ProductFormFeatureTypeCodeList productFormFeatureValue, OnixCodeListFactory<ProductFormFeatureTypeCodeList> factory)
    {
        this.productFormFeatureValue.setOnixValue(productFormFeatureValue);
        this.productFormFeatureValue.setStringConverter(new OnixCodeListStringConverter<>(factory));
    }

    public void setProductFormFeatureDescription(String productFormFeatureDescription)
    {
        this.productFormFeatureDescription.setText(productFormFeatureDescription);
    }
}
