package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.ProductIdentifierTypeCode;

/**
 * User: mjungierek
 * Date: 30.07.2016
 * Time: 11:53
 */
public class ProductIdentifier extends OnixElement
{
    private OnixCodeListValueElement<ProductIdentifierTypeCode> productIDType = new OnixCodeListValueElement<>("ProductIDType", "b221", OnixOccurenceType.MANDATORY, ProductIdentifierTypeCode::byCode);
    private OnixElement idTypeName = new OnixElement("IDTypeName", "b233");
    private OnixElement idValue = new OnixElement("IDValue", "b244", OnixOccurenceType.MANDATORY);

    public ProductIdentifier()
    {
        super("ProductIdentifier", "productidentifier");
        addContent(productIDType);
        addContent(idTypeName);
        addContent(idValue);
    }

    public void setProductIDType(ProductIdentifierTypeCode productIDType)
    {
        this.productIDType.setOnixValue(productIDType);
    }

    public void setIdTypeName(String idTypeName)
    {
        this.idTypeName.setText(idTypeName);
    }

    public void setIdValue(String idValue)
    {
        this.idValue.setText(idValue);
    }
}
