package de.machmireinebook.onix2.element;

/**
 * User: mjungierek
 * Date: 31.07.2016
 * Time: 22:12
 */
public class ProfessionalAffiliation extends OnixElement
{
    private OnixElement professionalPosition = new OnixElement("ProfessionalPosition", "b045");
    private OnixElement affiliation = new OnixElement("Affiliation", "b046");

    public ProfessionalAffiliation()
    {
        super("ProfessionalAffiliation", "professionalaffiliation");
        addContent(professionalPosition);
        addContent(affiliation);
    }

    public void setProfessionalPosition(String professionalPosition)
    {
        this.professionalPosition.setText(professionalPosition);
    }

    public void setAffiliation(String affiliation)
    {
        this.affiliation.setText(affiliation);
    }
}
