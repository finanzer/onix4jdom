package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.codelist.NameCodeType;
import de.machmireinebook.codelist.PublishingRoleCode;
import de.machmireinebook.validation.ValidationMessage;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 * A repeatable group of data elements which together identify an entity which is associated with the publishing of a
 * product. The composite will allow additional publishing roles to be introduced without adding new fields.
 * Each occurrence of the composite must carry either a name code or a name or both.
 */
public class Publisher extends OnixElement
{
    private OnixCodeListValueElement<PublishingRoleCode> publishingRole = new OnixCodeListValueElement<>("PublishingRole", "b291", PublishingRoleCode::byCode);
    private OnixCodeListValueElement<NameCodeType> nameCodeType = new OnixCodeListValueElement<>("NameCodeType", "b241", NameCodeType::byCode);
    private OnixElement<String> nameCodeTypeName = new OnixElement<>("NameCodeTypeName", "b242");
    private OnixElement<String> nameCodeValue = new OnixElement<>("NameCodeValue", "b243");
    private OnixElement<String> publisherName = new OnixElement<>("PublisherName", "b081");
    private List<Website> websites = new ArrayList<>();

    public Publisher()
    {
        super("Publisher", "publisher");
        addContent(publishingRole);
        addContent(nameCodeType);
        addContent(nameCodeTypeName);
        addContent(nameCodeValue);
        addContent(publisherName);
    }

    /**
     * An ONIX code which identifies a role played by an entity in the publishing of a product. Optional and
     * non-repeating. The default if the element is omitted is 'publisher'.
     *
     * @param publishingRole
     */
    public void setPublishingRole(PublishingRoleCode publishingRole)
    {
        this.publishingRole.setOnixValue(publishingRole);
    }

    /**
     * An ONIX code which identifies the scheme from which the value in the &lt;NameCodeValue&gt; element is taken. Optional
     * and non-repeating, but mandatory if the &lt;Publisher&gt; composite does not carry a &lt;PublisherName&gt;.
     *
     * @param nameCodeType
     */
    public void setNameCodeType(NameCodeType nameCodeType)
    {
        this.nameCodeType.setOnixValue(nameCodeType);
    }

    /**
     * A name which identifies a proprietary name code when the code in &lt;NameCodeType&gt; indicates a proprietary scheme,
     * eg a bibliographic agency's own code. Optional and non-repeating.
     *
     * @param nameCodeTypeName
     */
    public void setNameCodeTypeName(String nameCodeTypeName)
    {
        this.nameCodeTypeName.setText(nameCodeTypeName);
    }

    /**
     * A code value taken from the scheme specified in &lt;NameCodeType&gt;. Mandatory if and only if &lt;NameCodeType&gt;
     * is present, and non-repeating.
     *
     * @param nameCodeValue
     */
    public void setNameCodeValue(String nameCodeValue)
    {
        this.nameCodeValue.setText(nameCodeValue);
    }

    /**
     * The name of an entity associated with the publishing of a product. Mandatory if there is no name code in an
     * occurrence of the &lt;Publisher&gt; composite, and optional if a name code is included. Non-repeating.
     *
     * @param publisherName
     */
    public void setPublisherName(String publisherName)
    {
        this.publisherName.setText(publisherName);
    }

    /**
     * A repeatable group of data elements which together identify and provide pointers to a website which is related
     * to the publisher identified in an occurrence of the &lt;Publisher&gt; composite.
     *
     * @param website
     */
    public void addWebsite(Website website)
    {
        websites.add(website);
        int index = indexOf(publisherName);
        addContent(index + websites.size(), website);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        if (StringUtils.isEmpty(publisherName.getText()) && StringUtils.isEmpty(nameCodeType.getText()))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("Publisher", "PublisherName is if there is no name code in an" +
                    " occurrence of <Publisher>"));
        }
        return result;
    }
}
