package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.IllegalOnixDataException;
import de.machmireinebook.codelist.EpublicationFormatCode;
import de.machmireinebook.codelist.ProductContentTypeCode;
import de.machmireinebook.codelist.ProductFormCode;
import de.machmireinebook.codelist.ProductFormDetailCode;
import de.machmireinebook.codelist.ProductPackagingTypeCode;
import de.machmireinebook.codelist.ProductRelationCode;
import de.machmireinebook.codelist.TradeCategoryCode;
import de.machmireinebook.stringconverter.IntegerStringConverter;

/**
 * A repeatable group of data elements which together describe a product which has a specified relationship to the
 * product which is described in the ONIX record. Although for reasons of upwards compatibility the composite includes
 * individual fields for ISBN and EAN-13 number, use of the nested &lt;ProductIdentifier&gt; composite is to be preferred,
 * since it allows any recognized identifier scheme (eg DOI) to be used.
 *
 * The minimum required content of an occurrence of the &lt;RelatedProduct&gt; composite is a &lt;RelationCode&gt; and either a
 * product identifier or a &lt;ProductForm&gt; value. In other words, it is valid to list related products by relationship
 * and identifier only, or by relationship and form only.
 */
public class RelatedProduct extends OnixElement
{
    private OnixCodeListValueElement<ProductRelationCode> relationCode = new OnixCodeListValueElement<>("RelationCode", "h208", OnixOccurenceType.MANDATORY, ProductRelationCode::byCode);
    private List<ProductIdentifier> productIdentifiers = new ArrayList<>();
    private List<Website> websites = new ArrayList<>();
    private OnixCodeListValueElement<ProductFormCode> productForm = new OnixCodeListValueElement<>("ProductForm", "b012", ProductFormCode::byCode);
    private OnixCodeListValueElement<ProductFormDetailCode> productFormDetail = new OnixCodeListValueElement<>("ProductFormDetail", "b333", ProductFormDetailCode::byCode);
    private List<ProductFormFeature> formFeatures = new ArrayList<>();
    private OnixCodeListValueElement<ProductPackagingTypeCode> productPackaging = new OnixCodeListValueElement<>("ProductPackaging", "b225", ProductPackagingTypeCode::byCode);
    private OnixElement<String> productFormDescription = new OnixElement<>("ProductFormDescription", "b014");
    private OnixElement<Integer> numberOfPieces = new OnixElement<>("NumberOfPieces", "b210", new IntegerStringConverter());
    private OnixCodeListValueElement<TradeCategoryCode> tradeCategory = new OnixCodeListValueElement<>("TradeCategory", "b384", TradeCategoryCode::byCode);
    private OnixCodeListValueElement<ProductContentTypeCode> productContentType = new OnixCodeListValueElement<>("ProductContentType", "b385", ProductContentTypeCode::byCode);
    private OnixCodeListValueElement<ProductContentTypeCode> epubType = new OnixCodeListValueElement<>("EpubType", "b211", ProductContentTypeCode::byCode);
    private OnixElement<String> epubTypeVersion = new OnixElement<>("EpubTypeVersion", "b212");
    private OnixElement<String> epubTypeDescription  = new OnixElement<>("EpubTypeDescription", "b213");
    private OnixCodeListValueElement<EpublicationFormatCode> epubFormat = new OnixCodeListValueElement<>("EpubFormat", "b214", EpublicationFormatCode::byCode);
    private OnixElement<String> epubFormatVersion = new OnixElement<>("EpubFormatVersion", "b215");
    private OnixElement<String> epubTypeNote  = new OnixElement<>("EpubTypeNote", "b277");
    private List<Publisher> publishers = new ArrayList<>();

    public RelatedProduct()
    {
        super("RelatedProduct", "relatedproduct");
        addContent(relationCode);
        addContent(productForm);
        addContent(productFormDetail);
        addContent(productPackaging);
        addContent(productFormDescription);
        addContent(numberOfPieces);
        addContent(tradeCategory);
        addContent(productContentType);
        addContent(epubType);
        addContent(epubTypeVersion);
        addContent(epubTypeDescription);
        addContent(epubFormat);
        addContent(epubFormatVersion);
        addContent(epubTypeNote);
    }

    /**
     * An ONIX code which identifies the nature of the relationship between two products, eg 'replaced-by'. Mandatory
     * in each occurrence of the &lt;RelatedProduct&gt; composite, and non-repeating.
     *
     * @param relationCode
     */
    public void setRelationCode(OnixCodeListValueElement<ProductRelationCode> relationCode)
    {
        this.relationCode = relationCode;
    }

    /**
     * A repeatable group of data elements which together define the identifier of a product in accordance with a
     * specified scheme, and allowing other types of product identifier for a related product to be included without
     * defining additional data elements.
     *
     * @param productIdentifier
     */
    public void addProductIdentifier(ProductIdentifier productIdentifier)
    {
        productIdentifiers.add(productIdentifier);
        int index = indexOf(relationCode);
        addContent(index + productIdentifiers.size(), productIdentifier);
    }

    /**
     * An optional and repeatable group of data elements which together identify and provide pointers to a website which is relevant to the product identified in an occurrence of the &lt;RelatedProduct&gt; composite.
     *
     * @param website
     */
    public void addWebsite(Website website)
    {
        websites.add(website);
        int index = indexOf(relationCode);
        addContent(index + productIdentifiers.size() + websites.size()
                + websites.size(), website);
    }

    /**
     * An ONIX code which indicates the primary form of the product. Optional and non-repeating; required in any
     * occurrence of the &lt;RelatedProduct&gt; composite that does not carry a product identifier.
     *
     * @param productForm
     */
    public void setProductForm(ProductFormCode productForm)
    {
        this.productForm.setOnixValue(productForm);
    }

    /**
     * An ONIX code which provides added detail of the medium and/or format of the product. Optional and repeatable.
     *
     * @param productFormDetail
     */
    public void setProductFormDetail(ProductFormDetailCode productFormDetail)
    {
        this.productFormDetail.setOnixValue(productFormDetail);
    }

    /**
     * A repeatable group of data elements which together describe an aspect of product form that is too specific to
     * be covered in the &lt;ProductForm&gt; and &lt;ProductFormDetail&gt; elements. Optional.
     *
     * @param formFeature
     */
    public void addProductFormFeature(ProductFormFeature formFeature)
    {
        formFeatures.add(formFeature);
        int index = indexOf(productFormDetail);
        addContent(index + formFeatures.size(), formFeature);
    }

    /**
     * An ONIX code which indicates the type of packaging used for the product. Optional and non-repeating.
     *
     * @param productPackaging
     */
    public void setProductPackaging(ProductPackagingTypeCode productPackaging)
    {
        this.productPackaging.setOnixValue(productPackaging);
    }

    /**
     * If product form codes do not adequately describe the product, a short text description may be added. The text
     * may include the number and type of pieces contained in a multiple product, and/or a more detailed specification
     * of the product form. The field is optional and non-repeating.
     *
     * @param productFormDescription
     */
    public void setProductFormDescription(String productFormDescription)
    {
        this.productFormDescription.setText(productFormDescription);
    }

    /**
     * If the product is homogeneous (ie all items or pieces which constitute the product have the same form), the number
     * of items or pieces may be included here. If the product consists of a number of items or pieces of different
     * forms (eg books and audio cassettes), the &lt;ContainedItem&gt; composite should be used ' see below. This field is
     * optional and non-repeating.
     *
     * @param numberOfPieces
     */
    public void setNumberOfPieces(Integer numberOfPieces)
    {
        this.numberOfPieces.setOnixValue(numberOfPieces);
    }

    /**
     * An ONIX code which indicates a trade category which is somewhat related to but not properly an attribute of
     * product form. Optional and non-repeating.
     *
     * @param tradeCategory
     */
    public void setTradeCategory(TradeCategoryCode tradeCategory)
    {
        this.tradeCategory.setOnixValue(tradeCategory);
    }

    /**
     * An ONIX code which indicates certain types of content which are closely related to but not strictly an attribute
     * of product form, eg audiobook. Optional and repeatable. The element is intended to be used with products where
     * content is delivered in the form of a digital or analogue recording. It is not expected to be used for books.
     *
     * @param productContentType
     */
    public void setProductContentType(ProductContentTypeCode productContentType)
    {
        this.productContentType.setOnixValue(productContentType);
    }

    /**
     * An ONIX code identifying the type of an epublication. This element is mandatory if and only if the
     * &lt;ProductForm&gt; code for the product is DG.
     *
     * @param epubType
     */
    public void setEpubType(ProductContentTypeCode epubType)
    {
        if (!productForm.getOnixValue().equals(ProductFormCode.Electronic_book_text))
        {
            throw new IllegalOnixDataException("EpubType could only set if ProductForm is equals DG");
        }
        this.epubType.setOnixValue(epubType);
    }

    /**
     * A version number which applies to a specific epublication type. Optional and non-repeating, and can occur only
     * if the &lt;EpubType&gt; field is present.
     *
     * @param epubTypeVersion
     */
    public void setEpubTypeVersion(String epubTypeVersion)
    {
        if (epubType.getOnixValue() == null)
        {
            throw new IllegalOnixDataException("EpubTypeVersion could only set if EpubType is present");
        }
        this.epubTypeVersion.setText(epubTypeVersion);
    }

    /**
     * A free text description of an epublication type. Optional and non-repeating, and can occur only if the
     * &lt;EpubType&gt; field is present.
     *
     * @param epubTypeDescription
     */
    public void setEpubTypeDescription(String epubTypeDescription)
    {
        if (epubType.getOnixValue() == null)
        {
            throw new IllegalOnixDataException("EpubTypeDescription could only set if EpubType is present");
        }
        this.epubTypeDescription.setText(epubTypeDescription);
    }

    /**
     * An ONIX code identifying the underlying format of an epublication. Optional and non-repeating, and can occur only
     * if the &lt;EpubType&gt; field is present. Note that where the epublication type is wholly defined by the delivery
     * format, this element effectively duplicates the &lt;EpubType&gt; field.
     *
     * @param epubFormat
     */
    public void setEpubFormat(EpublicationFormatCode epubFormat)
    {
        this.epubFormat.setOnixValue(epubFormat);
    }

    /**
     * A version number which applies to an epublication format. Optional and non-repeating, and can occur only if the
     * &lt;EpubFormat&gt; field is present.
     *
     * @param epubFormatVersion
     */
    public void setEpubFormatVersion(String epubFormatVersion)
    {
        this.epubFormatVersion.setText(epubFormatVersion);
    }

    /**
     * A free text description of features of a product which are specific to its appearance as a particular epublication
     * type. Optional and non-repeatable, and can occur only if the &lt;EpubType&gt; field is present.
     *
     * @param epubTypeNote
     */
    public void setEpubTypeNote(String epubTypeNote)
    {
        if (epubType.getOnixValue() == null)
        {
            throw new IllegalOnixDataException("EpubTypeNote could only set if EpubType is present");
        }
        this.epubTypeNote.setText(epubTypeNote);
    }

    /**
     * A repeatable group of data elements which together identify an entity which is associated with the publishing of a
     * product. The composite will allow additional publishing roles to be introduced without adding new fields.
     * Each occurrence of the composite must carry either a name code or a name or both.
     *
     * @param publisher the publisher to add to onix message
     */
    public void addPublisher(Publisher publisher)
    {
        publishers.add(publisher);
        int index = indexOf(epubTypeNote);
        addContent(index + publishers.size(),  publisher);
    }

}
