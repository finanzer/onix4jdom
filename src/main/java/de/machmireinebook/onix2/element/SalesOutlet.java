package de.machmireinebook.onix2.element;

import de.machmireinebook.validation.ValidationMessage;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 * An optional and repeatable group of data elements which together identify a sales outlet to which a restriction is
 * linked. Each occurrence of the composite must include a &lt;SalesOutletIdentifier&gt; composite or a &lt;SalesOutletName&gt; or both.
 */
public class SalesOutlet extends OnixElement
{
    private SalesOutletIdentifier salesOutletIdentifier = new SalesOutletIdentifier();
    private OnixElement<String> salesOutletName = new OnixElement<>("SalesOutletName", "b382");

    public SalesOutlet()
    {
        super("SalesOutlet", "salesoutlet");
        addContent(salesOutletName);
    }

    /**
     * A group of data elements which together represent a coded identification of a person or organization, used here
     * to identify a sales outlet. Non-repeating in this context.
     *
     * @param salesOutletIdentifier
     */
    public void setSalesOutletIdentifier(SalesOutletIdentifier salesOutletIdentifier)
    {
        this.salesOutletIdentifier = salesOutletIdentifier;
        addContent(0, salesOutletIdentifier);
    }

    /**
     * The name of a wholesale or retail sales outlet to which a sales restriction is linked. Non-repeating.
     *
     * @param salesOutletName
     */
    public void setSalesOutletName(String salesOutletName)
    {
        this.salesOutletName.setText(salesOutletName);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        if (StringUtils.isEmpty(salesOutletName.getText()) && salesOutletIdentifier == null)
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("SalesOutlet", "Each occurrence of <SalesOutlet> must include a <SalesOutletIdentifier> composite or a <SalesOutletName> or both.", "SalesOutletIdentifier", "SalesOutletName"));
        }

        return result;
    }

}
