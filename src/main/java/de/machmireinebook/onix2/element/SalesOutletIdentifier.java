package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.SalesOutletIdentifierTypeCode;

/**
 * A group of data elements which together represent a coded identification of a person or organization, used here to
 * identify a sales outlet. Non-repeating in this context.
 */
public class SalesOutletIdentifier extends OnixElement
{
    private OnixCodeListValueElement<SalesOutletIdentifierTypeCode> salesOutletIDType = new OnixCodeListValueElement<>("SalesOutletIDType", "b393", OnixOccurenceType.MANDATORY, SalesOutletIdentifierTypeCode::byCode);
    private OnixElement<String> idTypeName = new OnixElement<>("IDTypeName", "b233");
    private OnixElement idValue = new OnixElement("IDValue", "b244", OnixOccurenceType.MANDATORY);

    public SalesOutletIdentifier()
    {
        super("SalesOutletIdentifier", "salesoutletidentifier");
        addContent(salesOutletIDType);
        addContent(idTypeName);
        addContent(idValue);
    }

    /**
     * An ONIX code which identifies the scheme from which the value in the &lt;IDValue&gt; element is taken. Mandatory in
     * each occurrence of the &lt;SalesOutletIdentifier&gt; composite, and non-repeating.
     *
     * @param salesOutletIDType
     */
    public void setSalesOutletIDType(SalesOutletIdentifierTypeCode salesOutletIDType)
    {
        this.salesOutletIDType.setOnixValue(salesOutletIDType);
    }

    /**
     * A name which identifies a proprietary identifier scheme when, and only when, the code in the &lt;SalesOutletIDType&gt;
     * element indicates a proprietary scheme. Optional and non-repeating.
     *
     * @param idTypeName
     */
    public void setIdTypeName(String idTypeName)
    {
        this.idTypeName.setText(idTypeName);
    }

    /**
     * A code value taken from the scheme specified in the &lt;SalesOutletIDType&gt; element. Mandatory in each occurrence of
     * the &lt;SalesOutletIdentifier&gt; composite, and non-repeating.
     *
     * @param idValue
     */
    public void setIdValue(String idValue)
    {
        this.idValue.setText(idValue);
    }
}
