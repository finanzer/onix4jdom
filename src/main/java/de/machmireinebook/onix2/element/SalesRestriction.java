package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.codelist.SalesRestrictionTypeCode;

/**
 * A group of data elements which together identify a non-territorial sales restriction which a publisher applies to
 * a product. Optional and repeatable.
 */
public class SalesRestriction extends OnixElement
{
    private OnixCodeListValueElement<SalesRestrictionTypeCode> salesRestrictionType = new OnixCodeListValueElement<>("SalesRestrictionType", "b381", SalesRestrictionTypeCode::byCode);
    private List<SalesOutlet> salesOutlets = new ArrayList<>();
    private OnixElement<String> salesRestrictionDetail = new OnixElement<>("SalesRestrictionDetail", "b383");

    public SalesRestriction()
    {
        super("SalesRestriction", "salesrestriction");
        addContent(salesRestrictionType);
        addContent(salesRestrictionDetail);
    }

    /**
     * An ONIX code which identifies a non-territorial sales restriction. Mandatory in each occurrence of the
     * &lt;SalesRestriction&gt; composite, and non-repeating.
     *
     * @param salesRestrictionType
     */
    public void setSalesRestrictionType(SalesRestrictionTypeCode salesRestrictionType)
    {
        this.salesRestrictionType.setOnixValue(salesRestrictionType);
    }

    /**
     * An optional and repeatable group of data elements which together identify a sales outlet to which a restriction is
     * linked. Each occurrence of the composite must include a &lt;SalesOutletIdentifier&gt; composite or a &lt;SalesOutletName&gt; or both.
     *
     * @param salesOutlet
     */
    public void addSalesOutlet(SalesOutlet salesOutlet)
    {
        salesOutlets.add(salesOutlet);
        int index = indexOf(salesRestrictionType);
        addContent(index + salesOutlets.size(),  salesOutlet);
    }

    /**
     * A free text field describing an 'unspecified' restriction, or giving more explanation of a coded restriction type.
     * Optional and non-repeating.
     *
     * @param salesRestrictionDetail
     */
    public void setSalesRestrictionDetail(String salesRestrictionDetail)
    {
        this.salesRestrictionDetail.setText(salesRestrictionDetail);
    }
}
