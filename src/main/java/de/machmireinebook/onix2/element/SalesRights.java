package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.CountryCode;
import de.machmireinebook.codelist.RegionCode;
import de.machmireinebook.codelist.SalesRightsTypeCode;
import de.machmireinebook.validation.ValidationMessage;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 * A repeatable group of data elements which together identify territorial sales rights which a publisher chooses to
 * exercise in a product. The &lt;SalesRights&gt; composite may occur once for each value of &lt;b089&gt;.
 *
 * Example:
 *
 UK edition with ISBN of US equivalent
 Short tags
 {@code
 <salesrights>
    <b089>01</b089>	Available with exclusive rights in
    <b090>GB IE AU NZ ZA</b090>	UK, Ireland, Australia, New Zealand, South Africa
 </salesrights>
 <salesrights>
    <b089>02</b089>	Available with non-exclusive rights in
    <b388>ROW</b388>	Rest of world
 </salesrights>
 <notforsale>	Not for sale in
    <b090>US CA</b090>	USA, Canada
    <productidentifier>
        <b221>02</b221>	ID type = ISBN
        <b244>0123456784</b244>	ISBN of equivalent product in USA and Canada
    </productidentifier>
 </notforsale>
 }
 * Release 2.1 allows rights to be specified for any geographical territory. Group PR.21 details the rights that the
 * publisher chooses to exercise in the product described by the ONIX record. These may be different from the rights
 * owned by the publisher in the underlying work (which are not specified in an ONIX for Books Product Record) and from
 * the distribution rights exercised by a particular supplier (see Group PR.24).
 *
 * The aim is to provide precise and reliable geographical rights information that can be used in a computer system to
 * determine whether a product can or cannot be sold in a particular territory. There are no defaults. If no information
 * if given about a particular territory, it must not be assumed that rights are, or are not, held.
 *
 * The &lt;SalesRights&gt; composite allows rights to be specified as exclusive or non-exclusive or not-for-sale in any
 * combination of countries or country subdivisions. It is also possible to specify rights as 'worldwide' or 'worldwide
 * with specified exclusions' if this enables them to be stated more concisely.
 *
 * The &lt;NotForSale&gt; composite allows details of an equivalent product to be sent in respect of a country or countries
 * in which the product described in the ONIX record is not for sale. This information is particularly helpful in
 * enabling international online booksellers to ensure that territorial rights are correctly identified and observed.
 * It is therefore recommended that wherever possible the &lt;NotForSale&gt; composite should be used in preference to
 * the &lt;SalesRights&gt; composite with code value 03 in &lt;SalesRightsType&gt;. However, both methods of expressing
 * 'not for sale' remain valid.
 *
 * Special note on US and UK 'Open Market', and UK 'Airport' or 'Airside', editions. It is expected that such editions,
 * like any others, should carry a full statement of the territories in which they are available for sale. If it is
 * desired, as a matter of convenience, to refer to them as 'Open Market' or 'Airside', this should be additional to,
 * not instead of, a full territorial rights statement, and should be handled through the new &lt;TradeCategory&gt; element
 * in Group PR.3. Coding for 'UK Airports' and 'UK Airside' is included in the new element &lt;RightsTerritory&gt;, as these
 * need to be distinguished as specific 'territories', but there should be no coding for 'Open Market' within this
 * data element group.
 */
public class SalesRights extends OnixElement
{
    private OnixCodeListValueElement<SalesRightsTypeCode> salesRightsType = new OnixCodeListValueElement<>("SalesRightsType", "b089", OnixOccurenceType.MANDATORY, SalesRightsTypeCode::byCode);
    private OnixCodeListValueElement<CountryCode> rightsCountry = new OnixCodeListValueElement<>("RightsCountry", "b090", CountryCode::byCode);
    private OnixCodeListValueElement<RegionCode> rightsTerritory = new OnixCodeListValueElement<>("RightsTerritory", "b388", RegionCode::byCode);


    public SalesRights()
    {
        super("SalesRights", "salesrights");
        addContent(salesRightsType);
        addContent(rightsCountry);
        addContent(rightsTerritory);
    }

    /**
     * An ONIX code which identifies the type of sales right or exclusion which applies in the territories which are
     * associated with it. Mandatory in each occurrence of the &lt;SalesRights&gt; composite, and non-repeating.
     *
     * @param salesRightsType
     */
    public void setSalesRightsType(SalesRightsTypeCode salesRightsType)
    {
        this.salesRightsType.setOnixValue(salesRightsType);
    }

    /**
     * One or more ISO standard codes identifying a country. Successive codes may be separated by spaces. Thus, a
     * single occurrence of the element can carry an unlimited number of country codes, for countries that share the
     * sales rights specified in &lt;SalesRightsType&gt;. For upwards compatibility, the element remains repeatable, so that
     * multiple countries can also be listed as multiple occurrences of the whole element. At least one occurrence of
     * &lt;RightsCountry&gt; or &lt;RightsTerritory&gt; or &lt;RightsRegion&gt; is mandatory in any occurrence of the &lt;SalesRights&gt; composite.
     *
     * @param rightsCountry
     */
    public void setRightsCountry(CountryCode rightsCountry)
    {
        this.rightsCountry.setOnixValue(rightsCountry);
    }

    /**
     * One or more ONIX codes identifying a territory which is not a country, but which is precisely defined in
     * geographical terms, eg World, Northern Ireland, Australian National Territory. Successive codes are separated
     * by spaces, so that the element can carry an unlimited number of territory codes, for territories that share
     * the sales rights specified in &lt;SalesRightsType&gt;. Optional and non-repeating.
     *
     * @param rightsTerritory
     */
    public void setRightsTerritory(RegionCode rightsTerritory)
    {
        this.rightsTerritory.setOnixValue(rightsTerritory);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        if (StringUtils.isEmpty(rightsCountry.getText()) && StringUtils.isEmpty(rightsTerritory.getText()))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("SalesRights", " At least one occurrence of " +
                    " <RightsCountry> or <RightsTerritory> is mandatory in any occurrence of the<SalesRights> composite.",
                    "RightsCountry", "RightsTerritory"));
        }

        return result;
    }
}
