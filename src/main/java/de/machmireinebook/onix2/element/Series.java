package de.machmireinebook.onix2.element;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.stringconverter.IntegerStringConverter;

/**
 * User: mjungierek
 * Date: 30.07.2016
 * Time: 23:07
 */
public class Series extends OnixElement
{
    private List<SeriesIdentifier> seriesIdentifiers = new ArrayList<>();
    private OnixElement<String> titleOfSeries = new OnixElement<>("TitleOfSeries", "b018");
    private List<Title> titles = new ArrayList<>();
    private List<Contributor> contributors = new ArrayList<>();
    private OnixElement<Integer> numberWithinSeries = new OnixElement<>("NumberWithinSeries", "b019", new IntegerStringConverter());
    private OnixElement pubSequenceNumberWithinSeries = new OnixElement("PubSequenceNumberWithinSeries", "b222");
    private OnixElement<Year> yearOfAnnual = new OnixElement<>("YearOfAnnual", "b020");

    public Series()
    {
        super("Series", "Series");
        addContent(titleOfSeries);
        addContent(numberWithinSeries);
        addContent(pubSequenceNumberWithinSeries);
        addContent(yearOfAnnual);
    }

    public void addSeries(SeriesIdentifier seriesIdentifier)
    {
        seriesIdentifiers.add(seriesIdentifier);
        addContent(seriesIdentifiers.size(), seriesIdentifier);
    }

    public void setTitleOfSeries(String titleOfSeries)
    {
        this.titleOfSeries.setText(titleOfSeries);
    }

    public void addTitle(Title title)
    {
        titles.add(title);
        addContent(seriesIdentifiers.size() + 1  + titles.size(), title);
    }

    public void addContributor(Contributor contributor)
    {
        contributors.add(contributor);
        addContent(seriesIdentifiers.size() + 1  + titles.size() + contributors.size(), contributor);
    }

    public void setNumberWithinSeries(Integer numberWithinSeries)
    {
        this.numberWithinSeries.setOnixValue(numberWithinSeries);
    }

    public void setPubSequenceNumberWithinSeries(String pubSequenceNumberWithinSeries)
    {
        this.pubSequenceNumberWithinSeries.setText(pubSequenceNumberWithinSeries);
    }

    public void setYearOfAnnual(Year yearOfAnnual)
    {
        this.yearOfAnnual.setOnixValue(yearOfAnnual);
    }
}
