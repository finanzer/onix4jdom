package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.SeriesIdentifierTypeCode;

/**
 * User: mjungierek
 * Date: 30.07.2016
 * Time: 23:13
 */
public class SeriesIdentifier extends OnixElement
{
    private OnixCodeListValueElement<SeriesIdentifierTypeCode> seriesIDType = new OnixCodeListValueElement<>("SeriesIDType", "b273", SeriesIdentifierTypeCode::byCode);
    private OnixElement idTypeName = new OnixElement("IDTypeName", "b233");
    private OnixElement idValue = new OnixElement("IDValue", "b244", OnixOccurenceType.MANDATORY);

    public SeriesIdentifier()
    {
        super("SeriesIdentifier", "seriesidentifier");
        addContent(seriesIDType);
        addContent(idTypeName);
        addContent(idValue);
    }

    public void setSeriesIDType(SeriesIdentifierTypeCode seriesIDType)
    {
        this.seriesIDType.setOnixValue(seriesIDType);
    }

    public void setIdTypeName(String idTypeName)
    {
        this.idTypeName.setText(idTypeName);
    }

    public void setIdValue(String idValue)
    {
        this.idValue.setText(idValue);
    }
}
