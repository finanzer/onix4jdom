package de.machmireinebook.onix2.element;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.stringconverter.IntegerStringConverter;
import de.machmireinebook.validation.ValidationMessage;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 * A repeatable group of data elements which together specify a quantity of stock and, where a supplier has more than
 * one warehouse, a supplier location. Optional.
 */
public class Stock extends OnixElement
{
    private LocationIdentifier locationIdentifier;
    private OnixElement<String> locationName = new OnixElement<>("LocationName", "j349");
    private StockQuantityCoded stockQuantityCoded;
    private OnixElement<Integer> onHand = new OnixElement<>("OnHand", "j350", new IntegerStringConverter());
    private OnixElement<Integer> onOrder = new OnixElement<>("OnOrder", "j351", new IntegerStringConverter());
    private OnixElement<Integer> cbo = new OnixElement<>("CBO", "j375", new IntegerStringConverter());
    private List<OnOrderDetail> onOrderDetails = new ArrayList<>();

    public Stock()
    {
        super("Stock", "stock");
        addContent(locationName);
        addContent(onHand);
        addContent(onOrder);
        addContent(cbo);
    }

    /**
     * A group of data elements which together define the identifier of a stock location in accordance with a specified
     * scheme, and allowing different types of location identifier to be supported without defining additional data
     * elements. Optional and non-repeating.
     *
     * @param locationIdentifier LocationIdentifier object
     */
    public void setLocationIdentifier(LocationIdentifier locationIdentifier)
    {
        this.locationIdentifier = locationIdentifier;
        addContent(0, locationIdentifier);
    }

    /**
     * The name of a stock location. Optional and non-repeating.
     *
     * @param locationName Free text, suggested maximum length 100 characters
     */
    public void setLocationName(String locationName)
    {
        this.locationName.setText(locationName);
    }

    /**
     * A group of data elements which together specify coded stock level without stating the exact quantity of stock.
     * Either &lt;StockQuantityCoded&gt; or &lt;OnHand&gt; is mandatory in each occurrence of the &lt;Stock&gt; composite, even if the
     * onhand quantity is zero. Non-repeating.
     *
     * @param stockQuantityCoded StockQuantityCoded object
     */
    public void setStockQuantityCoded(StockQuantityCoded stockQuantityCoded)
    {
        this.stockQuantityCoded = stockQuantityCoded;
        int index = indexOf(locationName);
        addContent(index, stockQuantityCoded);
    }

    /**
     * The quantity of stock on hand. Either &lt;StockQuantityCoded&gt; or &lt;OnHand&gt; is mandatory in each occurrence of the
     * &lt;Stock&gt; composite, even if the onhand quantity is zero. Non-repeating.
     *
     * @param onHand Variable-length integer, suggested maximum length 7 digits
     */
    public void setOnHand(Integer onHand)
    {
        this.onHand.setOnixValue(onHand);
    }

    /**
     * The quantity of stock on order. Optional and non-repeating.
     *
     * @param onOrder Variable-length integer, suggested maximum length 7 digits
     */
    public void setOnOrder(Integer onOrder)
    {
        this.onOrder.setOnixValue(onOrder);
    }

    /**
     * The quantity of stock on order which is already committed to meet backorders. Optional and non-repeating.
     *
     * @param cbo Variable-length integer, suggested maximum length 7 digits
     */
    public void setCbo(Integer cbo)
    {
        this.cbo.setOnixValue(cbo);
    }

    /**
     * A repeatable group of data elements which together specify details of a stock shipment currently awaited,
     * normally from overseas. Optional.
     *
     * @param onOrderDetail OnOrderDetail object
     */
    public void addOnOrderDetail(OnOrderDetail onOrderDetail)
    {
        onOrderDetails.add(onOrderDetail);
        int index = indexOf(cbo);
        addContent(index + onOrderDetails.size(), onOrderDetail);
    }


    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        if ((stockQuantityCoded == null || stockQuantityCoded.getChildren().isEmpty()) && StringUtils.isEmpty(onHand.getText()))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("Stock", "Either <StockQuantityCoded> or <OnHand> is mandatory in each occurrence of the <Stock> composite", "StockQuantityCoded", "OnHand"));
        }

        return result;
    }
}
