package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.StockQuantityCodeTypeCode;

/**
 * User: mjungierek
 * Date: 06.09.2016
 * Time: 21:57
 */
public class StockQuantityCoded extends OnixElement
{
    private OnixCodeListValueElement<StockQuantityCodeTypeCode> stockQuantityCodeType = new OnixCodeListValueElement<>("StockQuantityCodeType", "j293", OnixOccurenceType.MANDATORY, StockQuantityCodeTypeCode::byCode);
    private OnixElement<String> stockQuantityCodeTypeName = new OnixElement<>("StockQuantityCodeTypeName", "j296");
    private OnixElement<String> stockQuantityCode = new OnixElement<>("StockQuantityCode", "j297", OnixOccurenceType.MANDATORY);

    public StockQuantityCoded()
    {
        super("StockQuantityCoded", "stockquantitycoded");
        addContent(stockQuantityCodeType);
    }

    /**
     * An ONIX code identifying the scheme from which the value in the &lt;StockQuantityCode&gt; element is taken. Mandatory
     * in each occurrence of the &lt;StockQuantityCoded&gt; composite, and non-repeating.
     *
     * @param stockQuantityCodeType StockQuantityCodeTypeCode object
     */
    public void setStockQuantityCodeType(StockQuantityCodeTypeCode stockQuantityCodeType)
    {
        this.stockQuantityCodeType.setOnixValue(stockQuantityCodeType);
    }

    /**
     * A name that identifies a proprietary stock quantity coding scheme when the code in the &lt;StockQuantityCodeType&gt;
     * element indicates a proprietary scheme, eg a wholesaler’s own code. Optional, and non-repeating.
     *
     * @param stockQuantityCodeTypeName Free text, suggested maximum length 50 characters
     */
    public void setStockQuantityCodeTypeName(String stockQuantityCodeTypeName)
    {
        this.stockQuantityCodeTypeName.setText(stockQuantityCodeTypeName);
    }

    /**
     * A code value taken from the scheme specified in the &lt;StockQuantityCodeType&gt; element. Mandatory in each
     * occurrence of the &lt;StockQuantityCoded&gt; composite, and non-repeating.
     *
     * @param stockQuantityCode According to the scheme specified in &lt;StockQuantityCodeType&gt;
     */
    public void setStockQuantityCode(String stockQuantityCode)
    {
        this.stockQuantityCode.setText(stockQuantityCode);
    }
}
