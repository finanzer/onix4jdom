package de.machmireinebook.onix2.element;

import de.machmireinebook.IllegalOnixDataException;
import de.machmireinebook.codelist.SubjectSchemeIdentifierCode;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.StringUtils;

/**
 * An optional and repeatable group of data elements which together describe a subject classification or subject heading
 * which is additional to the BISAC, BIC or other main subject category.
 */
public class Subject extends OnixElement
{
    private OnixCodeListValueElement<SubjectSchemeIdentifierCode> subjectSchemeIdentifier
            = new OnixCodeListValueElement<>("SubjectSchemeIdentifier", "b067", OnixOccurenceType.MANDATORY, SubjectSchemeIdentifierCode::byCode);
    private OnixElement<String> subjectSchemeName = new OnixElement<>("SubjectSchemeName", "b171");
    private OnixElement<String> subjectSchemeVersion = new OnixElement<>("SubjectSchemeVersion", "b068");
    private OnixElement<String> subjectCode = new OnixElement<>("SubjectCode", "b069");
    private OnixElement<String> subjectHeadingText = new OnixElement<>("SubjectHeadingText", "b070");

    public Subject()
    {
        super("Subject", "subject");
        addContent(subjectSchemeIdentifier);
        addContent(subjectSchemeName);
        addContent(subjectSchemeVersion);
        addContent(subjectCode);
        addContent(subjectHeadingText);
    }

    /**
     * An ONIX code which identifies the subject scheme which is used in an occurrence of the &lt;Subject&gt; composite.
     * Mandatory in each occurrence of the composite, and non-repeating.
     *
     * When the scheme listed in the code list display is annotated 'Code', use the associated &lt;SubjectCode&gt; element
     * to carry the value (if so required, the &lt;SubjectHeadingText&gt; element can be used simultaneously to carry the
     * text equivalent of the code). When the scheme is annotated 'Text', use the &lt;SubjectHeadingText&gt; element to carry
     * the text of the subject heading.
     *
     * Scheme code 23 may be used for a publisher's own subject category code, by agreement with trading partners
     * to whom product information is sent. Scheme code 24, with a name in the &lt;SubjectSchemeName&gt; element, may be
     * used to identify a proprietary scheme, eg one used by a bibliographic agency or wholesaler.
     *
     * @param subjectSchemeIdentifier
     */
    public void setSubjectSchemeIdentifier(SubjectSchemeIdentifierCode subjectSchemeIdentifier)
    {
        this.subjectSchemeIdentifier.setOnixValue(subjectSchemeIdentifier);
    }

    /**
     * A name identifying a proprietary subject scheme when &lt;SubjectSchemeIdentifier&gt; is coded '24'.
     * Optional and non-repeating.
     */
    public void setSubjectSchemeName(String subjectSchemeName)
    {
        if (!subjectSchemeIdentifier.getOnixValue().equals(SubjectSchemeIdentifierCode.Proprietary_subject_scheme))
        {
            throw new IllegalOnixDataException("SubjectSchemeName could only set if SubjectSchemeName is equals Proprietary_subject_scheme (Value: 24)");
        }
        this.subjectSchemeName.setText(subjectSchemeName);
    }

    /**
     * A number which identifies a version or edition of the subject scheme specified in the associated
     * &lt;MainSubjectSchemeIdentifier&gt; element. Optional and non-repeating.
     *
     * @param subjectSchemeVersion
     */
    public void setSubjectSchemeVersion(String subjectSchemeVersion)
    {
        this.subjectSchemeVersion.setText(subjectSchemeVersion);
    }

    /**
     * A subject class or category code from the scheme specified in the &lt;MainSubjectSchemeIdentifier&gt; element. Either
     * &lt;SubjectCode&gt; or &lt;SubjectHeadingText&gt; or both must be present in each occurrence of the &lt;MainSubject&gt;
     * composite. Non-repeating.
     */
    public void setSubjectCode(String subjectCode)
    {
        this.subjectCode.setText(subjectCode);
    }

    /**
     * The text of a heading taken from the scheme specified in the &lt;MainSubjectSchemeIdentifier&gt; element; or the text
     * equivalent to the &lt;SubjectCode&gt; value, if both code and text are sent. Either &lt;SubjectCode&gt; or
     * &lt;SubjectHeadingText&gt; or both must be present in each occurrence of the &lt;MainSubject&gt; composite. Non-repeating.
     *
     * @param subjectHeadingText
     */
    public void setSubjectHeadingText(String subjectHeadingText)
    {
        this.subjectHeadingText.setText(subjectHeadingText);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        result.setValid(result.isValid() && StringUtils.isNotEmpty(subjectCode.getText()) || StringUtils.isNotEmpty(subjectHeadingText.getText()));
        return result;
    }

}
