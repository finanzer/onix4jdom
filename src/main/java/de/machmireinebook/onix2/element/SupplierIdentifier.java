package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.SupplierIdentifierTypeCode;

/**
 * A repeatable group of data elements which together define the identifier of a supplier in accordance with a specified
 * scheme, and allowing different types of supplier identifier to be included without defining additional data elements.
 * Optional, but each occurrence of the &lt;SupplyDetail&gt; composite must carry either at least one supplier identifier, or
 * a &lt;SupplierName&gt;.
 */
public class SupplierIdentifier extends OnixElement
{
    private OnixCodeListValueElement<SupplierIdentifierTypeCode> supplierIDType = new OnixCodeListValueElement<>("SupplierIDType", "j345", OnixOccurenceType.MANDATORY, SupplierIdentifierTypeCode::byCode);
    private OnixElement idTypeName = new OnixElement("IDTypeName", "b233");
    private OnixElement idValue = new OnixElement("IDValue", "b244", OnixOccurenceType.MANDATORY);

    public SupplierIdentifier()
    {
        super("SupplierIdentifier", "supplieridentifier");
        addContent(supplierIDType);
        addContent(idTypeName);
        addContent(idValue);
    }

    /**
     * An ONIX code identifying the scheme from which the identifier in the &lt;IDValue&gt; element is taken. Mandatory in
     * each occurrence of the &lt;SupplierIdentifier&gt; composite, and non-repeating.
     *
     * @param supplierIdentifierTypeCode
     */
    public void setSupplierIDType(SupplierIdentifierTypeCode supplierIdentifierTypeCode)
    {
        this.supplierIDType.setOnixValue(supplierIdentifierTypeCode);
    }

    /**
     * A name which identifies a proprietary identifier scheme when, and only when, the code in the &lt;SupplierIDType&gt;
     * element indicates a proprietary scheme, eg a wholesaler’s own code. Optional and non-repeating.
     *
     * @param idTypeName
     */
    public void setIdTypeName(String idTypeName)
    {
        this.idTypeName.setText(idTypeName);
    }

    public void setIdValue(String idValue)
    {
        this.idValue.setText(idValue);
    }

}
