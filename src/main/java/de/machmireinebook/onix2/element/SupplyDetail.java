package de.machmireinebook.onix2.element;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.IllegalOnixDataException;
import de.machmireinebook.codelist.AudienceRestrictionFlagCode;
import de.machmireinebook.codelist.CountryCode;
import de.machmireinebook.codelist.DateFormatCode;
import de.machmireinebook.codelist.ProductAvailabilityCode;
import de.machmireinebook.codelist.RegionCode;
import de.machmireinebook.codelist.ReturnsConditionsCodeTypeCode;
import de.machmireinebook.codelist.SupplierRoleCode;
import de.machmireinebook.codelist.UnpricedItemTypeCode;
import de.machmireinebook.stringconverter.CountryCodeListStringConverter;
import de.machmireinebook.stringconverter.IntegerStringConverter;
import de.machmireinebook.stringconverter.LocalDateStringConverter;
import de.machmireinebook.stringconverter.StringConverter;
import de.machmireinebook.validation.ValidationMessage;
import de.machmireinebook.validation.ValidationResult;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

/**
 * A repeatable group of data elements which together give details of a trade supply source and the product price and
 * availability from that source.
 *
 * Not implemented: PR.24.21 Availability status code, it's not recommended to use, but not deprecated, the preferred element
 * AvailabilityCode is implemented course.
 */
public class SupplyDetail extends OnixElement
{
    private OnixElement<String> supplierEANLocationNumber = new OnixElement<>("SupplierEANLocationNumber", "j135");
    private OnixElement<String> supplierSAN = new OnixElement<>("SupplierSAN", "j136");
    private List<SupplierIdentifier> supplierIdentifiers = new ArrayList<>();
    private OnixElement<String> supplierName = new OnixElement<>("SupplierName", "j137");
    private OnixElement<String> telephoneNumber = new OnixElement<>("TelephoneNumber", "j270");
    private OnixElement<String> faxNumber = new OnixElement<>("FaxNumber", "j271");
    private OnixElement<String> emailAddress = new OnixElement<>("EmailAddress", "j272");
    private List<Website> websites = new ArrayList<>();
    private OnixCodeListValueElement<SupplierRoleCode> supplierRole = new OnixCodeListValueElement<>("SupplierRole", "j292", SupplierRoleCode::byCode);
    private OnixElement<List<CountryCode>> supplyToCountry = new OnixElement<>("SupplyToCountry", "j138", new CountryCodeListStringConverter());
    private OnixElement<List<RegionCode>> supplyToTerritory = new OnixElement<>("SupplyToTerritory", "j397", new StringConverter<List<RegionCode>>()
    {
        @Override
        public String toString(List<RegionCode> regionCodes)
        {
            return StringUtils.join(regionCodes, " ");
        }

        @Override
        public List<RegionCode> fromString(String codes)
        {
            List<RegionCode> regionCodes = new ArrayList<>();
            String[] codeStrings = StringUtils.split(codes, " ");
            for (String codeString : codeStrings)
            {
                regionCodes.add(RegionCode.byCode(codeString));
            }
            return regionCodes;
        }
    });
    private OnixElement<List<CountryCode>> supplyToCountryExcluded = new OnixElement<>("SupplyToCountryExcluded", "j140", new CountryCodeListStringConverter());
    private OnixElement<String> supplyRestrictionDetail = new OnixElement<>("SupplyRestrictionDetail", "j399");
    private OnixCodeListValueElement<ReturnsConditionsCodeTypeCode> returnsCodeType = new OnixCodeListValueElement<>("ReturnsCodeType", "j268", ReturnsConditionsCodeTypeCode::byCode);
    private OnixElement<String> returnsCode = new OnixElement<>("ReturnsCode", "j269");
    private OnixElement<LocalDate> lastDateForReturns = new OnixElement<>("LastDateForReturns", "j387", new LocalDateStringConverter("yyyyMMdd"));
    private OnixCodeListValueElement<ProductAvailabilityCode> productAvailability = new OnixCodeListValueElement<>("ProductAvailability", "j396", ProductAvailabilityCode::byCode);
    private NewSupplier newSupplier;
    private OnixCodeListValueElement<DateFormatCode> dateFormat = new OnixCodeListValueElement<>("DateFormat", "j260", DateFormatCode::byCode);
    private OnixElement<LocalDate> expectedShipDate = new OnixElement<>("ExpectedShipDate", "j142");
    private OnixElement<LocalDate> onSaleDate = new OnixElement<>("OnSaleDate", "j143", new LocalDateStringConverter("yyyyMMdd"));
    private OnixElement<Integer> orderTime = new OnixElement<>("OrderTime", "j144", new IntegerStringConverter());
    private List<Stock> stocks = new ArrayList<>();
    private OnixElement<Integer> packQuantity = new OnixElement<>("PackQuantity", "j145", new IntegerStringConverter());
    private OnixCodeListValueElement<AudienceRestrictionFlagCode> audienceRestrictionFlag = new OnixCodeListValueElement<>("AudienceRestrictionFlag", "j146", AudienceRestrictionFlagCode::byCode);
    private OnixElement<String> audienceRestrictionNote = new OnixElement<>("AudienceRestrictionNote", "j147");
    private OnixCodeListValueElement<UnpricedItemTypeCode> unpricedItemType = new OnixCodeListValueElement<>("UnpricedItemType", "j192", UnpricedItemTypeCode::byCode);
    private List<Price> prices = new ArrayList<>();


    public SupplyDetail()
    {
        super("SupplyDetail", "supplydetail");
        addContent(supplierEANLocationNumber);
        addContent(supplierSAN);
        addContent(supplierName);
        addContent(telephoneNumber);
        addContent(faxNumber);
        addContent(emailAddress);
        addContent(supplierRole);
        addContent(supplyToCountry);
        addContent(supplyToTerritory);
        addContent(supplyToCountryExcluded);
        addContent(supplyRestrictionDetail);
        addContent(returnsCodeType);
        addContent(returnsCode);
        addContent(lastDateForReturns);
        addContent(productAvailability);
        addContent(dateFormat);
        addContent(expectedShipDate);
        addContent(onSaleDate);
        addContent(orderTime);
        addContent(packQuantity);
        addContent(audienceRestrictionFlag);
        addContent(audienceRestrictionNote);
        addContent(unpricedItemType);
    }

    /**
     * An EAN-13 location number identifying a supply source from which the product may be ordered by a trade customer.
     * Now also known as an “EAN-UCC Global Location Number” or GLN. Optional, but each occurrence of the &lt;SupplyDetail&gt;
     * composite must carry either at least one supplier identifier, or a &lt;SupplierName&gt;.
     *
     * @param supplierEANLocationNumber Fixed-length, thirteen numeric digits, of which the last is a check digit.
     */
    public void setSupplierEANLocationNumber(String supplierEANLocationNumber)
    {
        if (supplierEANLocationNumber.length() != 13 || !StringUtils.isNumeric(supplierEANLocationNumber))
        {
            throw new IllegalOnixDataException("SupplierEANLocationNumber is a fixed-length number, with thirteen digits, of which the last is a check digit.");
        }
        this.supplierEANLocationNumber.setText(supplierEANLocationNumber);
    }

    /**
     * The book trade Standard Address Number that identifies the supplier with whom trade orders for the product
     * should be placed. Used in the US and UK. Optional, but each occurrence of the &lt;SupplyDetail&gt; composite must
     * carry either at least one supplier identifier, or a &lt;SupplierName&gt;.
     *
     * @param supplierSAN
     */
    public void setSupplierSAN(String supplierSAN)
    {
        if (supplierSAN.length() != 7)
        {
            throw new IllegalOnixDataException("SupplierSAN is a fixed-length string, with seven digits or six digits and the letter X.");
        }
        this.supplierSAN.setText(supplierSAN);
    }

    /**
     * A repeatable group of data elements which together define the identifier of a supplier in accordance with a
     * specified scheme, and allowing different types of supplier identifier to be included without defining additional
     * data elements. Optional, but each occurrence of the &lt;SupplyDetail&gt; composite must carry either at least
     * one supplier identifier, or a &lt;SupplierName&gt;.
     *
     * @param supplierIdentifier
     */
    public void addSupplierIdentifier(SupplierIdentifier supplierIdentifier)
    {
        supplierIdentifiers.add(supplierIdentifier);
        int index = indexOf(supplierSAN);
        addContent(index + supplierIdentifiers.size(), supplierIdentifier);
    }

    /**
     * The name of a supply source from which the product may be ordered by a trade customer. Optional and non-repeating;
     * required if no supplier identifier is sent.
     *
     * @param supplierName Variable-length text, suggested maximum length 100 characters
     */
    public void setSupplierName(String supplierName)
    {
        this.supplierName.setText(supplierName);
    }

    /**
     * A telephone number of a supply source from which the product may be ordered by a trade customer. Optional and repeatable.
     *
     * @param telephoneNumber Variable-length text, suggested maximum length 20 characters
     */
    public void setTelephoneNumber(String telephoneNumber)
    {
        this.telephoneNumber.setText(telephoneNumber);
    }

    /**
     * A fax number of a supply source from which the product may be ordered by a trade customer. Optional and repeatable.
     *
     * @param faxNumber Variable-length text, suggested maximum length 20 characters
     */
    public void setFaxNumber(String faxNumber)
    {
        this.faxNumber.setText(faxNumber);
    }

    /**
     * An email address for a supply source from which the product may be ordered by a trade customer. Optional and repeatable.
     *
     * @param emailAddress Variable-length text, suggested maximum length 100 characters
     */
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress.setText(emailAddress);
    }

    /**
     * An optional and repeatable group of data elements which together identify and provide pointers to a website which
     * is related to the supplier identified in an occurrence of the &lt;SupplyDetail&gt; composite.
     *
     * @param website Website object
     */
    public void addWebsite(Website website)
    {
        websites.add(website);
        int index = indexOf(emailAddress);
        addContent(index + websites.size(), website);
    }

    /**
     * An ONIX code identifying the role of a supplier in relation to the product, eg Publisher, Publisher’s exclusive
     * distributor, etc. Optional and non-repeating.
     *
     * @param supplierRole SupplierRoleCode object
     */
    public void setSupplierRole(SupplierRoleCode supplierRole)
    {
        this.supplierRole.setOnixValue(supplierRole);
    }

    /**
     * One or more ISO standard codes identifying a country for which the supplier holds distribution rights for the
     * product. Successive codes may be separated by spaces. Thus, a single occurrence of the element can carry an
     * unlimited number of country codes. The possibility to repeat this element for upwards compatibility, is not implemented.
     *
     * @param countries  A list of {@link CountryCode}s, suggested maximum length 200 codes
     */
    public void setSupplyToCountry(List<CountryCode> countries)
    {
        this.supplyToCountry.setOnixValue(countries);
    }

    /**
     * One or more ONIX codes identifying a territory which is not a country, but which is precisely defined in
     * geographical terms, eg World, Northern Ireland, Australian Capital Territory. Successive codes may be separated
     * by spaces. Thus the element can carry an unlimited number of territory codes, for territories for which
     * the supplier has distribution rights. Optional and non-repeating.
     *
     * @param territories A list of CountryCodes, suggested maximum length 20 codes
     */
    public void setSupplyToRegion(List<RegionCode> territories)
    {
        this.supplyToTerritory.setOnixValue(territories);
    }

    /**
     * One or more ISO standard codes identifying a country which is excluded from a territory specified in &lt;SupplyToTerritory&gt;.
     * The possibility to repeat this element for upwards compatibility, is not implemented.
     *
     * @param countries  A list of CountryCodes, suggested maximum length 100 codes
     */
    public void setSupplyToCountryExcluded(List<CountryCode> countries)
    {
        this.supplyToCountryExcluded.setOnixValue(countries);
    }

    /**
     * A free text field describing a non-geographical restriction of the market covered by a distributor or other supplier.
     * Optional and non-repeating.
     *
     * @param supplyRestrictionDetail Variable-length text, suggested maximum length 300 characters
     */
    public void setSupplyRestrictionDetail(String supplyRestrictionDetail)
    {
        this.supplyRestrictionDetail.setText(supplyRestrictionDetail);
    }

    /**
     * An ONIX code identifying the scheme from which the returns conditions code in &lt;ReturnsCode&gt; is taken. Optional
     * and non-repeating, but this field must be present if &lt;ReturnsCode&gt; is present.
     *
     * @param returnsCodeType {@link ReturnsConditionsCodeTypeCode} object
     */
    public void setReturnsCodeType(ReturnsConditionsCodeTypeCode returnsCodeType)
    {
        this.returnsCodeType.setOnixValue(returnsCodeType);
    }

    /**
     * A returns conditions code from the scheme specified in &lt;ReturnsCodeType&gt;. Optional and non-repeating, but
     * this field must be present if &lt;ReturnsCodeType&gt; is present.
     *
     * @param returnsCode According to the scheme specified in &lt;ReturnsCodeType&gt;: for values defined by BISAC for US use, see List 66
     */
    public void setReturnsCode(String returnsCode)
    {
        this.returnsCode.setText(returnsCode);
    }

    /**
     * The last date for returns, when the supplier has placed a time limit on returns from retailers. Typically
     * this occurs when the publisher has made the product out-of-print. Optional and non-repeating.
     *
     * @param lastDateForReturns the {@link LocalDate} representing the last date for returns
     */
    public void setLastDateForReturns(LocalDate lastDateForReturns)
    {
        this.lastDateForReturns.setOnixValue(lastDateForReturns);
    }

    /**
     * An ONIX code indicating the availability of a product from a supplier. This element has been added as a preferred
     * successor to the &lt;AvailabilityCode&gt; element, and is intended to be used both by publishers (who should also
     * include the new &lt;PublishingStatus&gt; element in PR.20) and by intermediary suppliers (who may also include
     * &lt;PublishingStatus&gt; if they are in a position to do so. Each occurrence of the &lt;SupplyDetail&gt; composite must carry
     * either &lt;AvailabilityCode&gt; or &lt;ProductAvailability&gt;, or both. The element is non-repeating. Recommended practise
     * is in future to use this new element, and, where possible and appropriate, to include the &lt;PublishingStatus&gt;
     * element in PR.20. It is likely that the &lt;AvailabilityCode&gt; element will be “deprecated” in due course in
     * a future release.
     *
     * @param productAvailability {@link ProductAvailabilityCode} object
     */
    public void setProductAvailability(ProductAvailabilityCode productAvailability)
    {
        this.productAvailability.setOnixValue(productAvailability);
    }

    /**
     *  * A group of data elements which together specify a new supply source to which orders are referred. Use only when the
     * code in &lt;ProductAvailability&gt; or &lt;AvailabilityCode&gt; indicates “no longer available from us, refer to new supplier”.
     * Only one occurrence of the composite is permitted in this context.
     *
     * @param newSupplier the {@link NewSupplier} object
     */
    public void setNewSupplier(NewSupplier newSupplier)
    {
        this.newSupplier = newSupplier;
        int index = indexOf(productAvailability);
        addContent(newSupplier);
    }

    /**
     * An ONIX code which specifies the format of the date in &lt;ExpectedShipDate&gt;. Optional an non-repeating. If the
     * field is omitted, the default format YYYYMMDD will be assumed.
     *
     * @param dateFormat {@link DateFormatCode} object
     */
    public void setDateFormat(DateFormatCode dateFormat)
    {
        this.dateFormat.setOnixValue(dateFormat);
    }

    /**
     * If the product is not currently available, the date on which shipping from the supplier to retailers is expected
     * to begin or resume. Optional and non-repeating; required with certain code values in the &lt;AvailabilityCode&gt; element.
     * The format is as specified in the &lt;DateFormat&gt; field. The default format is YYYYMMDD, ie an exact year-month-day.
     *
     * @param expectedShipDates one LocalDate for a single ship date or two LocalDate for a spread of ship date (the second case is not yet implemented)
     */
    public void setExpectedShipDate(LocalDate... expectedShipDates)
    {
        if (expectedShipDates.length == 1)
        {
            String pattern = "yyyyMMdd";
            if (StringUtils.isNotEmpty(dateFormat.getText()))
            {
                pattern = dateFormat.getOnixValue().getPattern();
            }
            this.expectedShipDate.setStringConverter(new LocalDateStringConverter(pattern));
            this.expectedShipDate.setOnixValue(expectedShipDates[0]);
        }
        else if (expectedShipDates.length == 2)
        {
            throw new NotImplementedException("the case of a spread of dates is not (yet) implemented");
        }
    }

    /**
     * The date when a new product can be placed on sale by retailers in the market served by the supplier. Optional
     * and non-repeating. <b>If the &lt;OnSaleDate/gt; element is used, it means that there is an embargo on sales to consumers
     * before the stated date. Otherwise, sales to consumers are permitted as soon as stocks reach retailers.</b> In the
     * UK, publishers who are following the PA/BA Launch Dates Code of Practice should use this element for the Launch
     * Date.
     *
     * @param onSaleDate LocalDate object
     */
    public void setOnSaleDate(LocalDate onSaleDate)
    {
        this.onSaleDate.setOnixValue(onSaleDate);
    }

    /**
     * The expected average number of days from receipt of order to despatch (for items “manufactured on demand” or
     * “only to order”). Optional and non-repeating.
     *
     * @param orderTime Variable-length integer, one or two digits only
     */
    public void setOrderTime(Integer orderTime)
    {
        if (orderTime > 99)
        {
            throw new IllegalOnixDataException("Only values between 0 and 99 are allowed for <OrderTime>");
        }
        this.orderTime.setOnixValue(orderTime);
    }

    /**
     * A repeatable group of data elements which together specify a quantity of stock and, where a supplier has more
     * than one warehouse, a supplier location. Optional.
     *
     * @param stock Stock stock
     */
    public void addStock(Stock stock)
    {
        stocks.add(stock);
        int index = indexOf(orderTime);
        addContent(index + stocks.size(), stock);
    }

    /**
     * The quantity in each carton or binder’s pack in stock currently held by the supplier. (This element is placed
     * in Group PR.24 since it cannot be assumed that pack quantities will be the same for stock held at different suppliers.)
     *
     * @param packQuantity Variable-length integer, suggested maximum length four digits
     */
    public void setPackQuantity(Integer packQuantity)
    {
        this.packQuantity.setOnixValue(packQuantity);
    }

    /**
     * Used with &lt;AudienceRestrictionNote&gt; where within a particular market there is an additional restriction on sale,
     * imposed either by the publisher (eg an answer book to be sold only to bona fide teachers) or by another agency
     * (eg “indexing” in the German market). Optional and non-repeating.
     *
     * @param audienceRestrictionFlag AudienceRestrictionFlagCode object
     */
    public void setAudienceRestrictionFlag(AudienceRestrictionFlagCode audienceRestrictionFlag)
    {
        this.audienceRestrictionFlag.setOnixValue(audienceRestrictionFlag);
    }
    
    /**
     * Free text describing a non-territorial restriction on supply, only when &lt;AudienceRestrictionFlag&gt; is present.
     * Optional and non-repeating.
     * 
     * @param audienceRestrictionNote Variable-length text, maximum 300 characters
     */
    public void setAudienceRestrictionNote(String audienceRestrictionNote)
    {
        this.audienceRestrictionNote.setText(audienceRestrictionNote);
    }

    /**
     * An ONIX code which specifies a reason why a price amount is not sent. <b>If code value 02 is used to send advance
     * information without giving a price, the price must be confirmed as soon as possible.</b> Optional and non-repeating,
     * but required if the &lt;SupplyDetail&gt; composite does not carry a price.
     *
     * @param unpricedItemType
     */
    public void setUnpricedItemType(UnpricedItemTypeCode unpricedItemType)
    {
        this.unpricedItemType.setOnixValue(unpricedItemType);
    }

    @Override
    public ValidationResult validate()
    {
        ValidationResult result = super.validate();
        if (StringUtils.isEmpty(supplierEANLocationNumber.getText()) && StringUtils.isEmpty(supplierSAN.getText()) &&
                supplierIdentifiers.isEmpty() && StringUtils.isEmpty(supplierName.getText()))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("SupplyDetail", "Each occurrence of the &ltSupplyDetail&gt;\n" +
                    " composite must carry either at least one supplier identifier, or a &lt;SupplierName&gt;", "SupplierEANLocationNumber", "SupplierSAN", "SupplierIdentifier",  "SupplierName"));
        }
        if ((StringUtils.isNotEmpty(returnsCodeType.getText()) && StringUtils.isEmpty(returnsCode.getText())) ||
                (StringUtils.isEmpty(returnsCodeType.getText()) && StringUtils.isNotEmpty(returnsCode.getText())))
        {
            result.setValid(false);
            result.addValidationMessage(new ValidationMessage("SupplyDetail", "<ReturnsCode> field must be present if <ReturnsCodeType> is present and vice versa", "ReturnsCodeType", "ReturnsCode"));
        }
        return result;
    }


}
