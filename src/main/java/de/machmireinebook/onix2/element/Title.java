package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.NotificationOrUpdateTypeCode;

/**
 * User: mjungierek
 * Date: 30.07.2016
 * Time: 23:36
 */
public class Title extends OnixElement
{
    private OnixCodeListValueElement<NotificationOrUpdateTypeCode> titleType = new OnixCodeListValueElement<>("TitleType", "b202", OnixOccurenceType.MANDATORY, NotificationOrUpdateTypeCode::byCode);
    private OnixElement abbreviatedLength = new OnixElement("AbbreviatedLength", "b276");
    private OnixElement titleText = new OnixElement("TitleText", "b203");
    private OnixElement titlePrefix = new OnixElement("TitlePrefix", "b030");
    private OnixElement titleWithoutPrefix = new OnixElement("TitleWithoutPrefix", "b031");
    private OnixElement subtitle = new OnixElement("Subtitle", "b029");


    public Title()
    {
        super("Title", "title");
        addContent(titleType);
        addContent(abbreviatedLength);
        addContent(titleText);
        addContent(titlePrefix);
        addContent(titleWithoutPrefix);
        addContent(subtitle);
    }

    public void setTitleType(NotificationOrUpdateTypeCode titleType)
    {
        this.titleType.setOnixValue(titleType);
    }

    public void setAbbreviatedLength(String abbreviatedLength)
    {
        this.abbreviatedLength.setText(abbreviatedLength);
    }

    public void setTitleText(String titleText)
    {
        this.titleText.setText(titleText);
    }

    public void setTitlePrefix(String titlePrefix)
    {
        this.titlePrefix.setText(titlePrefix);
    }

    public void setTitleWithoutPrefix(String titleWithoutPrefix)
    {
        this.titleWithoutPrefix.setText(titleWithoutPrefix);
    }

    public void setSubtitle(String subtitle)
    {
        this.subtitle.setText(subtitle);
    }
}
