package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.WebsiteRoleCode;

/**
 *  Group of data elements which together identify and provide pointers to a website. Used in different other composite
 *  like {@link Product} and {@link SupplyDetail}.
 */
public class Website extends OnixElement
{
    private OnixCodeListValueElement<WebsiteRoleCode> websiteRole = new OnixCodeListValueElement<>("WebsiteRole", "b367", WebsiteRoleCode::byCode);
    private OnixElement websiteDescription = new OnixElement("WebsiteDescription", "b294");
    private OnixElement websiteLink = new OnixElement("WebsiteLink", "b295");

    public Website()
    {
        super("Website", "website");
        addContent(websiteRole);
        addContent(websiteDescription);
        addContent(websiteLink);
    }

    /**
     * An ONIX code which identifies the role or purpose of the website which is linked through the &lt;WebsiteLink&gt; element.
     * Optional and non-repeating.
     *
     * @param websiteRole WebsiteRoleCode object
     */
    public void setWebsiteRole(WebsiteRoleCode websiteRole)
    {
        this.websiteRole.setOnixValue(websiteRole);
    }

    /**
     * Free text describing the nature of the website which is linked through the &lt;WebsiteLink&gt; element. Optional
     * and non-repeating.
     *
     * @param websiteDescription Variable-length text, suggested maximum length 300 characters (XHTML is enabled in
     *                           this element – see ONIX for Books – Product Information Message – XML Message Specification, Section 7)
     */
    public void setWebsiteDescription(String websiteDescription)
    {
        this.websiteDescription.setText(websiteDescription);
    }

    /**
     * The URL for the website. Mandatory in each occurrence of the &lt;Website&gt; composite, and non-repeating.
     *
     * @param websiteLink Variable-length text, suggested maximum length 300 characters
     */
    public void setWebsiteLink(String websiteLink)
    {
        this.websiteLink.setText(websiteLink);
    }
}
