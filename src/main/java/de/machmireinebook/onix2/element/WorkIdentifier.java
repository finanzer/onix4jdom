package de.machmireinebook.onix2.element;

import de.machmireinebook.codelist.WorkIdentifierTypeCode;

/**
 * User: mjungierek
 * Date: 31.07.2016
 * Time: 10:24
 */
public class WorkIdentifier extends OnixElement
{
    private OnixCodeListValueElement<WorkIdentifierTypeCode> workIDType = new OnixCodeListValueElement<>("WorkIDType", "b201", WorkIdentifierTypeCode::byCode);
    private OnixElement idTypeName  = new OnixElement("IDTypeName", "b233");
    private OnixElement idValue = new OnixElement("IDValue", "b244", OnixOccurenceType.MANDATORY);

    public WorkIdentifier()
    {
        super("WorkIdentifier", "workidentifier");
        addContent(workIDType);
        addContent(idTypeName);
        addContent(idValue);
    }

    public void setWorkIDType(WorkIdentifierTypeCode workIDType)
    {
        this.workIDType.setOnixValue(workIDType);
    }

    public void setIdTypeName(String idTypeName)
    {
        this.idTypeName.setText(idTypeName);
    }

    public void setIdValue(String idValue)
    {
        this.idValue.setText(idValue);
    }
}
