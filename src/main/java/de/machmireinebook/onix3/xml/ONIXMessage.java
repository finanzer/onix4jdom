package de.machmireinebook.onix3.xml;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;

/**
 * User: mjungierek
 * Date: 29.07.2016
 * Time: 13:58
 */
public class ONIXMessage extends Document
{
    private Header header = new Header();
    private List<Product> products = new ArrayList<Product>();
    private Element noProduct = new Element("NoProduct");

    public ONIXMessage()
    {
        Element root = new Element("ONIXMessage");
        root.setAttribute("release", "3.0");
        setRootElement(root);
        root.addContent(header);
        root.addContent(noProduct);
    }

    public void addProduct(Product product)
    {
        if (products.isEmpty())
        {
            getRootElement().removeContent(noProduct);
        }
        products.add(product);
        getRootElement().addContent(product);
    }
}
