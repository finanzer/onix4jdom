package de.machmireinebook.onix3.xml;

import java.util.Arrays;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.IllegalNameException;

/**
 * User: mjungierek
 * Date: 29.07.2016
 * Time: 15:29
 */
public class OnixElement extends Element
{
    private final List<String> allowedAttributes = Arrays.asList("datestamp", "sourcename", "sourcetype", "collationkey",
                                            "dateformat", "language", "release", "textcase", "textformat", "textscript");

    public void setDatestamp(String value)
    {
        setAttribute("datestamp", value);
    }

    public void setSourcename(String value)
    {
        setAttribute("sourcename", value);
    }

    public void setSourcetype(String value)
    {
        setAttribute("sourcetype", value);
    }

    @Override
    public Element setAttribute(String name, String value)
    {
        if (!allowedAttributes.contains(name))
        {
            throw new IllegalNameException(name + " is not a allowed onix attribute name");
        }
        return super.setAttribute(name, value);
    }
}
