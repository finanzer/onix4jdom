package de.machmireinebook.stringconverter;

import java.util.ArrayList;
import java.util.List;

import de.machmireinebook.codelist.CountryCode;

import org.apache.commons.lang3.StringUtils;

/**
 * A StringConverter to convert a List of {@see CountryCode} to a string consisting of two upper case letter codes separated by spaces and vice versa.
 */
public class CountryCodeListStringConverter extends StringConverter<List<CountryCode>>
{
    @Override
    public String toString(List<CountryCode> countryCodes)
    {
        return StringUtils.join(countryCodes, " ");
    }

    @Override
    public List<CountryCode> fromString(String codes)
    {
        List<CountryCode> countryCodes = new ArrayList<>();
        String[] codeStrings = StringUtils.split(codes, " ");
        for (String codeString : codeStrings)
        {
            countryCodes.add(CountryCode.byCode(codeString));
        }
        return countryCodes;
    }
}