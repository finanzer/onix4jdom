package de.machmireinebook.stringconverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * <p>{@link StringConverter} implementation for {@link LocalDate} values.</p>
 *
 * @see LocalTimeStringConverter
 * @see LocalDateTimeStringConverter
 * @since JavaFX 8u40
 */
public class LocalDateStringConverter extends StringConverter<LocalDate> {


    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    // ------------------------------------------------------------ Constructors

    public LocalDateStringConverter() {
    }

    public LocalDateStringConverter(String pattern) {
        formatter = DateTimeFormatter.ofPattern(pattern);
    }

    // ------------------------------------------------------- Converter Methods

    /** {@inheritDoc} */
    @Override public LocalDate fromString(String value) {
        return LocalDate.parse(value, formatter);
    }

    /** {@inheritDoc} */
    @Override public String toString(LocalDate value) {
        return value.format(formatter);
    }

}
