package de.machmireinebook.stringconverter;

import de.machmireinebook.codelist.OnixCodeList;
import de.machmireinebook.codelist.OnixCodeListFactory;

/**
 * <p>{@link StringConverter} implementation for {@link OnixCodeList}
 * (and int primitive) values.</p>
 */
public class OnixCodeListStringConverter<T extends OnixCodeList> extends StringConverter<T>
{
    OnixCodeListFactory<T> factory;

    public OnixCodeListStringConverter(OnixCodeListFactory<T> factory)
    {
        this.factory = factory;
    }

    /** {@inheritDoc} */
    @Override
    public T fromString(String value) {
        return factory.byCode(value);
    }

    /** {@inheritDoc} */
    @Override public String toString(T value)
    {
        // If the specified value is null, return a zero-length String
        if (value == null) {
            return "";
        }

        return value.getCode();
    }
}
