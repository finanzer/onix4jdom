package de.machmireinebook.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: mjungierek
 * Date: 13.08.2016
 * Time: 16:46
 */
public class ValidationMessage
{
    private String parentElement;
    private List<String> affectedElements = new ArrayList<>();
    private String message;

    public ValidationMessage(String parentElement, String message, String affectedElement )
    {
        this.parentElement = parentElement;
        this.affectedElements.add(affectedElement);
        this.message = message;
    }

    public ValidationMessage(String parentElement, String message, String... affectedElements)
    {
        this.parentElement = parentElement;
        this.affectedElements.addAll(Arrays.asList(affectedElements));
        this.message = message;
    }

    public String getParentElement()
    {
        return parentElement;
    }

    public void setParentElement(String parentElement)
    {
        this.parentElement = parentElement;
    }

    public List<String> getAffectedElements()
    {
        return Collections.unmodifiableList(affectedElements);
    }

    public void addAffectedElement(String affectedElement)
    {
        this.affectedElements.add(affectedElement);
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
