package de.machmireinebook.validation;

import java.util.ArrayList;
import java.util.List;

/**
 * User: mjungierek
 * Date: 13.08.2016
 * Time: 21:05
 */
public class ValidationResult
{
    private List<ValidationMessage> validationMessages = new ArrayList<>();
    private boolean valid = true;

    public ValidationResult()
    {
    }

    public ValidationResult(boolean valid, List<ValidationMessage> validationMessages)
    {
        this.validationMessages = validationMessages;
        this.valid = valid;
    }

    public void addValidationMessage(ValidationMessage validationMessage)
    {
        validationMessages.add(validationMessage);
    }

    public List<ValidationMessage> getValidationMessages()
    {
        return validationMessages;
    }

    public boolean isValid()
    {
        return valid;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }
}
