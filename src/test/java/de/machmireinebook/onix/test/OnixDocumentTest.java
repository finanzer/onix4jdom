package de.machmireinebook.onix.test;

import java.time.LocalDate;

import de.machmireinebook.codelist.ProductContentTypeCode;
import de.machmireinebook.codelist.ProductFormCode;
import de.machmireinebook.codelist.RecordSourceTypeCode;
import de.machmireinebook.codelist.WebsiteRoleCode;
import de.machmireinebook.onix2.element.Contributor;
import de.machmireinebook.onix2.element.ONIXMessage;
import de.machmireinebook.onix2.element.Product;
import de.machmireinebook.onix2.element.Series;
import de.machmireinebook.onix2.element.Title;
import de.machmireinebook.onix2.element.Website;

import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * User: mjungierek
 * Date: 29.07.2016
 * Time: 20:59
 */
public class OnixDocumentTest
{
    @org.junit.Test
    public void onix2BasicTest() throws Exception
    {
        ONIXMessage m = new ONIXMessage();
        m.getHeader().setFromCompany("mach-mir-ein-ebook.de E-Book-Verlag Jungierek");
        m.getHeader().setFromPerson("Melanie Jungierek");
        m.getHeader().setFromEmail("info@mach-mir-ein-ebook.de");
        m.getHeader().setSentDate(LocalDate.now());

        Product product = new Product();
        m.addProduct(product);
        product.setRecordReference("mach-mir-ein-ebook.de/115");
        product.setRecordSourceType(RecordSourceTypeCode.Publisher);

        product.setProductForm(ProductFormCode.Electronic_book_text);
        product.setProductContentType(ProductContentTypeCode.Text_eye_readable);

        Series series = new Series();
        series.setTitleOfSeries("Innenstadtstories");
        series.setNumberWithinSeries(1);
        Title seriesTitle = new Title();
        seriesTitle.setTitleText("Innenstadtstories");
        seriesTitle.setSubtitle("Die Großstadtsaga");
        series.addTitle(seriesTitle);
        product.addSeries(series);

        Title productTitle = new Title();
        productTitle.setTitleText("Recht und Ordnung");
        product.addTitle(productTitle);

        Website website = new Website();
        website.setWebsiteRole(WebsiteRoleCode.Contributor_s_own_website_for_group_or_series_of_works);
        website.setWebsiteDescription("Website for promotion of series Innenstadtstories");
        website.setWebsiteLink("http://www.innenstadtstories.de/");
        product.addWebsite(website);

        Contributor contributor = new Contributor();
        product.addContributor(contributor);

        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        String output = outputter.outputString(m);
        System.out.println(output);
    }

}
